<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

/**
 * Ultility functions & constants
 */
require_once APP_PATH . "/app/ultility.php";
require_once APP_PATH . "/app/system_defined.php";

return new \Phalcon\Config(array(
    'database' => array(
            'adapter'     => 'Mysql',
            'host'        => 'localhost',
            'username'    => 'anvylabs_pos',
            'password'    => '@AnvyTeam5',
            'dbname'      => 'anvylabs_pos',
            'charset'     => 'utf8',
    ),
    'databases' => array(
        'anvylabs2' => array(
            'adapter'     => 'Mysql',
            'host'        => 'localhost',
            'username'    => 'anvylabs_pos',
            'password'    => '@AnvyTeam5',
            'dbname'      => 'anvylabs_pos',
            'charset'     => 'utf8',
        )
    ),
    'application' => array(
        'appDir'         => APP_PATH . '/app/',
        'controllersDir' => APP_PATH . '/app/controllers/frontend',
        'adminControllersDir' => APP_PATH . '/app/controllers/admin',
        'frontendControllersDir'   => APP_PATH . '/app/controllers/frontend',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginDir'      => APP_PATH . '/app/plugin/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => URL,
        'cryptSalt'      => 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D',
        'viewCacheTime'  => 86400
    ),
    'mail' => array(
        'fromName' => 'hung.lam',
        'fromEmail' => 'khuongnhi.shop@gmail.com',
        'smtp' => array(
            'server'=>'smtp.gmail.com',
            'port'=>587,
            'security'=>'ssl',
            'username'=>'khuongnhi.shop@gmail.com',
            'password'=>'hung!(()'
        )
    )
));
