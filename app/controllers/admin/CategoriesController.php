<?php
namespace RW\Controllers\Admin;

class CategoriesController extends ControllerBase {

    protected $notFoundMessage = 'This category did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'name', 'image','order_no'], function($array) {
            if (isset($array['image'])) {
                if (!is_null($array['image'])) {
                    $array['image'] = URL.'/'.$array['image'];
                } else {
                    $array['image'] = '';
                }
            }
            return $array;
        },$arr_where);
    }

    public function editAction($id = 0)
    {                
        return $this->editRecord($id, function($cate) {
            if (!is_null($cate->image)) {
                $cate->image = URL.'/'.$cate->image;
            }
            settype($cate->id, 'int');
            settype($cate->order_no, 'int');
            settype($cate->parent_id, 'int');
            settype($cate->position, 'int');
            return $cate;
        });
    }

    public function updateAction()
   {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        if (isset($data['id'])) {
            $category = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($category) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $category = new $this->model;
            $message = 'has been created';
        }
        $category->name = $filter->sanitize($data['name'], 'string');
        $category->short_name = removeVietnamseChac($category->name);
        $category->order_no = $filter->sanitize($data['order_no'], 'string');
        $category->position = $filter->sanitize($data['position'], 'string');
        $category->meta_title = $filter->sanitize($data['meta_title'], 'string');
        $category->description = $filter->sanitize($data['description'], 'string');
        $category->banner_desciption = $filter->sanitize($data['banner_desciption'], 'string');
        $category->type = $filter->sanitize($data['type'], 'string');
        $category->meta_description = $filter->sanitize($data['meta_description'], 'string');
        $category->parent_id = $filter->sanitize($data['parent_id'], 'int');
        if ($this->request->hasFiles() == true) {
            $imagePath = PUBLIC_PATH . DS . 'images' . DS . 'category';
            if (!file_exists($imagePath)) {
                mkdir($imagePath, 0755, true);
            }
            foreach($this->request->getUploadedFiles() as $file) {
                if (isImage($file->getType())) {
                    $fileName = $file->getName();
                    $fileExt = $file->getExtension();

                    $fileName = str_replace('.'.$fileExt, '_'.date('d-m-y').'.'.$fileExt, \Phalcon\Text::uncamelize($fileName));

                    if ($file->moveTo($imagePath . DS . $fileName)) {
                        if (isset($category->image) && file_exists(PUBLIC_PATH . DS . $category->image)) {
                            unlink(PUBLIC_PATH . DS . $category->image);
                        }
                        $category->image = 'images/category/'.$fileName;
                    }
                    break;
                }
            }
        }
        if ($category->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'category <b>'.$category->name.'</b> '.$message.' successful.', 'data' => ['id' => $category->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $category->getMessages()];
        }

        return $this->response($arrReturn);
    }

    public function getOptionsAction($id)
    {
        $arr_explode = explode('/', $id);
        $id = end($arr_explode);
        settype($id, 'int');
        $arr_list_parent = $this->model->find([
            'conditions' => 'parent_id = ?1 and id != ?2'
            ,"bind"       => array(1 => 0,2=>$id)
        ]);
        $arr_list_parent = $arr_list_parent->toArray();
        $arr_option = array();
        for($i=0;$i<count($arr_list_parent);$i++){
            $arr_option [] = array("text"=>$arr_list_parent[$i]['name'],"value"=>(int) $arr_list_parent[$i]['id'] );
        }
        $arrReturn = ['error' => 0, 'data' => $arr_option ];
        return $this->response($arrReturn);
    }

}
