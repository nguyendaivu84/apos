<?php
namespace RW\Controllers\Admin;

class CommentsController extends ControllerBase {

    protected $notFoundMessage = 'This comment did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'customer_logo', 'customer_name','comments'], function($array) {
            if (isset($array['customer_logo'])) {
                if (!is_null($array['customer_logo'])) {
                    $array['customer_logo'] = URL.'/'.$array['customer_logo'];
                } else {
                    $array['customer_logo'] = '';
                }
            }
            return $array;
        },$arr_where);
    }

    public function editAction($id = 0)
    {
        return $this->editRecord($id, function($comment) {
            if (!is_null($comment->customer_logo)) {
                $comment->customer_logo = URL.'/'.$comment->customer_logo;
            }
            return $comment;
        });

    }

    public function getListCustomersAction(){
        $arr_customers = (new \RW\Models\Customers)->find([
            'conditions' => 'deleted = ?1 and active = ?2'
            ,"bind"       => array(1 => 0 , 2=>1)
        ]);
        $arr_customers = $arr_customers->toArray();
        $arr_option = array();
        for($i=0;$i<count($arr_customers);$i++){
            $arr_option [] = array("text"=>$arr_customers[$i]['name'],"value"=> (int) $arr_customers[$i]['id'] );
        }
        $arrReturn = ['error' => 0, 'data' => $arr_option ];
        return $this->response($arrReturn);
    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        if (isset($data['id'])) {
            $comment = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            $comment->update_at = strtotime(date('m-d-Y'));
            if ($comment) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $comment = new $this->model;
            $message = 'has been created';
            $comment->created_at = strtotime(date('m-d-Y'));
        }
        $comment->customer_id = $filter->sanitize($data['customer_id'], 'int');

        $customer = (new \RW\Models\Customers)->findFirst($comment->customer_id);
        if($customer){
            $comment->customer_name = $customer->name;
            $comment->customer_logo = $customer->image;
            $comment->customer_link = $customer->link;
        }
        $comment->active = $filter->sanitize($data['active'], 'int');
        $comment->user_id = $filter->sanitize($data['user_id'], 'int');
        $comment->comments = $filter->sanitize($data['comments'], 'string');
        $comment->mau_nen = $filter->sanitize($data['mau_nen'], 'string');
        $comment->mau_chu = $filter->sanitize($data['mau_chu'], 'string');
       
        if ($comment->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'Comment '.$message.' successful.', 'data' => ['id' => $comment->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $comment->getMessages()];
        }

        return $this->response($arrReturn);
    }
}
