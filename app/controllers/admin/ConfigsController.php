<?php
namespace RW\Controllers\Admin;

class ConfigsController extends ControllerBase {
    protected $notFoundMessage = 'This configure did not exist.';
    public function listAction()
    {
        $result = $this->listRecords(['id', 'cf_key', 'cf_value', 'status']);
        //pr($result);
        return $result;
    }
    public function editAction($id = 0)
    {
        $filter = new \Phalcon\Filter;
        $config = $this->model->findFirst($filter->sanitize($id, 'int'));
        if ($config) {
            return $this->response(['error' => 0, 'data' => $config->toArray()]);
        } else {
            return $this->error404($this->notFoundMessage);
        }
    }
    public function loadSystemConfigAction(){
        $arr_data = array(
            'SYSTEM_LOGO'=>URL.'/'.SYSTEM_LOGO,
            'SYSTEM_EMAIL'=>SYSTEM_EMAIL,
            'SYSTEM_HOTLINE'=>SYSTEM_HOTLINE,
            'SYSTEM_TITLE'=>SYSTEM_TITLE,
            // 'SYSTEM_ICONS'=>SYSTEM_ICONS,
            'COMPANY_PHONE'=>COMPANY_PHONE,
            'COMPANY_ADDRESS'=>COMPANY_ADDRESS,
            'SYSTEM_COPYRIGHT'=>SYSTEM_COPYRIGHT
        );
        return $this->response(['error' => 0, 'data' => $arr_data]);
    }
    public function SaveSystemConfigAction(){
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        if(!isset($data['SYSTEM_LOGO'])) $data['SYSTEM_LOGO'] = SYSTEM_LOGO;
        if ($this->request->hasFiles() == true) {            
            $imagePath = PUBLIC_PATH . DS . 'images' . DS;
            if (!file_exists($imagePath)) {
                @mkdir($imagePath, 0755, true);
            }
            foreach($this->request->getUploadedFiles() as $file) {
                if (isImage($file->getType())) {
                    $fileName = $file->getName();
                    $fileExt = $file->getExtension();

                    $fileName = str_replace('.'.$fileExt, '_'.date('d-m-y').'.'.$fileExt, \Phalcon\Text::uncamelize($fileName));

                    if ($file->moveTo($imagePath . DS . $fileName)) {                        
                        $data['SYSTEM_LOGO'] = 'images/'.$fileName;
                    }
                    break;
                }
            }
        }
        
        $v_str = "<?php \r\n";
        foreach($data as $key => $val) {
            $v_str .= "\t defined('".$key."') || define('".$key."', '".$val."'); \r\n";
        }
        /*
            $v_str .= ' ?> ';        
        */
        @chmod(APP_PATH . "/app/system_defined.php",0777);
        $fp = fopen(APP_PATH . "/app/system_defined.php",'w');
        fwrite($fp, $v_str);
        fclose($fp);
        $arrReturn = ['error' => 0, 'message' => 'Save successful'];
        return $this->response($arrReturn);
    }
    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['cf_key' => '', 'cf_value' => '', 'status' => ''], $data);
        if (isset($data['id'])) {
            $config = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($config) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $config = new $this->model;
            $message = 'has been created';
        }

        $config->cf_key = $filter->sanitize($data['cf_key'], 'string');
        $config->cf_value = $data['cf_value'];
        $config->status = $filter->sanitize($data['status'], 'int');
        $config->type = $filter->sanitize($data['type'], 'int');
        $config->order_no = $filter->sanitize($data['order_no'], 'int');

        if ($config->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'Configure <b>'.$config->cf_key.'</b> '.$message.' successful.', 'data' => ['id' => $config->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $config->getMessages()];
        }

        return $this->response($arrReturn);
    }

    public function getOptionGroupAction()
    {
        $arrReturn = ['error' => 0, 'data' => $this->model->getOptionGroup()];

        return $this->response($arrReturn);
    }

    public function getListUnitAction()
    {
        $data = $this->model->getListUnit();
        $arrReturn = ['error' => 0, 'data' => $data];

        return $this->response($arrReturn);
    }

}
