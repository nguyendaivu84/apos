<?php
namespace RW\Controllers\Admin;

class CustomersController extends ControllerBase {

    protected $notFoundMessage = 'This customer did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'image', 'name','description'], function($array) {
            if (isset($array['image'])) {
                if (!is_null($array['image'])) {
                    $array['image'] = URL.'/'.$array['image'];
                } else {
                    $array['image'] = '';
                }
            }
            return $array;
        },$arr_where);
    }

    public function editAction($id = 0)
    {
        return $this->editRecord($id, function($customer) {
            if (!is_null($customer->image)) {
                $customer->image = URL.'/'.$customer->image;
            }
            return $customer;
        });

    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        if (isset($data['id'])) {
            $customer = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($customer) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $customer = new $this->model;
            $message = 'has been created';
            $customer->created_at = strtotime(date('m-d-Y'));
        }
        $customer->name = $filter->sanitize($data['name'], 'string');
        $customer->short_name = removeVietnamseChac($customer->name);
        $customer->link = $filter->sanitize($data['link'], 'string');
        $customer->description = $filter->sanitize($data['description'], 'string');
        $customer->active = $filter->sanitize($data['active'], 'int');
        if ($this->request->hasFiles() == true) {
            $imagePath = PUBLIC_PATH . DS . 'images' . DS . 'customers';
            if (!file_exists($imagePath)) {
                mkdir($imagePath, 0755, true);
            }
            foreach($this->request->getUploadedFiles() as $file) {
                if (isImage($file->getType())) {
                    $fileName = $file->getName();
                    $fileExt = $file->getExtension();

                    $fileName = str_replace('.'.$fileExt, '_'.date('d-m-y').'.'.$fileExt, \Phalcon\Text::uncamelize($fileName));

                    if ($file->moveTo($imagePath . DS . $fileName)) {
                        if (isset($customer->image) && file_exists(PUBLIC_PATH . DS . $customer->image)) {
                            unlink(PUBLIC_PATH . DS . $customer->image);
                        }
                        $customer->image = 'images/customers/'.$fileName;
                    }
                    break;
                }
            }
        }
        if ($customer->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'customer <b>'.$customer->name.'</b> '.$message.' successful.', 'data' => ['id' => $customer->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $customer->getMessages()];
        }

        return $this->response($arrReturn);
    }
}
