<?php
namespace RW\Controllers\Admin;

class JobsController extends ControllerBase {

    protected $notFoundMessage = 'This job did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'title', 'short_description','customer_logo'], function($array) {
            if (isset($array['short_description'])) {
                if (is_null($array['short_description'])) {
                    $array['short_description'] = 'N/A';
                }
            }
            return $array;
        },$arr_where);
    }
    
    public function editAction($id = 0)
    {
        $arr_explode = explode('/', $id);
        $id = (int) end($arr_explode);
        return $this->editRecord($id, function($job) {
            if (!is_null($job->image)) {
                $job->image = URL.'/'.$job->image;                
            }
            $job->time_expires = date('m/d/Y',strtotime($job->time_expires));                
            settype($job->id, "int");
            settype($job->order_no, "int");
            settype($job->active, "int");
            settype($job->deleted, "int");
            return $job;
        });

    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        $v_date_created = false;
        $v_date_updated = false;
        $session = $this->auth->getIdentity();

        $v_short_name = removeVietnamseChac($filter->sanitize($data['title'], 'string'));
        $current_id = 0;
        if(isset($data['id'])){
            $current_id = $filter->sanitize($data['id'], 'int');
        }
        $check = $this->model->findFirst('short_name = "'.$v_short_name.'" and id !='.$current_id);
        if($check){
            do{
                $str_key = randomStr();
                $v_short_name = removeVietnamseChac($filter->sanitize($data['title'], 'string')).'-'.$str_key;
                $check = $this->model->findFirst('short_name = "'.$v_short_name.'"');
            }while($check);
        }
        if (isset($data['id'])) {
            $job = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            $job->updated_at = date('Y-m-d H:i:s');
            if ($job) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $job = new $this->model;
            $job->created_at = date('Y-m-d H:i:s');
            $message = 'has been created';
        }                

        $job->title = $filter->sanitize($data['title'], 'string');        
        $job->short_name = $v_short_name;        
        
        $job->short_description = $filter->sanitize($data['short_description'], 'string');        
        $job->type = $filter->sanitize($data['type'], 'string');
        if(isset($data['customer_id'])) $job->customer_id = $filter->sanitize($data['customer_id'], 'int');
        $v_customer = (new \RW\Models\Customers)->findFirst($job->customer_id);
        $v_customer = $v_customer->toArray();
        
        if($v_customer && is_array($v_customer)){
            $job->customer_logo = $v_customer['image'];
        }       
        
        if(isset($data['time_expires'])){
            $job->time_expires = date('Y-m-d H:i:s',strtotime($filter->sanitize($data['time_expires'], 'string'))); // ;
        }
        $job->country = $filter->sanitize($data['country'], 'string');
        $job->city = $filter->sanitize($data['city'], 'string');        
        $job->active = $filter->sanitize($data['active'], 'int');
        $job->order_no = $filter->sanitize($data['order_no'], 'int');
        $job->description = $data['description'];
       
        if ($job->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'Job <b>'.$job->title.'</b> '.$message.' successful.', 'data' => ['id' => $job->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $job->getMessages()];
        }

        return $this->response($arrReturn);
    }

    public function getOptionsCustomersAction()
    {
        $arr_list_customer = (new \RW\Models\Customers)->find([
            'conditions' => 'deleted = ?1'
            ,"bind"       => array(1 => 0)
        ]);
        $arr_list_customer = $arr_list_customer->toArray();
        $arr_option = array();
        for($i=0;$i<count($arr_list_customer);$i++){
            $arr_option [] = array("text"=>$arr_list_customer[$i]['name'],"value"=> (int) $arr_list_customer[$i]['id'] );
        }
        $arrReturn = ['error' => 0, 'data' => $arr_option ];
        return $this->response($arrReturn);
    }
}
