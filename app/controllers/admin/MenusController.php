<?php
namespace RW\Controllers\Admin;
use RW\Models\Categories;
class MenusController extends ControllerBase {

    public function getCategoryList()
    {        
        $arr_list = (new \RW\Models\Categories)->find([
            'conditions' => 'deleted != ?1'
            ,"bind"       => array(1 => 1)
        ]);
        $arr_list = $arr_list->toArray();
        $arr_option = array();
        for($i=0;$i<count($arr_list);$i++){
            $arr_option [] = array("text"=>$arr_list[$i]['name'],"value"=>(int) $arr_list[$i]['id'] );
        }        
        return $arr_option;
    }

    public function getPriceList(){
         $arr_list = (new \RW\Models\Pricelists)->find([
            'conditions' => 'deleted != ?1 and active = ?2'
            ,"bind"       => array(1 => 1,2=>1)
        ]);
        $arr_list = $arr_list->toArray();
        $arr_option = array();
        for($i=0;$i<count($arr_list);$i++){
            $arr_option [] = array("text"=>$arr_list[$i]['title'],"value"=>(int) $arr_list[$i]['id'] );
        }        
        return $arr_option;
    }

    public function indexAction()
    {        
        $arrData = [
            'parent' => $this->model->getParent(),
            'tree' => $this->model->getTree(),
            'category'=>self::getCategoryList(),
            'pricelists'=>self::getPriceList(),
        ];
        return $this->response(['error' => 0, 'data' => $arrData]);
    }
    public function uploadImageAction(){        
        $this->view->disable();
        $v_image_link = '';
        if($this->request->isAjax()){
            $imagePath = PUBLIC_PATH . DS . 'images' . DS . 'menus';
            if (!file_exists($imagePath)) {
                mkdir($imagePath, 0755, true);
            }            
            foreach($this->request->getUploadedFiles() as $file) {
                if (isImage($file->getType())) {
                    $fileName = $file->getName();
                    $fileExt = $file->getExtension();

                    $fileName = str_replace('.'.$fileExt, '_'.date('d-m-y').'.'.$fileExt, \Phalcon\Text::uncamelize($fileName));

                    if ($file->moveTo($imagePath . DS . $fileName)) {
                        $v_image_link = '/images/menus/'.$fileName;                        
                    }
                    break;
                }
            }            
        }
        return $this->response(array('image'=>$v_image_link));
    }
    public function reorderAction()
    {
            $filter = new \Phalcon\Filter;
            $data = $this->getPost() ;
            $header =  json_decode($data['header'],true);
            $footer = json_decode($data['footer'],true);
            foreach ($header as $key => $parent) {
                $menu = $this->model->findFirst($filter->sanitize($parent['id'], 'int'));
                $menu->order_no=$key;
                $menu->save();
                if(isset($parent['children']) && count($parent['children'])){
                    foreach ($parent['children'] as $key2 => $child) {
                        $menu_child = $this->model->findFirst($filter->sanitize($child['id'], 'int'));
                        $menu_child->order_no=$key;
                        $menu_child->parent_id=$parent['id'];
                        $menu_child->save();
                    }
                }
            }
            foreach ($footer as $key => $parent) {
                $menu = $this->model->findFirst($filter->sanitize($parent['id'], 'int'));
                $menu->order_no=$key;
                $menu->save();
                if(isset($parent['children']) && count($parent['children'])){
                    foreach ($parent['children'] as $key2 => $child) {
                        $menu_child = $this->model->findFirst($filter->sanitize($child['id'], 'int'));
                        $menu_child->order_no=$key;
                        $menu_child->parent_id=$parent['id'];
                        $menu_child->save();
                    }
                }
            }
            return $this->response(['error' => 0]);
    }

    public function updateAction()
    {
        $admin = $this->auth->getIdentity();
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['name' => '', 'link' => '', 'group_name' => '', 'parent_id' => 0, 'order_no' => 1], $data);
        if (isset($data['id']) && $data['id']) {
            $menu = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($menu) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $menu = new $this->model;
            $menu->created_by = $admin['id'];
            $message = 'has been created';
        }
        $menu->name = $filter->sanitize($data['name'], 'string');
        $menu->link = $filter->sanitize($data['link'], 'string');
        $menu->group_name = $filter->sanitize($data['group_name'], 'string');
        $menu->parent_id = $filter->sanitize($data['parent_id'], 'int');
        $menu->category_id = $filter->sanitize($data['category_id'], 'int');
        $menu->pricelist_id = $filter->sanitize($data['pricelist_id'], 'int');
        $menu->image = $filter->sanitize($data['image'], 'string');
        $menu->contents = html_entity_decode($data['contents']);
        $menu->contents = str_replace('<br />', '<br>', $menu->contents);
        $menu->displayhtml = $data['displayhtml'] ? 1 : 0;
        $menu->order_no = $filter->sanitize($data['order_no'], 'int');
        $menu->updated_by = $admin['id'];
        if (isset($menu->id) && $menu->id === $menu->parent_id) {
            $menu->parent_id = 0;
        }
        if ($menu->save() === true) {
            $arrData = [
                'id'        => $menu->getId(),
                'parent'    => $this->model->getParent()
            ];
            $arrReturn = ['error' => 0, 'message' => 'Menu <b>'.$menu->name.'</b> '.$message.' successful.', 'data' => $arrData];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $menu->getMessages()];
        }

        return $this->response($arrReturn);
    }
    public function getContentsAction($id){
        $arr = explode('/', $id);
        $id = (int) end($arr);
        $menu = $this->model->findFirst($id);
        return $this->response(['error' => 0, 'data' => $menu->contents]);
    }
}
