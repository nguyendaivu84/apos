<?php
namespace RW\Controllers\Admin;

class PackagesController extends ControllerBase {

    protected $notFoundMessage = 'This package did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'name', 'description'], function($array) {
            if (isset($array['description'])) {
                if (is_null($array['description'])) {
                    $array['description'] = 'N/A';
                }
            }
            return $array;
        },$arr_where);
    }
    public function drawpackagelistAction($id){
        $arr_explode = explode('/', $id);
        $id = (int) end($arr_explode);
        $arr_package_details = (new \RW\Models\PackageDetails)->find([            
             'conditions' => 'deleted = ?1 and active = ?2 and packaged_id = ?3'
            ,"bind"       => array(1 => 0 , 2=>1,3=>$id)
        ]);
        $arr_package_details = $arr_package_details->toArray();
        $draw = '';
        if(empty($arr_package_details)){
            $draw = '<tr id="no_option" style="height:50px"><td colspan="3">No item found</td></tr>';
        }else{
            for($i=0;$i<count($arr_package_details);$i++){
                $draw .= '<tr style="height:50px" id="option_'.$arr_package_details[$i]['id'].'">
                    <td>'.$arr_package_details[$i]['name'].'</td>
                    <td>'.$arr_package_details[$i]['price'].'</td>
                    <td>
                        <button type="button" class="btn btn-info" onclick=deleteoption("'.$arr_package_details[$i]['id'].'") >Delete</button>
                    </td>
                </tr>';
            }            
        }
        $arrReturn = ['error' => 0, 'data' => $draw ];
        return $this->response($arrReturn);
    }
    public function deleteOptionListAction($id){
        $arr_explode = explode('/', $id);
        $id = (int) end($arr_explode);
        $option = (new \RW\Models\PackageDetails)->findFirst($id);
        $arrReturn = array('error' => 1);
        if($option){
            $option->deleted = 1;
            $option->save();
            $arrReturn = array('error' => 0);
        }
        return $this->response($arrReturn);
    }
    public function updateoptionlistAction(){
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        $details = (new \RW\Models\PackageDetails);
        $details->name = $filter->sanitize($data['name'], 'string');
        $details->short_name = removeVietnamseChac($details->name);
        $details->price = $filter->sanitize($data['price'], 'float');
        $details->packaged_id = $filter->sanitize($data['id'], 'int');
        $arrReturn = ['error' => 1 ];       
        if($details->save() === true){
             $draw = '<tr style="height:50px" id="option_'.$details->getId().'">
                    <td>'.$details->name.'</td>
                    <td>'.$details->price.'</td>
                    <td>
                        <button type="button" class="btn btn-info" onclick=deleteoption("'.$details->getId().'") >Delete</button>
                    </td>
                </tr>';
            $arrReturn = ['error' => 0,'data'=> $draw];
        }
        return $this->response($arrReturn);
    }
    public function editAction($id = 0)
    {
        $arr_explode = explode('/', $id);
        $id = (int) end($arr_explode);
             
        return $this->editRecord($id, function($package) {                        
            settype($package->id, "int");
            settype($package->active, "int");
            return $package;
        });
    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        $v_date_created = false;
        $v_date_updated = false;
        $session = $this->auth->getIdentity();

        $v_short_name = removeVietnamseChac($filter->sanitize($data['name'], 'string'));
        $current_id = 0;
        if(isset($data['id'])){
            $current_id = $filter->sanitize($data['id'], 'int');
        }
        $check = $this->model->findFirst('short_name = "'.$v_short_name.'" and id !='.$current_id);
        if($check){
            do{
                $str_key = randomStr();
                $v_short_name = removeVietnamseChac($filter->sanitize($data['title'], 'string')).'-'.$str_key;
                $check = $this->model->findFirst('short_name = "'.$v_short_name.'"');
            }while($check);
        }
        if (isset($data['id'])) {
            $package = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($package) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $package = new $this->model;
            $message = 'has been created';
        }                

        $package->name = $filter->sanitize($data['name'], 'string');        
        $package->short_name = $v_short_name;        
        
        $package->description = $filter->sanitize($data['description'], 'string');

        $package->active = $filter->sanitize($data['active'], 'int');
        $package->pricelist_id = $filter->sanitize($data['pricelist_id'], 'int');
       
        if ($package->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'package <b>'.$package->title.'</b> '.$message.' successful.', 'data' => ['id' => $package->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $package->getMessages()];
        }

        return $this->response($arrReturn);
    }

    public function getlistoptionAction()
    {
        $arr_list_customer = (new \RW\Models\Pricelists)->find([            
             'conditions' => 'deleted = ?1 and active = ?2'
            ,"bind"       => array(1 => 0 , 2=>1)
        ]);
        $arr_list_customer = $arr_list_customer->toArray();
        $arr_option = array();
        for($i=0;$i<count($arr_list_customer);$i++){
            $arr_option [] = array("text"=>$arr_list_customer[$i]['title'],"value"=> (int) $arr_list_customer[$i]['id'] );
        }
        $arrReturn = ['error' => 0, 'data' => $arr_option ];
        return $this->response($arrReturn);
    }
}
