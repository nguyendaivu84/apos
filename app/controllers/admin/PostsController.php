<?php
namespace RW\Controllers\Admin;

class PostsController extends ControllerBase {

    protected $notFoundMessage = 'This post did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'title', 'short_description','order_no','image','category_name'], function($array) {
            if (isset($array['short_description'])) {
                if (is_null($array['short_description'])) {
                    $array['short_description'] = 'N/A';
                }
            }
            return $array;
        },$arr_where);
    }
    function randomStr($number = 6){
        $v_chars = '0123456789';
        $l = strlen($v_chars)-1;
        $r = '';
        for($i=0;$i<$number;$i++){
            $p = rand(0,$l);
            $c = substr($v_chars,$p,1);
            $t = rand(0,1);
            $c = $t==1?strtoupper($c):$c;
            $r.= $c;
        }
        return $r;
    }
    public function editAction($id = 0)
    {
        $arr_explode = explode('/', $id);
        $id = (int) end($arr_explode);
        return $this->editRecord($id, function($post) {
            if (!is_null($post->image)) {
                $post->image = URL.'/'.$post->image;
                settype($post->id, "int");
                settype($post->order_no, "int");
                settype($post->active, "int");
                settype($post->category_id, "int");
                settype($post->deleted, "int");
            }
            return $post;
        });

    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        $v_date_created = false;
        $v_date_updated = false;
        $session = $this->auth->getIdentity();

        $v_short_name = removeVietnamseChac($filter->sanitize($data['title'], 'string'));
        $check = $this->model->findFirst('short_name = "'.$v_short_name.'"');
        if($check){
            do{
                $str_key = self::randomStr();
                $v_short_name = removeVietnamseChac($filter->sanitize($data['title'], 'string')).'-'.$str_key;
                $check = $this->model->findFirst('short_name = "'.$v_short_name.'"');
            }while($check);
        }
        if (isset($data['id'])) {
            $post = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            $v_date_updated = strtotime(date('m-d-Y'));
            if ($post) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $post = new $this->model;
            $v_date_created = strtotime(date('m-d-Y'));
            $message = 'has been created';
        }                

        $post->title = $filter->sanitize($data['title'], 'string');
        $post->keywords = str_replace(' ', ';', $filter->sanitize($data['keywords'], 'string'));
        $post->keywords = str_replace(',', ';', $post->keywords);
        $post->short_name = $v_short_name;
        
        
        $post->short_description = $filter->sanitize($data['short_description'], 'string');
        $post->description = $filter->sanitize($data['description'], 'string');
        $post->type = $filter->sanitize($data['type'], 'string');
        if(isset($data['category_id'])) $post->category_id = $filter->sanitize($data['category_id'], 'int');
        $v_category = (new \RW\Models\Categories)->findFirst($post->category_id);
        $v_category = $v_category->toArray();
        
        if($v_category && is_array($v_category)){
            $post->category_name = $v_category['name'];
        }
        $post->meta_title = $filter->sanitize($data['meta_title'], 'string');
        $post->meta_description = $filter->sanitize($data['meta_description'], 'string');
        $post->link_name = $filter->sanitize($data['link_name'], 'string');
        $post->link = $filter->sanitize($data['link'], 'string');
        $post->active = $filter->sanitize($data['active'], 'int');
        $post->order_no = $filter->sanitize($data['order_no'], 'int');
        if($v_date_created){
            $post->created_at = $v_date_created;
            $post->created_by = $session['id'];
        }
        if($v_date_updated){
            $post->updated_at = $v_date_updated;
            $post->updated_by = $session['id'];
        }

        if ($this->request->hasFiles() == true) {
            $imagePath = PUBLIC_PATH . DS . 'images' . DS . 'posts';
            if (!file_exists($imagePath)) {
                @mkdir($imagePath, 0755, true);
            }
            foreach($this->request->getUploadedFiles() as $file) {
                if (isImage($file->getType())) {
                    $fileName = $file->getName();
                    $fileExt = $file->getExtension();

                    $fileName = str_replace('.'.$fileExt, '_'.date('d-m-y').'.'.$fileExt, \Phalcon\Text::uncamelize($fileName));

                    if ($file->moveTo($imagePath . DS . $fileName)) {
                        if (isset($post->image) && file_exists(PUBLIC_PATH . DS . $post->image)) {
                            unlink(PUBLIC_PATH . DS . $post->image);
                        }
                        $post->image = 'images/posts/'.$fileName;
                    }
                    break;
                }
            }
        }
        if ($post->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'Banner <b>'.$post->name.'</b> '.$message.' successful.', 'data' => ['id' => $post->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $post->getMessages()];
        }

        return $this->response($arrReturn);
    }

    public function getOptionsCategoryAction()
    {
        $arr_list_parent = (new \RW\Models\Categories)->find([
            'conditions' => 'deleted = ?1'
            ,"bind"       => array(1 => 0)
        ]);
        $arr_list_parent = $arr_list_parent->toArray();
        $arr_option = array();
        // $arr_option [] = array("text"=>'---Select---',"value"=> 0 );
        for($i=0;$i<count($arr_list_parent);$i++){
            $arr_option [] = array("text"=>$arr_list_parent[$i]['name'],"value"=> (int) $arr_list_parent[$i]['id'] );
        }
        $arrReturn = ['error' => 0, 'data' => $arr_option ];
        return $this->response($arrReturn);
    }
}
