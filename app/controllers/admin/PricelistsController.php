<?php
namespace RW\Controllers\Admin;

class PricelistsController extends ControllerBase {

    protected $notFoundMessage = 'This baogia did not exist.';

    public function listAction()
    {   
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'title', 'description'], function($array) {            
            return $array;
        },$arr_where);
    }

    public function editAction($id = 0)
    {
        return $this->editRecord($id, function($baogia) {            
            return $baogia;
        });

    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $v_date_created = false;
        $v_date_updated = false;
        if (isset($data['id'])) {
            $baogia = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($baogia) {
                $message = 'has been updated';
                $v_date_updated = strtotime(date('m-d-Y'));
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $baogia = new $this->model;
            $message = 'has been created';
            $v_date_created = strtotime(date('m-d-Y'));
        }
        $baogia->title = $filter->sanitize($data['title'], 'string');
        $baogia->short_name = removeVietnamseChac($baogia->title);
        $baogia->description = $filter->sanitize($data['description'], 'string');
        $baogia->order_no = $filter->sanitize($data['order_no'], 'int');
        $baogia->active = $filter->sanitize($data['active'], 'int');
        $baogia->phanloai_danhmuc = $filter->sanitize($data['phanloai_danhmuc'], 'int');
        $baogia->maunen = $filter->sanitize($data['maunen'], 'string');
        $baogia->mauchu = $filter->sanitize($data['mauchu'], 'string');
        $baogia->gia = $filter->sanitize($data['gia'], 'float');
        $baogia->sonam = $filter->sanitize($data['sonam'], 'string');
        $baogia->hotro_tag = $filter->sanitize($data['hotro_tag'], 'int');
        $baogia->danhsach_tin = $filter->sanitize($data['danhsach_tin'], 'int');
        $baogia->nhieudanhsach_tin = $filter->sanitize($data['nhieudanhsach_tin'], 'int');
        $baogia->ketnoi_mangxahoi = $filter->sanitize($data['ketnoi_mangxahoi'], 'int');
        if($v_date_created){
            $baogia->created_at = $v_date_created;
        }
        if($v_date_updated){
            $baogia->updated_at = $v_date_updated;
        }
        if ($baogia->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'baogia <b>'.$baogia->title.'</b> '.$message.' successful.', 'data' => ['id' => $baogia->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $baogia->getMessages()];
        }
        return $this->response($arrReturn);
    }
}
