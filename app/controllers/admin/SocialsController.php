<?php
namespace RW\Controllers\Admin;

class SocialsController extends ControllerBase {

    protected $notFoundMessage = 'This social did not exist.';

    public function listAction()
    {
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        return $this->listRecords(['id', 'image', 'link','name'], function($array) {
            if (isset($array['image'])) {
                if (!is_null($array['image'])) {
                    $array['image'] = URL.'/'.$array['image'];
                } else {
                    $array['image'] = '';
                }
            }
            return $array;
        },$arr_where);
    }

    public function editAction($id = 0)
    {
        return $this->editRecord($id, function($social) {
            if (!is_null($social->image)) {
                $social->image = URL.'/'.$social->image;
            }
            return $social;
        });

    }

    public function updateAction()
    {
        $filter = new \Phalcon\Filter;
        $data = $this->getPost();
        $data = array_merge(['link' => ''], $data);
        if (isset($data['id'])) {
            $social = $this->model->findFirst($filter->sanitize($data['id'], 'int'));
            if ($social) {
                $message = 'has been updated';
            } else {
                return $this->error404($this->notFoundMessage);
            }
        } else {
            $social = new $this->model;
            $message = 'has been created';
        }
        $social->name = $filter->sanitize($data['name'], 'string');
        $social->short_name = removeVietnamseChac($social->name);
        $social->link = $filter->sanitize($data['link'], 'string');
        $social->type = $filter->sanitize($data['type'], 'string');
        $social->order_no = $filter->sanitize($data['order_no'], 'int');
        $social->active = $filter->sanitize($data['active'], 'int');
        if ($this->request->hasFiles() == true) {
            $imagePath = PUBLIC_PATH . DS . 'images' . DS . 'socials';
            if (!file_exists($imagePath)) {
                mkdir($imagePath, 0755, true);
            }
            foreach($this->request->getUploadedFiles() as $file) {
                if (isImage($file->getType())) {
                    $fileName = $file->getName();
                    $fileExt = $file->getExtension();

                    $fileName = str_replace('.'.$fileExt, '_'.date('d-m-y').'.'.$fileExt, \Phalcon\Text::uncamelize($fileName));

                    if ($file->moveTo($imagePath . DS . $fileName)) {
                        if (isset($social->image) && file_exists(PUBLIC_PATH . DS . $social->image)) {
                            unlink(PUBLIC_PATH . DS . $social->image);
                        }
                        $social->image = 'images/socials/'.$fileName;
                    }
                    break;
                }
            }
        }
        if ($social->save() === true) {
            $arrReturn = ['error' => 0, 'message' => 'social <b>'.$social->name.'</b> '.$message.' successful.', 'data' => ['id' => $social->getId()]];
        } else {
            $arrReturn = ['error' => 1, 'messages' => $social->getMessages()];
        }

        return $this->response($arrReturn);
    }
}
