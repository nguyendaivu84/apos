<?php
namespace RW\Controllers\Frontend;

use RW\Models\Jobs;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
class CareersController extends ControllerBase
{
    public function indexAction()
    {
    	$careers = new Jobs;
    	$columns = array('title','short_name','customer_logo','type','country','city','time_expires','created_at');
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        $arr_where [] = array('field'=>'active','parameter'=>'=','value'=>1);

        $arr_data['data'] = $this->listRecords($careers,$columns,$arr_where,'order_no asc',null,true,4,1);
        $arr_data['page'] = 1;
        $this->view->data = $arr_data;
    	$this->view->content = $this->view->partial('frontend/careers/index');
    }
    public function jobDetailAction($alias){
    	$filter = new \Phalcon\Filter;
    	$careers = Jobs::findFirst('short_name="'.$filter->sanitize($alias, 'string').'"');
    	$careers = $careers->toArray();
    	$careers['type_string'] = ucwords(str_replace('-', ' ', $careers['type']));
    	$arr_data['data'] = $careers;

    	$id = $careers['id'];

    	$careers = new Jobs;
    	$columns = array('title','short_name');
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        $arr_where [] = array('field'=>'active','parameter'=>'=','value'=>1);
        $arr_where [] = array('field'=>'id','parameter'=>'!=','value'=>$id);

        $arr_list = $this->listRecords($careers,$columns,$arr_where,'order_no asc',null,true,3);

        $arr_data['list'] = $arr_list;

    	$this->view->data = $arr_data;
    	$this->view->content = $this->view->partial('frontend/careers/details');	
    }
    public function listjobsAction(){
    	if ($this->request->isAjax()) {
			$arr_post = $this->request->getPost();
	    	$page = isset($arr_post['page']) ? $arr_post['page'] : 0;
	    	settype($page);
	    	if($page<=0) $page = 1;
	    	else $page++;
	    	$careers = new Jobs;
	    	$columns = array('title','short_name','customer_logo','type','country','city','time_expires','created_at');
	        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
	        $arr_where [] = array('field'=>'active','parameter'=>'=','value'=>1);
	        $data = '';
	        $arr_data = $this->listRecords($careers,$columns,$arr_where,'order_no asc',null,true,4,$page);

	        for($i=0;$i<count($arr_data);$i++){
	        	$data .='<div class="item-job col-md-12 '.$arr_data[$i]['city'].' '.$arr_data[$i]['type'].' '.removeVietnamseChac($arr_data[$i]['title']).'">';
					$data .='<div class="col-md-6 col-xs-6">';
						$data .='<img src="'.URL.'/'.$arr_data[$i]['customer_logo'] .'" alt="" class="company-logo-job">';
						$data .='<a href="'.URL.'/jobs/'.$arr_data[$i]['short_name'].'.html" class="name-job thick title4 black">'.$arr_data[$i]['title'] .'</a></div>';
					$data .='<div class="col-md-3 col-xs-3"><a href="#" class="location-job title4 gray">'. $arr_data[$i]['country'] .', '. $arr_data[$i]['city'] .'</a></div>';
					$data .='<div class="col-md-3 col-xs-3 text-right">';
						$data .='<a href="#" class="type-job thick title4 '.$arr_data[$i]['type'].'">'.$arr_data[$i]['type'].'</a>';
						$data .='<a href="#" class="time-job title4 gray">'.time_elapsed_string($arr_data[$i]['created_at']).'</a>';
						$data .='<meta property="og:title" class="meta-title" content="'.$arr_data[$i]['title'].'" />';
					$data .='</div>';
				$data .='</div>';
	        }        
	        $arrReturn = ['error' => 1, 'messages' => $data,'page'=>$page];
	        return $this->response($arrReturn);
    	}else{
    		$this->view->content = $this->view->partial('frontend/blocks/errors');die;
    	}    	
    }
}