<?php
namespace RW\Controllers\Frontend;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use RW\Models\Menus;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

use RW\Models\Banners;
use RW\Models\Socials;

class ControllerBase extends Controller
{
    protected $response;

    public function initialize()
    {
        // echo $this->security->hash(123456); die;
        $this->response = new \Phalcon\Http\Response;
    }
    public function listRecords($model,$columns = [],$arr_where = array(),$order='', $arrayHandle = null,$to_array=true,$limit = 1000000,$pageNumber=false)
    {
        $filter = new \Phalcon\Filter;
        $data = array_merge([
                'search'     => [],
                'pagination' => [                    
                    'sort'       => 'asc',
                    'sortName'   => 'id'
                ]
            ], $this->getPost());
        $conditions = [];
        $bind = [];
        foreach($data['search'] as $fieldName => $value) {
            if (is_numeric($value)) {
                if (is_int($value)) {
                    $value = $filter->sanitize($value, 'int');
                } else if (is_float($value)) {
                    $value = $filter->sanitize($value, 'float');
                }
                $conditions[] = "{$fieldName}= :{$fieldName}:";
                $bind[$fieldName] = $value;
            } else if (is_string($value)) {
                $value = $filter->sanitize($value, 'string');
                $conditions[] = "{$fieldName} LIKE :{$fieldName}:";
                $bind[$fieldName] = '%'.$value.'%';
            }
        }
        $conditions = implode(' AND ', $conditions);
        
        if(!empty($arr_where)){
            foreach($arr_where as $key => $val){
                if($conditions ==""){
                    $conditions .= $val['field'] . ' ' . $val['parameter'] . ' ' . $val['value'];
                }else{
                    if(isset($val['or'])) $conditions = "(".$conditions.")". ' OR ' . $val['field'] . ' ' . $val['parameter'] . ' ' . $val['value'];
                    else $conditions .= ' AND ' . $val['field'] . ' ' . $val['parameter'] . ' ' . $val['value'];
                }
            }
        } 
        $order = $order=='' ? 'id desc' : $order;
        if($pageNumber){
            $offset = ceil( ($pageNumber-1) * $limit);
            $data = $model->find([
                'conditions' => $conditions,
                'bind'       => $bind,
                'order'      => $order,
                'columns'    => $columns,
                'limit'      => $limit,
                'offset'     => $offset
            ]);            
        }else{
            $data = $model->find([
                'conditions' => $conditions,
                'bind'       => $bind,
                'order'      => $order,
                'columns'    => $columns,
                'limit'      => $limit                
            ]);
        }

        if ($data) {
            if($to_array){
                $data = $data->toArray();
                $arrayHandleCallable = is_callable($arrayHandle);
                foreach($data as $key => $value) {
                    if ($arrayHandleCallable) {
                        $value = $arrayHandle($value);
                    }
                    $data[$key] = $value;
                }
            }            
        } else {
            $data = [];
        }
        return $data;
    }
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();
        $this->view->baseURL = URL;
        $this->view->theme = THEME;
        $this->view->CONTENTS = "";
        $menu = new Menus();
        $this->view->menus = $menu->getTree();        
        if(isset($_SESSION['home-contents'])){            
            $this->view->CONTENTS = $_SESSION['home-contents'];
        }
        $this->view->COMPANY_ADDRESS = COMPANY_ADDRESS;
        $this->view->COMPANY_PHONE = COMPANY_PHONE;
        $this->view->SYSTEM_EMAIL = SYSTEM_EMAIL;
        $this->view->SYSTEM_HOTLINE = SYSTEM_HOTLINE;
        $this->view->SYSTEM_TITLE = SYSTEM_TITLE;
        $this->view->SYSTEM_COPYRIGHT = SYSTEM_COPYRIGHT;
        $this->view->SYSTEM_LOGO = SYSTEM_LOGO;
        $this->view->DEMO_LINK = DEMO_LINK;        

        if ($this->session->has("top-banner")) {
            $this->view->BANNER_IMAGE = $this->session->get("top-banner");            
        }else{
            $arr_banner = Banners::find('position = 4 AND deleted = 0');
            $arr_banner_list = array();
            foreach($arr_banner as $arr){
                $arr_banner_list [] = URL.'/'.$arr->image;
            }
            $this->view->BANNER_IMAGE = $arr_banner_list;
            $this->session->set("top-banner",$this->view->BANNER_IMAGE);
        }
        if ($this->session->has("bottom-social")) {
            $this->view->SOCIAL_NETWORKS = $this->session->get("bottom-social");            
        }else{
            $arr_social = Socials::find('active = 1 AND deleted = 0');
            $arr_social_list = array();
            foreach($arr_social as $arr){
                $arr_social_list [] = array(
                    'image'=>$arr->image,
                    'link'=>$arr->link,
                    'name'=>$arr->name,
                    'type'=>$arr->type,
                );
            }
            $this->view->SOCIAL_NETWORKS = $arr_social_list;
            $this->session->set("bottom-social",$this->view->SOCIAL_NETWORKS);
        }
    }
    
    public function test(){
        $to = "hunglmkpc044@gmail.com";
        $title ="test";
        $name="hunglam";
        $params = "this is a test";

        $mailSettings = $this->config->mail;
        $smtp =  Swift_SmtpTransport::newInstance(
            $mailSettings->smtp->server,
            $mailSettings->smtp->port,
            $mailSettings->smtp->security
        )
        ->setUsername($mailSettings->smtp->username)
        ->setPassword($mailSettings->smtp->password);        
        try{
            $message = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom($mailSettings->fromEmail)
                ->setTo($to)
                ->setBody("hunglam");
            $smtp->send($message);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }        
    }
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {          
         if (!$this->request->isAjax()) {
            $this->assets
                    ->collection('css')
                    ->addCss('/bower_components/bootstrap/dist/css/bootstrap.min.css')
                    ->addCss('/'.THEME.'css/reset.css')
                    ->addCss('/'.THEME.'css/newsletter.css')
                    ->addCss('/'.THEME.'css/custom.css')
                    ->setSourcePath(PUBLIC_PATH)
                    ->setTargetPath(PUBLIC_PATH.DS.THEME.'/css/app.min.css')                    
                    ->setTargetUri('/'.THEME.'css/app.min.css')
                    ->join(true)
                    ->addFilter(new \Phalcon\Assets\Filters\Cssmin());
             $this->assets
                    ->collection('js')
                    ->addJs('/bower_components/jquery/dist/jquery.min.js')
                    ->addJs('/bower_components/jquery-popup-overlay/jquery.popupoverlay.js')
                    ->addJs('/'.THEME.'js/owl.carousel.min.js')
                    ->addJs('/'.THEME.'js/run.js')
                    ->setSourcePath(PUBLIC_PATH)
                    ->setTargetPath(PUBLIC_PATH.DS.THEME.'/js/app.min.js')
                    ->setTargetUri('/'.THEME.'js/app.min.js')
                    ->join(true)
                    ->addFilter(new \Phalcon\Assets\Filters\Jsmin());            
            $this->view->setViewsDir($this->view->getViewsDir() . '/frontend/');

        } 
    }

    final function getPost()
    {
        $postJSON = $this->request->getRawBody();
        if (!empty($postJSON)) {
            $postJSON = json_decode($postJSON, true);
        } else {
            $postJSON = [];
        }
        return array_merge($postJSON, $this->request->getPost());
    }


    final function response($responseData = [], $responseCode = 200, $responseMessage = '', $responseHeader= [])
    {
        $this->view->disable();
        $this->response->setContentType('application/json', 'UTF-8')
                            ->setStatusCode($responseCode, $responseMessage)
                            ->setJsonContent($responseData);
        if (!empty($responseHeader)) {
            foreach($responseHeader as $headerName => $headerValue) {
                $this->response->setHeader($headerName, $headerValue);
            }
        }
        return $this->response;
    }

    protected function error404($message = 'Page not found')
    {
        return $this->response(['error' => 1, 'message' => $message], 404, $message);
    }

}
