<?php
namespace RW\Controllers\Frontend;

use RW\Models\Categories;
use RW\Models\Banners;
use RW\Models\Contacts;
use RW\Models\Posts;
use RW\Models\Customers;
use RW\Models\Users;
use RW\Models\DemoUsers;
use RW\Models\Menus;
use RW\Models\Newsletters;
use RW\Models\Pricelists;
use RW\Models\Comments;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
class IndexController extends ControllerBase
{
    public function indexAction()
    {
        // lay danh sach id cua cac category hien thi trang chu

        $category = new Categories;
        $columns_cate = array('name','short_name','id','type');
        $arr_where_cate [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        $arr_where_cate [] = array('field'=>'type','parameter'=>'=','value'=>'"bai-viet-trang-chu"');

        $arr_categories = $this->listRecords($category,$columns_cate,$arr_where_cate);

        $v_list_categories = '(';
        $arr_run = array();
        for($i=0;$i<count($arr_categories);$i++){
            if($i==count($arr_categories)-1) $v_list_categories .= $arr_categories[$i]['id'];
            else $v_list_categories .= $arr_categories[$i]['id'].',';
            $arr_run [$arr_categories[$i]['id']] = $arr_categories[$i]['type'];
        }        
        $v_list_categories .=')';

        // end
        
        // list users support
        $Users = new Users;
        $columns_user = array('fullname','skype_id','thumb','phone');
        $arr_where_user [] = array('field'=>'support','parameter'=>'=','value'=>1);
        $arr_users = $this->listRecords($Users,$columns_user,$arr_where_user);        
        // end
        
        $columns = array('title','type','category_id','short_name','link','link_name','image','meta_title','meta_description','short_description','order_no','descoration','description');
        $arr_where [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
        $arr_where [] = array('field'=>'category_id','parameter'=>'in','value'=>$v_list_categories);
        $arr_where [] = array('field'=>'type','parameter'=>'=','value'=>'"home"','or'=>true);
        $order='order_no asc';
        $post = new Posts;
        $arr_data = $this->listRecords($post,$columns,$arr_where,$order);

        $arr_data = array(
            'data'=>$arr_data,
            'support_list'=>$arr_users,
            'count'=>count($arr_data)
        );
        $this->view->data = $arr_data;
        
        $this->view->content = $this->view->partial('frontend/home/index');
    }

    public function ErrorsAction(){
        $this->view->content = $this->view->partial('frontend/blocks/errors');
    }
    public function detailAction($link){
        $menus = new Menus;
        $menus_field = array('id','category_id');
        $arr_where_menus [] = array('field'=>'link','parameter'=>'=','value'=>'"'.$link.'"');
        $arr_menus = $this->listRecords($menus,$menus_field,$arr_where_menus);
        if(!empty($arr_menus)){
            $category_id = $arr_menus[0]['category_id'];
            $post = new Posts;
            $post_field = array('id','short_name','description','category_name','category_link','meta_title','meta_description','title','keywords');
            $arr_where_post [] = array('field'=>'category_id','parameter'=>'=','value'=>$category_id);
            $arr_where_post [] = array('field'=>'active','parameter'=>'=','value'=>1);
            $arr_where_post [] = array('field'=>'deleted','parameter'=>'=','value'=>0);
            $arr_post = $this->listRecords($post,$post_field,$arr_where_post);
            if(empty($arr_post)) self::ErrorsAction();

             $arr_data = array(
                'data'=>$arr_post
            );
            $this->view->data = $arr_data;

            $this->view->content = $this->view->partial('frontend/pages/details');
        }else{
            self::ErrorsAction();
        }
    }

    public function loginAction(){
        if($this->request->isPost()){
            $arr_return = [
                'error'=>1,
                'message'=>0,
                'link_demo'=>''
            ];
            $email = $this->request->hasPost('email')?$this->request->getPost('email'):'';
            $password = $this->request->hasPost('password')?$this->request->getPost('password'):'';
            $this->view->disable();
            $demo_user = DemoUsers::findFirst(array(
                                "email = :email: AND password = :password:",
                                'bind' => array('email' => $email, 'password' => md5($password))
                        ));
            if($demo_user){
                 $db_name = 'demo_user_'.$demo_user->id;
                 $info = getInfo();
                 $arr_return['error']=0;
                 $arr_return['link_demo'] = $info['link_demo']."?token=".str_rot13(base64_encode($db_name));
            }else{
                $arr_return['message']='Sai email hoặc mật khẩu';
            }
            return $this->response($arr_return);
        }else{
            return $this->response->redirect('/');
        }
    }
    public function createUserAction(){
        if($this->request->isPost()){
            $arr_return = [
                'error'=>1,
                'message'=>'',
                'link_demo'=>''
            ];
            $store_name = $this->request->hasPost('store_name')?$this->request->getPost('store_name'):'';
            $name = $this->request->hasPost('name')?$this->request->getPost('name'):'';
            $phone = $this->request->hasPost('phone')?$this->request->getPost('phone'):'';
            $email = $this->request->hasPost('email')?$this->request->getPost('email'):'';
            $password = $this->request->hasPost('password')?$this->request->getPost('password'):'';
            $type_shop = $this->request->hasPost('type_shop')?$this->request->getPost('type_shop'):'shop';
            $this->view->disable();
            $demo_user = DemoUsers::findFirst(array(
                                "email = :email: AND password = :password:",
                                'bind' => array('email' => $email, 'password' => md5($password))
                        ));
            if($demo_user){
                $arr_return['message']='Email này đã được đăng ký';
            }else{
                $demo_user_new = new DemoUsers();
                $demo_user_new->store_name = $store_name;
                $demo_user_new->name = $name;
                $demo_user_new->phone = $phone;
                $demo_user_new->email = $email;
                $demo_user_new->password = md5($password);
                if($demo_user_new->save()){
                    $db_name = 'demo_user_'.$demo_user_new->id;
                    $info = getInfo();
                    // Create connection
                    $conn = new \mysqli($info['db_server'], $info['da_admin'], $info['da_password']);
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 
                    // Create database
                    $sql = "CREATE DATABASE $db_name";
                    if ($conn->query($sql) === TRUE) {
                        
                        
                        // Grant normal user
                        if($info['db_user']!=''){
                            $conn->query("GRANT ALL ON $db_name.* TO '".$info['db_user']."'@'localhost'");
                        }
                        // Import data base
                        // $cmd = $info['mysql_path']." --host=".$info['db_server']." --user=".$info['da_admin']." --password=".$info['da_password']." -D $db_name -e 'source ".PUBLIC_PATH.DS.$type_shop.".sql"."'"
                        $cmd = $info['mysql_path']." -u ".$info['da_admin']." -p".$info['da_password']." $db_name < ".PUBLIC_PATH.DS.$type_shop.".sql";
                        proc_open();
                        exec($cmd);
                        sleep(3);
                        $arr_return['cmd'] = $info['mysql_path']." -u ".$info['da_admin']." -p".$info['da_password']." $db_name < ".PUBLIC_PATH.DS.$type_shop.".sql";

                        $arr_return['error']=0;
                    }else{
                        $arr_return['message']='Lỗi tạo dữ liệu';
                    }
                    // Link to demo with token
                    $arr_return['link_demo'] = $info['link_demo']."?token=".str_rot13(base64_encode($db_name));
                }else{
                    $arr_return['message']='Lỗi tạo user';
                }
                return $this->response($arr_return);
            }
        }else{
            return $this->response->redirect('/');
        }
    }
}
