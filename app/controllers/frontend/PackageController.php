<?php
namespace RW\Controllers\Frontend;

use RW\Models\Menus;
use RW\Models\Pricelists;
use RW\Models\PackageDetails;
use RW\Models\Packages;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
class PackageController extends ControllerBase
{
    public function detailPriceAction($alias)
    {    	
        $alias = 'chi-tiet/'.$alias.'.html';
        $menus = Menus::findFirst('link = "'.$alias . '"');
        if($menus){
            $project_list_id = $menus->pricelist_id;            
            $pricelist = Pricelists::findFirst($project_list_id);
            $arr_packages = Packages::find('pricelist_id='.$project_list_id);
            $arr_packages = $arr_packages->toArray();
            for($i=0;$i<count($arr_packages);$i++){
                $arr_options = PackageDetails::find('active = 1 and deleted = 0 and packaged_id = '.$arr_packages[$i]['id']);
                $arr_options = $arr_options->toArray();                
                $arr_packages[$i]['options'] = $arr_options;
            }
            $arr_data = array(
                'one'=>$pricelist->toArray(),
                'data'=>$arr_packages
            );
            $this->view->data = $arr_data;
            $this->view->content = $this->view->partial('frontend/pages/details');
        }else{
            $this->view->content = $this->view->partial('frontend/blocks/errors');die;
        }    	
    }
}