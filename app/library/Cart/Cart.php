<?php
namespace RW\Cart;

use Phalcon\Mvc\User\Component;

class Cart extends Component
{

    private $cart;

    public function __construct()
    {
        $this->cart = $this->session->has('cart') ? $this->session->get('cart') : [];
        if (empty($this->cart)) {
            $this->cart = $this->getDefaul();
        }
    }

    private function getDefaul()
    {
        $company_default = \RW\Models\JTCompany::getCompanyDefault();
        return [
                'items' => [],
                'total' => 0,
                'quantity' => 0,
                'taxper' => $company_default['tax'],
                'taxkey' => $company_default['taxkey'],
                'tax' => 0,
                'main_total' => 0,
                'note'  => '',
                'order_id'  => '',
                'order_type'  => 0
            ];
    }

    public function buildItems($items)
    {
        if (!is_array($items)) {
            return false;
        }
        $this->cart['items'] = $items;
        return $this->rebuild();
    }

    public function add($data)
    {
        $data = array_merge(['_id' => '', 'options' => []], $data);
        $cartKey = md5((string)$data['_id'].serialize($data['options']));
        if (isset($this->cart['items'][$cartKey])) {
            $quantity = $this->cart['items'][$cartKey]['quantity'] + $data['quantity'];
            $priceData = \RW\Models\JTProduct::getPrice([
                                            '_id'   => $data['_id'],
                                            'sizew' => 0,
                                            'sizeh' => 0,
                                            'quantity'  => $quantity,
                                            'companyId' => '',
                                            'options'   => $data['options'],
                                            'fields'    => ['name', 'description']
                                        ]);
            $this->update($cartKey, [
                            '_id'       => new \MongoId($data['_id']),
                            'name'      => $priceData['name'],
                            'description'=> $priceData['description'],
                            'note'      => $data['note'],
                            'image'     => $data['image'],
                            'quantity'  => $quantity,
                            'options'   => $data['options'],
                            'total' => $priceData['sub_total']
                        ]);
        } else {
            $this->cart['items'][$cartKey] = $data;
        }
        $this->rebuild();
        return $cartKey;
    }

    public function update($cartKey, $data)
    {
        if (!isset($this->cart['items'][$cartKey])) {
            throw new Exception("The item \"{$cartKey}\" did not exist.");
        }
        $this->cart['items'][$cartKey] = array_merge($this->cart['items'][$cartKey], $data);
        $this->rebuild();
        return $this->get();
    }

    private function updateTotalQuantity()
    {
        $this->cart['total'] = $this->cart['quantity'] = 0;
        foreach ($this->cart['items'] as $item) {
            $this->cart['total'] += $item['total'];
            $this->cart['quantity'] += $item['quantity'];
        }
        $this->cart['total'] =  round((float)$this->cart['total'],2);
        if(!isset($this->cart['taxper'])){
            $this->cart['taxper'] = 0;
        }
        $this->cart['tax'] =  $this->cart['total']*$this->cart['taxper']/100;
        $this->cart['main_total'] = round((float)($this->cart['total']*(1+$this->cart['taxper']/100)),2);
        $this->cart['tax'] = $this->cart['main_total'] - $this->cart['total'];
    }

    private function rebuild($cart = [])
    {
        if (!empty($cart)) {
            $this->cart = $cart;
        }
        $this->updateTotalQuantity();
        return $this->session->set('cart', $this->cart);
    }

    public function get($cartKey = '')
    {
        if (!empty($cartKey)) {
            if (!isset($this->cart['items'][$cartKey])) {
                throw new Exception("The item \"{$cartKey}\" did not exist.");
            }
            return $this->cart['items'][$cartKey];
        }
        return $this->cart;
    }

    public function getItems()
    {
        return $this->cart['items'];
    }

    public function getTotal()
    {
        return $this->cart['total'];
    }

    public function getQuantity()
    {
        return $this->cart['quantity'];
    }

    public function getDetailTotal()
    {
        return array(   'total' => $this->cart['total'],
                        'taxper' => $this->cart['taxper'],
                        'tax' => $this->cart['tax'],
                        'main_total' => $this->cart['main_total']
                    );
    }

    public function updateNote($note = '')
    {
        $this->cart['note'] = $note;
        return $this->rebuild();
    }

    public function updateTax($taxper=0){
        $this->cart['taxper'] = $taxper;
        return $this->rebuild();
    }
    public function setCart($data){
        $this->rebuild($data);
    }
    public function setOrderType($type=0){
        $this->cart['order_type'] = $type;
        return $this->rebuild();
    }
    public function remove($cartKey)
    {
        if (isset($this->cart['items'][$cartKey])) {
            unset($this->cart['items'][$cartKey]);
            return $this->rebuild();
        }
        throw new Exception("The item \"{$cartKey}\" did not exist.");
    }

    public function destroy()
    {
        return $this->rebuild($this->getDefaul());
    }
}
