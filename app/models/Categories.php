<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class Categories extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $image;
    public $position;
    public $order_no;
    

    public function getSource()
    {
        return 'categories';
    }

    public function validation()
    {
        $this->validate(
            new PresenceOf(
                array(
                    'field'    => 'name',
                    'message'  => 'Name is required.'
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }
}
