<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class Comments extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;
    

    public function getSource()
    {
        return 'comments';
    }
}
