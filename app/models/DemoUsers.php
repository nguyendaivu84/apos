<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class DemoUsers extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;
    

    public function getSource()
    {
        return 'demo_user';
    }
}
