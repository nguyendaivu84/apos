<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class Jobs extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;
    

    public function getSource()
    {
        return 'jobs';
    }
}
