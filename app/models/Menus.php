<?php
namespace RW\Models;
use Phalcon\Session\Adapter\Files;
class Menus extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var text
     */
    public $link;

    /**
     *
     * @var string
     */
    public $group_name;

    /**
     *
     * @var integer
     */
    public $parent_id;

    /**
     *
     * @var integer
     */
    public $order_no;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'menus';
    }

    public  function getMenuHeader()
    {
        $arrReturn = [];
        $menus = $this->find([
            // 'conditions' => 'group_name = "header"',
            'conditions' => 'active = 1"',
            'columns'    => ['id', 'name', 'link', 'group_name'],
            'order'      => 'order_no ASC'
        ]);
        if ($menus) {
            foreach ($menus as $menu) {
                $arrReturn[] = $menu->toArray();
            }
        }
        return $arrReturn;
    }
    public function getParent()
    {
        $arrReturn = [];

        $menus = $this->find([
            'conditions' => 'parent_id = 0',
            'columns'    => ['id','pricelist_id', 'name','category_id', 'link', 'group_name', 'parent_id', 'order_no','image','contents','displayhtml'],
            'order'      => 'order_no ASC'
        ]);

        $arrReturn = [];

        if ($menus) {
            foreach ($menus as $menu) {
                $arrReturn[$menu->group_name][] = $menu->toArray();
            }

            foreach ($arrReturn as $type => $menu) {
                array_unshift($arrReturn[$type], ["id" => "0", "name" => "*Root"]);
            }
        }

        return $arrReturn;
    }

    public function getTree()
    {
        $menus = $this->find([
            'columns'   => ['id','pricelist_id', 'name','category_id', 'link', 'group_name', 'parent_id', 'order_no','image','contents','displayhtml'],
            'order'     => 'order_no ASC'
        ]);

        $arrReturn = [];

        if ($menus) {
            foreach ($menus as $menu) {                
                $arrReturn[$menu->group_name][] = (array)$menu;//$menu->toArray();
            }

            foreach ($arrReturn as $group => $menus) {                
                $arrReturn[$group] = self::setMenu($menus);
            }

        }
        return $arrReturn;
    }

    private static function setMenu($menu)
    {
        $session = new \Phalcon\Session\Adapter\Files();
        $arrMenu = [];
        foreach($menu as $value){
            if($value['name']=='home' && ($value['link']=='/' || $value['link']=='' )  ) {                
                settype($value['displayhtml'], 'int');                                
                if($value['displayhtml']){    
                    $_SESSION['home-contents'] = $value['contents'];
                }else{                    
                    if($session->has("home-html")){
                        $session->remove("home-html");
                    }
                }
            }else{
                $arrMenu[$value['parent_id']][$value['id']]=$value;
            }            
        }
        return $arrMenu;
    }

    public static function findFirstByLink($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
