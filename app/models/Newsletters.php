<?php
namespace RW\Models;
class Newsletters extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    
    public function getSource()
    {
        return 'newsletter';
    }
}
