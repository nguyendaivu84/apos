<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class PackageDetails extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;
    

    public function getSource()
    {
        return 'package_details';
    }
}
