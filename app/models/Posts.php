<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class Posts extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */    
    

    public function getSource()
    {
        return 'post';
    }
    public static function findFirstByShortName($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    public function validation()
    {
        $this->validate(
            new PresenceOf(
                array(
                    'field'    => 'title',
                    'message'  => 'Image is required.'
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }
}
