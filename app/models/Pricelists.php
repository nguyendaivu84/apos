<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class Pricelists extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;
    

    public function getSource()
    {
        return 'pricelist';
    }

    public function validation()
    {
        $this->validate(
            new PresenceOf(
                array(
                    'field'    => 'title',
                    'message'  => 'Key is required.'
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }
}
