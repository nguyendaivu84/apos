<?php
namespace RW\Models;

use Phalcon\Mvc\Model\Validator\PresenceOf;

class Socials extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $image;

    /**
     *
     * @var string
     */
    public $link;
    public $order_no;
    

    public function getSource()
    {
        return 'social';
    }

    public static function findFirstImageByType($parameters = null)
    {
        $social =  parent::findFirst($parameters);
        return $social->image;
    }
}
