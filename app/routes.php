<?php
$router = new Phalcon\Mvc\Router(false);

$router->removeExtraSlashes(true);

$router->add('/create-user', array(
    'namespace'     => 'RW\Controllers\Frontend',
    'controller'    => 'Index',
    'action'        => 'createUser',
));
$router->add('/login', array(
    'namespace'     => 'RW\Controllers\Frontend',
    'controller'    => 'Index',
    'action'        => 'login',
));

foreach(['','/'] as $mod) {

    $nsp = 'RW\Controllers\Frontend';
    $router->add($mod.'/:params.html', array(
        'namespace'     => $nsp,
        'controller'    => 'Index',
        'action'        => 'detail',
        'params'        => 1
    ));
}
$router->add('', array(
    'namespace'     => 'RW\Controllers\Frontend',
    'controller'    => 'Index',
    'action'        => 'index',
));
$router->add('/', array(
    'namespace'     => 'RW\Controllers\Frontend',
    'controller'    => 'Index',
    'action'        => 'index',
));

// ==================

foreach(['Admin'] as $module) {
    $uri = strtolower($module);
    $router->add('/'. $uri, array(
        'namespace'     => 'RW\Controllers\\'.$module,
        'controller'    => 'Index',
        'action'        => 'index',
    ));

    $router->add('/'. $uri .'/:controller', array(
        'namespace'     => 'RW\Controllers\\'. $module,
        'controller'    => 1
    ))->convert('controller', function ($controller) {
        return \Phalcon\Text::camelize($controller);
    });

    $router->add('/'. $uri .'/:controller/:action', array(
        'namespace'     => 'RW\Controllers\\'. $module,
        'controller'    => 1,
        'action'        => 2
    ))->convert('controller', function ($controller) {
        return \Phalcon\Text::camelize($controller);
    })->convert('action', function ($action) {
        return lcfirst(\Phalcon\Text::camelize($action));
    });

    $router->add('/'. $uri .'/:controller/:action/:params', array(
        'namespace'     => 'RW\Controllers\\'. $module,
        'controller'    => 1,
        'action'        => 2,
        'paramsList'        => 3
    ))->convert('controller', function ($controller) {
        return \Phalcon\Text::camelize($controller);
    })->convert('action', function ($action) {
        return lcfirst(\Phalcon\Text::camelize($action));
    });

}

return $router;