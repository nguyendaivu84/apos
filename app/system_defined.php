<?php 
defined('SYSTEM_EMAIL') || define('SYSTEM_EMAIL', 'worktraq@anvydigital.com'); 
defined('SYSTEM_HOTLINE') || define('SYSTEM_HOTLINE', '0976723656'); 
defined('SYSTEM_TITLE') || define('SYSTEM_TITLE', 'Anvylabs'); 
defined('COMPANY_ADDRESS') || define('COMPANY_ADDRESS', '25 Nguyen Hong, tp.HCM, Q.Binh Thanh'); 
defined('COMPANY_PHONE') || define('COMPANY_PHONE', '(08)3 551 0912'); 
defined('SYSTEM_COPYRIGHT') || define('SYSTEM_COPYRIGHT', '© 2016 Anvy Digital. All rights reserved.'); 
defined('SYSTEM_LOGO') || define('SYSTEM_LOGO', '/themes/frontend/images/logo.png'); 
