<?php
//=================================================================================
defined('DS')               || define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH')         || define('APP_PATH', realpath('.'));
defined('PUBLIC_PATH')      || define('PUBLIC_PATH', APP_PATH . DS .'public');
defined('PHAMTOM_CONVERT')  || define('PHAMTOM_CONVERT', APP_PATH.DS.'phantomjs'.DS.'rasterize.js');
defined('THEME')            || define('THEME','themes/frontend/');
//=================================================================================
$info = getInfo();
defined('DEBUG')    || define('DEBUG', $info['debug']);
defined('URL')      || define('URL', $info['url']);
defined('DB')    || define('DB', $info['db']);
defined('DEFAULT_CURRENCY')    || define('DEFAULT_CURRENCY', 'VNĐ');
defined('_POS_PAGE_SIZE_')    || define('_POS_PAGE_SIZE_', 100);

function to_array($arr){
    // return (array)$arr;
    try{    
        return $arr->toArray();
    }catch(Exception $ex){
        return (array)$arr;
    }
}
function randomStr($number = 6){
    $v_chars = '0123456789';
    $l = strlen($v_chars)-1;
    $r = '';
    for($i=0;$i<$number;$i++){
        $p = rand(0,$l);
        $c = substr($v_chars,$p,1);
        $t = rand(0,1);
        $c = $t==1?strtoupper($c):$c;
        $r.= $c;
    }
    return $r;
}
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'năm',
        'm' => 'tháng',
        'w' => 'tuần',
        'd' => 'ngày',
        'h' => 'giờ',
        'i' => 'phút',
        's' => 'giây',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' trước' : 'mới đăng';
}
function converType($str = ''){
    $arr = array(
        'full-time'=>'Toàn thời gian',
        'part-time'=>'Bán thời gian',
        'internship'=>'Thực tập',
        'freelance'=>'Làm tư do',
        'freelancer'=>'Làm tư do',
        'temporary'=>'Làm thời gian ngắn',
        ''=>'N/A'
    );
    return $arr[$str];
}
function getInfo()
{
    $serverName = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
    $arrInfo = [];
    $arrConfig = [
        'pos.anvylabs.com'  => [
            'url'         => 'http://pos.anvylabs.com',
            'db'          => 'anvylabs2',
            'db_server'   => 'localhost',
            'da_admin'    => 'da_admin',
            'da_password' => '7QWkbz4IRk',
            'mysql_path'  => '/usr/local/mysql/bin/mysql',
            'db_user'     => 'anvylabs_demo',
            'link_demo'   => 'http://demo.anvylabs.com'
        ],
        'quangcao.local'  => [
            'url'         => 'http://quangcao.local',
            'db'          => 'anvylabs2',
            'db_server'   => 'localhost',
            'da_admin'    => 'root',
            'da_password' => '123456',
            'mysql_path'  => 'F:\openserver\modules\database\MySQL-5.6-x64\bin\mysql',
            'db_user'     => 'kami',
            'link_demo'   => 'http://demo.quangcao.local'
        ]
    ];
    if (php_sapi_name() === 'cli') {        
        if( DIRECTORY_SEPARATOR == '\\' ) {
            $arrInfo = $arrConfig['quangcao.local'];
            define('DEMO_LINK', 'http://smshop.local/session/quickRegister/');
        } else {
            $arrInfo = $arrConfig['pos.anvylabs.com'];
            define('DEMO_LINK', 'http://demo.anvylabs.com/session/quickRegister/');
        }
    } else {
        $arrInfo = $arrConfig[$serverName];
        if($serverName=='pos.anvylabs.com') define('DEMO_LINK', 'http://demo.anvylabs.com/session/quickRegister/');
        else define('DEMO_LINK', 'http://smshop.local/session/quickRegister/');
    }
    if (in_array($serverName, ['quangcao.local', ''])) {
        $arrInfo['debug'] = true;
    } else {
        $arrInfo['debug'] = true;
    }
    return $arrInfo;
}

function pr($data){
    echo '<pre>';print_r($data);echo '</pre>';
}
function display_format_currency($p_number,$number = 2){
    return number_format((double)$p_number,$number).' '.DEFAULT_CURRENCY;
}
//Display currency
function format_currency($num, $afterComma = -1){
    if(is_string($num))
        $num = str_replace(',', '', $num);
    $num = (float)$num;
    if($afterComma == -1)
        $afterComma = $_SESSION['format_currency'];
    $num = round($num,$afterComma);
    return number_format($num,$afterComma);
}
//Undisplay currency
function unformat_currency($str){
    $str = explode(".",$str);
    $dec = str_replace(",","",$str[0]);
    $dec = (int)$dec;
    if(isset($str[1]))
        $per = (int)$str[1];
    else
        $per = 0;
    $per = $per/1000;
    return $dec+$per;
}
function isImage($mime){
    return in_array($mime, ['image/jpeg', 'image/jpg', 'image/png', 'image/gif']);
}
function slug($string, $separator = '-'){
    $title = \Patchwork\Utf8::toAscii($string);
    $flip = $separator == '-' ? '_' : '-';
    $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
    $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));
    $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);
    return trim($title, $separator);
}
// Sort mảng theo giá trị key, hàm đơn giản
function aasort(&$array=array(), $key='',$order=1,$isResetKey = false) {
    $sorter=array();
    $ret=array();
    if(is_array($array) && count($array)>0){
        reset($array);
        foreach ($array as $ii => $va) {
            if(!isset($va[$key])) continue;
            $sorter[$ii]=$va[$key];
        }
    }
    if($order==1)
        asort($sorter);
    else
        arsort($sorter);
    if(!$isResetKey)
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
    else
        foreach ($sorter as $ii => $va) {
            $ret[]=$array[$ii];
        }
    $array=$ret;
    return $array;
}
// Sort mảng theo giá trị key, cho phép theo nhiều cách sort_flags
function msort($array, $key,$order=1,$sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            if($order==1)
                asort($mapping, $sort_flags);
            else
                arsort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

function GroupToKey($str){
    $str = explode(".",$str);
    $str = end($str);
    $str = strtolower(str_replace(" ","_",trim($str)));
    return $str;
}
function GroupToName($str){
    $str = explode(".",$str);
    $str = end($str);
    return $str;
}
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
function _checkIsset($data = '',$default=''){
    return (isset($data)?$data:$default);
}
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function removeVietnamseChac($p_fragment , $p_lower = true,$doc = false){
    $translite_simbols = array (
        '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
        '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
        '#(ì|í|ị|ỉ|ĩ)#',
        '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
        '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
        '#(ỳ|ý|ỵ|ỷ|ỹ)#',
        '#(đ)#',
        '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
        '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
        '#(Ì|Í|Ị|Ỉ|Ĩ)#',
        '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
        '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
        '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
        '#(Đ)#',
        "/[^a-zA-Z0-9\-\.\_\'\"\ ]/",
    ) ;
    $replace = array (
        'a',
        'e',
        'i',
        'o',
        'u',
        'y',
        'd',
        'A',
        'E',
        'I',
        'O',
        'U',
        'Y',
        'D',
        '-',
    ) ;
    $v_fragment = preg_replace($translite_simbols, $replace, $p_fragment);
    if(!$doc) $v_fragment = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $v_fragment);
    else $v_fragment = preg_replace(array('/[^a-z0-9.]/i', '/[-]+/') , '-', $v_fragment);
    $v_temp = $p_lower?strtolower($v_fragment):$v_fragment;        
    $v_temp = preg_replace('/\+s/','-',$v_temp);
    return $v_temp;
}