<div class="container">
    <div class="block-content bottom40 bg-white"></div> 
    <style>
        .nav li a{
            color:#333 ;
        }
        .error404-category-title{
            text-align:center;
            font-weight:700;
            font-size:24px;
        }
    </style>
    <div class="block-content top50" style="width:100%;">
        <div class="error404-banner" style="min-height:500px;">
            <div class="error404-banner-top"></div>
            <div class="error404-banner-bottom"></div>
            <div class="error404-banner-content">
                <div class="col-md-3">
                    <img src="/img/super.png" alt="Super Tiki" class="img-responsive">
                </div>
                <div class="col-md-3" style="padding-top:15%">
                    <img src="/img/404.png" alt="Super Tiki" class="img-responsive">
                </div>
                <div class="error404-banner-content-item error404-said" style="padding-top:5%">
                    <h1 style="font-size:26px;">Xin lỗi, trang bạn đang tìm kiếm <br class="hidden-xs">không tồn tại!</h1>
                </div>
            </div>
        </div>       
    </div>
</div>