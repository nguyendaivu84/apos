<!-- 
<div class="footer full-block dark-grey">
    <div class="web-inner">
       <div class="footer-content">
          {% for index, menu in menus['footer'][0] %}            
            {% if menus['footer'][menu['id']] is defined %}

              <div class="col-sm-6 col-md-3">
                 <div id="footer_info" class="primary-sidebar widget-area" role="complementary">
                    <h3>{{menu['name']}}</h3>
                    <div class="{{menu['link']}}-container">
                       <ul id="{{menu['link']}}" class="menu">
                         {% for child in menus['footer'][menu['id']] %}
                            <li id="menu-item-{{menu['id']}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-424 current_page_item menu-item-505">
                              <a href="{{child['link']}}.html">{{child['name']}}</a>
                            </li>
                         {% endfor%}
                       </ul>
                    </div>
                 </div>
              </div>
            {% else %}
               <li id="menu-item-{{menu['id']}}" class="menu-item menu-item-type-custom menu-item-object-custom {% if router.getRewriteUri() == child['link']%}current-menu-item current_page_item{%endif%} menu-item-home menu-item-{{menu['id']}}">
                  <a href="/{{menu['link']}}.html">{{menu['name']}}</a>
               </li>
            {% endif %}

            

         {% endfor %}         
         
       </div>
    </div>
 </div>
 -->
<!-- address footer -->
<div class="footer full-block dark-grey">
   <div class="web-inner">
      <!-- <div style="margin: 0 22px 30;border-top: 1px solid #555"> </div> -->
      <div class="footer-content">
         <div class="col-sm-6">
            <div class="col-sm-6">
               <img src="{{baseURL}}/public/{{SYSTEM_LOGO}}" title="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN " alt="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN " height="48" width="126">
               <div><small>Phần mềm quản lý bán hàng</small></div>
               <div><small>SIÊU ĐƠN GIẢN</small></div>
            </div>
            <div class="col-sm-6">
               <div>Địa chỉ:<small> <i>{{COMPANY_ADDRESS}}</i></small></div>
               <div>Tổng đài:<small> <b>{{COMPANY_PHONE}} </b> </small></div>
            </div>
         </div>
         <div class="col-sm-6">
            <a class="social" href="https://www.youtube.com/channel/UCqXq0i5Mp16m2PnRI1Z66oQ">
            <i class="fa fa-youtube  bigger-200"></i>
            </a>
            <a class="social" href="https://www.facebook.com/">
            <i class="fa fa-facebook  bigger-200"></i>
            </a>
            <a class="social" href="http://blog./">
            <i class="fa fa-rss bigger-200"></i>
            </a>
         </div>
      </div>
   </div>
</div>
<div id="fadeandscale" class="well" style="display:none;min-width:450px">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="panel panel-default panel-information" style="border:none !important">
         <div class="panel-heading">
            <h3 class="panel-title text-center" style="font-size:22px">Thông tin cửa hàng</h3>
         </div>
         <div class="panel-body" style="padding-top:0;background-color: #f5f5f5;">
            <form onsubmit="return false;" class="form-horizontal" id="apos_register" method="post" role="form" novalidate="novalidate">
               <div id="div_error" class="alert alert-danger hidden" role="alert">
                <ul id="display"></ul>
               </div>
               <div id="loadingReg" class="hidden text-center">
                  <b class="text-succsess">Đang khởi tạo</b><br/>
                  <img src="/public/themes/frontend/images/loading.gif" alt="" style="width:50px;">
               </div>
               <div class="form-group">
                  <input class="form-control" required data-val-required="Chưa nhập tên cửa hàng" id="ShopName" name="ShopName" placeholder="Nhập tên cửa hàng" type="text" value="">
                  <i class="icon-home"></i>
               </div>              
               <div class="form-group">
                  <input class="form-control" data-val="true" required data-val-required="Chưa nhập tên" id="FullName" name="FullName" placeholder="Nhập tên của bạn" type="text" value="">
                  <i class="icon-user"></i>
               </div>
               <div class="form-group">
                  <input autocomplete="off" class="form-control" required data-val="true" data-val-required="Chưa nhập số điện thoại" id="PhoneNumber" name="PhoneNumber" placeholder="Nhập số điện thoại" type="text" value="">
                  <i class="icon-phone"></i>
               </div>
               <div class="form-group">
                  <input class="form-control" id="Email" name="Email" required placeholder="Nhập email của bạn" type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" value="">
                  <i class="icon-envelope"></i>
               </div>
               <div class="form-group">
                  <input autocomplete="new-password" class="form-control" data-val="true" data-val-required="Chưa nhập mật khẩu" id="Password" name="Password" placeholder="Nhập mật khẩu" type="password" value="">
                  <i class="icon-lock"></i>                  
               </div>
               <div class="form-group">
                  <input autocomplete="new-password" class="form-control" data-val="true" data-val-equalto="Xác nhận mật khẩu sai" data-val-equalto-other="*.Password" data-val-required="Chưa xác nhận mật khẩu" id="RepeatPassword" name="RepeatPassword" placeholder="Nhập lại mật khẩu" type="password" value="">
                  <i class="icon-retweet"></i>                  
                </div>
               <div class="form-group clearfix center">
                  <button type="submit" class="btn btn-warning" onclick="dangkyapos()">Đăng Ký</button>
                  <button class="fadeandscale_close btn btn-danger">Đóng</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div id="loginpopup" class="well" style="display:none;min-width:450px">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="panel panel-default panel-information" style="border:none !important">
         <div class="panel-heading">
            <h3 class="panel-title text-center" style="font-size:22px">&nbsp;</h3>
         </div>
         <div class="panel-body" style="padding-top:0;background-color: #f5f5f5;">
            <form onsubmit="return false;" class="form-horizontal" id="apos_register" method="post" role="form" novalidate="novalidate">
               <div id="div_errorlogin" class="alert alert-danger hidden" role="alert">
                <ul id="displaylogin"></ul>
               </div>
               <div id="loadinglogin" class="hidden text-center">
                  <b class="text-succsess">Đang đăng nhập</b><br/>
                  <img src="/public/themes/frontend/images/loading.gif" alt="" style="width:50px;">
               </div>
               <div class="form-group">
                  <input class="form-control" id="EmailLogin" name="Email" required placeholder="Nhập email của bạn" type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" value="">
                  <i class="icon-envelope"></i>
               </div>
               <div class="form-group">
                  <input autocomplete="new-password" class="form-control" data-val="true" data-val-required="Chưa nhập mật khẩu" id="PasswordLogin" name="Password" placeholder="Nhập mật khẩu" type="password" value="">
                  <i class="icon-lock"></i>                  
               </div>
               <div class="form-group clearfix center">
                  <button type="submit" class="btn btn-warning" onclick="dangnhapapos()">Đăng nhập</button>
                  <button class="loginpopup_close btn btn-danger">Đóng</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<input id="demo_link" value="{{DEMO_LINK}}" type="hidden" />
<style>
    #fadeandscale {
      transform: scale(0.8);
    }
    .popup_visible #fadeandscale {
      transform: scale(1);
    }
    #loginpopup {
      transform: scale(0.8);
    }
    .popup_visible #loginpopup {
      transform: scale(1);
    }
    .navbar{
      border-bottom: none !important;
    }
    #btn-scroll-down{
      width:65px !important;
    }
</style>