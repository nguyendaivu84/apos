<div id="navbar" class="navbar navbar-default fixed">
         <div class="navbar-container web-inner" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
            <span class="sr-only">Toggle sidebar</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>            
            </button>
            <ul class="nav nav-list dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">               
               {% for index, menu in menus['header'][0] %}
                 <li role="presentation">
                     <a role="menuitem" tabindex="-1" href="/{{menu['link']}}">{{menu['name']}}</a>
                  </li>
               {% endfor %}
               <li role="presentation">
               <a role="menuitem" tabindex="-1" class="initialism fadeandscale_open" name="dangkytopm" href="#fadeandscale">Đăng ký</a></li>
            </ul>
            <div class="">
               <div class="navbar-header pull-left">       
                  <a class="navbar-brand" href="/">
                  <img src="{{baseURL}}/public/{{SYSTEM_LOGO}}" height="48" width="126" alt="" style="padding-top:10px;">
                  </a>		  
               </div>               
               <div class="navbar-header hidden-sm" id="top-menu">
                  <ul id="menu-menu-1" class="nav navbar-nav">                     
                     <li id="menu-item-0" class="menu-item menu-item-type-custom menu-item-object-custom {% if router.getRewriteUri()=='' or router.getRewriteUri() =='/' %}current-menu-item current_page_item{%endif%} menu-item-home menu-item-0">
                        <a href="/">Trang chủ</a>
                     </li>
                     {% for index, menu in menus['header'][0] %}                     
                        {% if menus['header'][menu['id']] is defined %}
                           <li id="menu-item-{{menu['id']}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-{{menu['id']}}">                              
                              <a href="{{menu['link']}}.html">{{menu['name']}}</a>
                              <ul class="sub-menu">
                                 {% for child in menus['header'][menu['id']] %}
                                    <li id="menu-item-{{child['id']}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-416"><a href="{{baseURL}}/{{child['link']}}.html">{{child['name']}}</a></li>
                                 {% endfor%}
                              </ul>
                           </li>
                        {% else %}            
                           <li id="menu-item-{{menu['id']}}" class="menu-item menu-item-type-custom menu-item-object-custom {% if router.getRewriteUri() == menu['link']%}current-menu-item current_page_item{%endif%} menu-item-home menu-item-{{menu['id']}}">
                              <a href="{{menu['link']}}.html">{{menu['name']}}</a>
                           </li>
                        {% endif %}
                     {% endfor %}
                  </ul>
               </div>
               <div class="navbar-header pull-right hidden-p">
                  <div class="top-reg">
                     <a name="dangkytopw" class="initialism fadeandscale_open btn btn-warning login" href="#fadeandscale">Đăng ký</a>
                     <a name="dangnhaptopw" class="initialism loginpopup_open btn btn-warning login" href="#loginpopup">Đăng nhập</a>
                  </div>
               </div>
            </div>
         </div>
      </div>