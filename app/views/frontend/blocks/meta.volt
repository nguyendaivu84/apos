<meta name="description" content="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN giúp bạn quản lý cửa hàng, xem doanh số, lợi nhuận, tồn kho qua điện thoại, máy tính bảng chỉ 149k/ tháng">
<meta name="robots" content="noodp">      
<!-- google meta tag -->
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="website">
<meta property="og:title" content="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN ">
<meta property="og:description" content="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN  giúp bạn quản lý cửa hàng, xem doanh số, lợi nhuận, tồn kho qua điện thoại, máy tính bảng chỉ 149k/ tháng">
<meta property="og:url" content="/">
<meta property="og:site_name" content="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN ">
<!-- twitter meta tag -->
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN  giúp bạn quản lý cửa hàng, xem doanh số, lợi nhuận, tồn kho qua điện thoại, máy tính bảng chỉ 149k/ tháng">
<meta name="twitter:title" content="Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN ">
<meta name="twitter:image" content="https:///wp-content/uploads/2015/09/pos-l.png">