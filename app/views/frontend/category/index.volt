<style>
.nav li a{
	color:#333 ;
}
</style>
	<div class="block-content bottom50"></div>
	<div class="block-content block-content bottom40 wp_gmaps_canvas" style="min-height:300px">
		{% set custom_position = 1 %}
		{% if data is not empty %}
			{% for item in data %}		
				{% if custom_position % 2 ==1 %}
						<div class="block-content">
							<div class="container">
								<div class="col-md-6">
									<div class="bottom20">
										<h2 class="title2">
											{{item['title']}}
										</h2>
									</div>
									<div class="bottom20 title4 thin">
										{{item['short_description']}}
									</div>
									<button class="col-md-6 btn-primary btn btn-lg animate" data-animate="bounceInLeft" onclick="document.location.href='{{baseURL}}/products/{{item['short_name']}}.html'">
										{{ item['link_name'] !='' ? item['link_name'] : 'Xem chi tiết' }}
									</button>
								</div>
								<div class="col-md-6 text-center">
									<img class="animate img-block" data-animate="bounceInRight" src="{{ baseURL }}/{{item['image']}}" alt="">
								</div>
							</div>
						</div>
				{% else %}
					<div class="block-content">
						<div class="container">
							<div class="col-md-6 text-center">
								<img class="animate img-block" data-animate="bounceInLeft" src="{{ baseURL }}/{{item['image']}}" alt="">
							</div>
							<div class="col-md-6">
								<div class="bottom20">
									<h2 class="title2">
										{{item['title']}}
									</h2>
								</div>
								<div class="bottom20 title4 thin">
									{{item['short_description']}}
								</div>
								<button class="col-md-6 btn-primary btn btn-lg animate" data-animate="bounceInRight" onclick="document.location.href='{{baseURL}}/products/{{item['short_name']}}.html'">	{{ item['link_name'] !='' ? item['link_name'] : 'Xem chi tiết' }}
								</button>
							</div>
						</div>
					</div>
				{% endif %}	
				{% set custom_position += 1 %}
			{% endfor %}
		{% else %}
			<div class="block-content">
				<div class="container" style="font-size:24px;font-weight:700">
					Danh mục chưa có bài viết. Xin vui lòng thử danh mục khác
				</div>
			</div>
		{% endif %}		
	</div>
	<a class="button-top hide" href="#top">
		<i class="glyphicon glyphicon-menu-up"></i>
	</a>