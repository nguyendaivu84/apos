<div class="banner-blog" style="background-image: url({{baseURL}}/{{data['one']['image']}})">
</div>
<div class="container">
	<style>
		.nav li a{
			color:#333 ;
		}
		#footer_container{
			display:none;
		}
	</style>
	<div class="block-content bg-white top50">
		<div class="col-md-9" >
			<div class="time-blog">
				<span class="title1 thick">
					{{date('d',strtotime(data['one']['created_at']))}}
				</span>
				<span class="title2 thick">
					{{date('M',strtotime(data['one']['created_at']))}}
				</span>
				<span class="title3 thick">
					{{date('Y',strtotime(data['one']['created_at']))}}
				</span>
			</div>
			<div class="content-blog">
				<h1 class="title2 text-center thick">{{data['one']['title']}}</h1>
				<div class="bottom20 social-share">
					<script type='text/javascript' src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js' data-shr-siteid='397236d56c0a711dd0b9ab2a9c40d8c1' data-cfasync='false' async='async'></script>
					<div class='shareaholic-canvas' data-app='share_buttons' data-app-id='24266826'></div>
				</div>
				<div id="post-blog bottom20">
					<h3 class="text-center">{{data['one']['short_description']}}.</h3>
					{{data['one']['description']}}
				</div>
				<div class="bottom20 social-share">
					<div class='shareaholic-canvas' data-app='share_buttons' data-app-id='24266826'></div>
				</div>				
			</div>
		</div>
		{{ partial('frontend/details/newarticles') }}
	</div>
	<div class="block-content bg-white">
		<div class="col-md-12">
			<div class="fb-comments" data-href="https://www.facebook.com/anvydigital" data-width="100%" data-numposts="10"></div>
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1484320888500664";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				</div>
		</div>
	</div>
	<a class="button-top hide" href="#top">
	<i class="glyphicon glyphicon-menu-up"></i>
</a>