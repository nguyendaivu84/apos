{% if data['listPosts'] is not empty  %}
	<div class="col-md-3">
		<div class="col-md-12 top20 popular-today padding0">
			<span class="title3">NEW ARTICLES</span>
			{% for item in data['listPosts'] %}
				<div class="thumb-post" style="background-image:url('{{baseURL}}/{{item['image']}}')">
					<div class="mask"></div>
					<a href="{{baseURL}}/details/{{item['short_name']}}.html">
						<strong>
							{{item['title']}}
						</strong>
					</a>
				</div>
			{% endfor %}			
		</div>
	</div>
{% endif %}