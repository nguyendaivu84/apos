<div class="full-block">
   {%if data['count'] > 1 %}
      {% set run = 1 %}
      {% for item in data['data'] %}
         {% if run == 1%}
            <div class="main full-block main-bg ">
               <div id="report" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner sm-100 web-inner header">
                     <div class="item active">
                        <div class="">
                           <div class="img-banner-info" style="margin-top: 0px;">
                              <div class="col-xs-12">
                                 <h1>{{item['title']}}</h1>
                                 {{item['short_description']}}
                              </div>
                           </div>
                           <div class="img-banner-pic">
                              <a href="/">
                                 <img class="aligncenter wp-image-43 size-full" src="{{item['image']}}" alt="phần mềm quản lý bán hàng " width="600" height="347" sizes="(max-width: 600px) 100vw, 600px">
                              </a>
                           </div>
                        </div>
                     </div>                        
                  </div>
               </div>
            </div>
            <div class="full-block clear center"> 
               <a href="javascript:;" id="btn-scroll-down" class="btn btn-primary" style="border-radius: 100%!important;width:30px; margin-top: -35px;">
               <i class="fa fa-angle-down icon-only bigger-250"></i></a>
            </div>
         {%else%}      
         <div class="full-block {{item['descoration']}}">
            <div class="web-inner features center">
               <div class="col-xs-12">
                  {% if item['image']!='' %}
                     <div class="col-sm-6">
                        <div class="bounce"><img class="aligncenter size-full wp-image-45" src="{{item['image']}}" alt="thiet-bi" width="500" height="311"  sizes="(max-width: 500px) 100vw, 500px"></div>
                     </div>
                     <div class="col-sm-6">
                        <div class="">
                           <h3 class="main-color" style="text-align: left;">{{item['title']}}</h3>
                           {{item['short_description']}}      
                        </div>
                     </div>
                  {%else%}
                     <h2>{{item['title']}}</h2>
                     {{item['short_description']}}
                  {%endif%}               
               </div>
               {{item['description']}}            
            </div>
         </div>
         {%endif%}
         {% set run = run+1 %}
      {% endfor %}
   {%else%}
      {{data['data'][0]['description']}}
   {%endif%}

   <div class="col-xs-12 more-ver light-blue center">
      <div>
         <div class="col-xs-12 mt-20"><label class="try" style="color: #fff;">Bắt đầu sử dụng với 7 ngày MIỄN PHÍ</label></div>
         <div class="col-xs-12 mt-20">            
            <a class="initialism fadeandscale_open btn btn-success btn btn-warning btn-xs-100 mt-xs-10" href="#fadeandscale">Đăng ký MIỄN PHÍ</a>
         </div>         
      </div>
   </div>
   <div class="full-block">
      <div class="web-inner features center">
      </div>
   </div>
</div>

<div class="full-block more-ver center contact grey">
   {{ partial('/frontend/home/supportlist') }}
</div>