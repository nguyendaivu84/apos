<div class="container features ">
   <h2 class=""> LUÔN SẴN SÀNG HỖ TRỢ BẠN</h2>
   <div id="contact-carousel" class="owl-carousel">
      {% for item in data['support_list'] %}
         <div>
            <div>
               <img src="{{item['thumb']}}" class="img-users">
            </div>
            <div>
               <a href="skype:{{item['skype_id']}}?chat">
                  <p class="suno-sale">{{item['fullname']}}</p>
                  <p>Tư vấn viên - {{item['phone']}}</p>
                  <img style="border: 0;" role="Button" alt="Skype chat, instant message" src="/public/themes/frontend/images/chatbutton_32px.png">
               </a>
            </div>
         </div>
      {%endfor%}      
   </div>
   <p>Ngoài ra, bạn luôn có thể gọi cho chúng tôi vào</p>
   <p>
      <strong>Tổng đài: </strong>
      {{COMPANY_PHONE}}
      <br>
   </p>
</div>