<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN</title>
   <link href="/public/css/temp/font-awesome.min.css" rel="stylesheet">
    {{ assets.outputCss() }}
    {{ partial('blocks/meta') }}
</head>
<body class="no-skin">
   {{ partial('blocks/header') }}      
   <div class="main-container">
      <div class="main-content mt-50">
         {{content()}}            
      </div>         
      {{ partial('blocks/footer') }}
      
      <a href="javascript:;" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse" style="border-radius: 100%!important;">
            <i class="fa fa-angle-up icon-only bigger-200"></i>
      </a>
   </div>
   <div id="ads"></div>
   {{ assets.outputJs() }}
</body>
</html>