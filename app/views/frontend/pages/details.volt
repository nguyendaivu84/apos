<div class="full-block">
   {% for item in data['data'] %}    
      <div class="full-block {{item['descoration']}}">
         <div class="web-inner features center">
            <div class="col-xs-12">
              	<h3 class="main-color" style="text-align: left;">{{item['title']}}</h3>
              	{{item['description']}}
            </div>            
         </div>
      </div>
   {% endfor %}
</div>