<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

/**
 * Ultility functions & constants
 */
require_once APP_PATH . "/ultility.php";

return new \Phalcon\Config(array(
    'database' => array(
            'adapter'     => 'Mysql',
            'host'        => 'localhost',
            'username'    => 'root',
            'password'    => '123456',
            'dbname'      => 'anvylabs',
            'charset'     => 'utf8',
    ),
    'databases' => array(
        'anvylabs' => array(
            'adapter'     => 'Mysql',
            'host'        => 'localhost',
            'username'    => 'root',
            'password'    => '123456',
            'dbname'      => 'anvylabs',
            'charset'     => 'utf8',
        ),
        'anvylabs' => array(
            'adapter'     => 'Mysql',
            'host'        => 'localhost',
            'username'    => 'root',
            'password'    => '123456',
            'dbname'      => 'anvylabs',
            'charset'     => 'utf8',
        )
    ),
    'application' => array(
        'appDir'         => APP_PATH . '/app/',
        'controllersDir' => APP_PATH . '/app/controllers/frontend',
        'adminControllersDir' => APP_PATH . '/app/controllers/admin',        
        'servicesControllersDir' => APP_PATH . '/app/controllers/services',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginDir'      => APP_PATH . '/app/plugin/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => URL,
        'cryptSalt'      => 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D',
        'viewCacheTime'  => 888
    ),
));
