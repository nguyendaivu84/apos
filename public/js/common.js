function sendContacts(){
	var googleResponse = $('#g-recaptcha-response').val();
	if(googleResponse===undefined){
		alert("Please wait a moment!");
	}else{
		if(googleResponse == ""){

		}		    
		else{
			params = $('form').serialize();
			$.ajax({				
				data: params
				,type:'POST'
				,url: '/send-contact.html'
				,success: function(data){
					if(data.error==0) $("#reset_btn").click();
					$("#display_message").html(data.messages);
				}
			});
		}
	}
}
function SendSubscribe(){
	var params = {'email':$("#newsletterEmail").val(),'name':$("#newsletterName").val()};
	$.ajax({				
		data: params
		,type:'POST'
		,url: '/send-subscribe.html'
		,success: function(data){
			if(data.error==1){
				$("#newsletterEmail").css('border','1px solid red');
				$("#newsletterName").css('border','1px solid red');
			}else{
				$("#newsletterEmail").css('border','');
				$("#newsletterName").css('border','');
				$("#newsletterEmail").val('');
				$("#newsletterName").val('');
			}
		}
	});
}
function collectKeywords(){
	var keywords = [];
	var places = [];
	var run = 0;
	$('.meta-title').each(function(){
		keywords[run++] = $(this).attr('content')
	});
	run = 0;
	var arr = [];
	$('.meta-city').each(function(){
		if($.inArray($(this).attr('content'), arr) === -1) {
			places[run++] = $(this).attr('content');
			arr.push($(this).attr('content'));
		}		
	});
    $( "#keywords" ).autocomplete({
      source: keywords
    });
    $( "#places" ).autocomplete({
      source: places
    });
    
}
function bokytuviet(title)
{
    var title, slug;

    slug = title.toLowerCase();

    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');

    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    
    slug = slug.replace(/ /gi, " - ");    
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    return slug;
}
function filterData(){
	var checked = [];
	var run = 0;
	var show_all = true;
	$('.filter-checkbox').each(function(){
		if(!$(this).is(":checked")){
			checked[run++] = $(this).val();
		}else{
			show_all = false
		}
	});
	if(show_all){
		$(".item-job").show();
	}else{
		for(var i=0; i< checked.length; i++) {			
		 	$("."+checked[i]).hide();
		}
	}
}
function onPlacechange(){
	var places = $("#places").val();
	places = places.replace(/ /g, "-");
	if(places==''){
		$(".item-job").show();
	}else{
		$(".item-job").hide();
		$("."+places).show();
	}
}
function onDatachange(){
	var keywords = $("#keywords").val();
	keywords = keywords.replace(/ /g, "-");
	keywords = bokytuviet(keywords);
	if(keywords==''){
		$(".item-job").show();
	}else{
		$(".item-job").hide();
		$("."+keywords).show();
	}
}
function loadmore(){
	$.ajax({				
		data: { page:$("#pageNumber").val() }
		,type:'POST'
		,url: '/show-more.html'
		,success: function(data){
			if(data.messages!='') {
				$("#pageNumber").val(data.page);
				$("#list-job").append(data.messages);
				collectKeywords();
			}
		}
	});
}