'use strict';

app.controller('BaogiasController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'Price list';
    $scope.template = appHelper.baseURL + '/tpl/baogia/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'Price list';
    $scope.action = action;
    $scope.button = {
        title: 'List',
        link: '#/app/pricelists/list'
    };
    $scope.button_add = {
            title: 'Add',
            link: '#/app/pricelists/add'
    };
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }

    if (['add', 'edit'].indexOf(action) != -1) {
        $scope.form = {
            fields: [{
                type: 'hidden',
                name: 'id'
            },{
                type: 'text',
                label: 'Title',
                name: 'title'
            },{
                type: 'text-editor',
                label: 'Description',
                name: 'description'
            }, {
                type: 'number',
                label: 'Thời hạn(năm)',
                name: 'sonam',
                value:1
            }, {
                type: 'text',
                label: 'Giá(VNĐ)',
                name: 'gia',
                value: '0.0'
            }, {
                type: 'select',
                label: 'Màu nền',
                name: 'maunen',
                options:[{value:'brow',text:'Đen'},{value:'red',text:'Đỏ'},{value:'white',text:'Trắng'},{value:'green',text:'Xanh lá'},{value:'blue',text:'Xanh biển'}],
                value:'brow'
            }, {
                type: 'select',
                label: 'Màu chữ',
                name: 'mauchu',
                options:[{value:'white',text:'Trắng'},{value:'red',text:'Đỏ'},{value:'black',text:'Đen'},{value:'green',text:'Xanh lá'},{value:'blue',text:'Xanh biển'}],
                value:'white'
            }, {
                type: 'number',
                label: 'Order',
                name: 'order_no',
                value:1
            }
            , {
                type: 'select',
                label: 'Active/Hide',
                name: 'active',
                options:[{value:0,text:'Hide'},{value:1,text:'Show'}],
                value:1
            }
            , {
                type: 'select',
                label: 'List category',
                name: 'phanloai_danhmuc',
                options:[{value:0,text:'No'},{value:1,text:'Yes'}],
            }
            , {
                type: 'select',
                label: 'Tag support',
                name: 'hotro_tag',
                options:[{value:0,text:'No'},{value:1,text:'Yes'}],
            }, {
                type: 'select',
                label: 'List news',
                name: 'danhsach_tin',
                options:[{value:0,text:'No'},{value:1,text:'Đơn giản'},{value:2,text:'Full'}],
            }, {
                type: 'select',
                label: 'Multiple web news',
                name: 'nhieudanhsach_tin',
                options:[{value:0,text:'No'},{value:1,text:'1 trang'},{value:2,text:'Nhiều trang'}],
            }, {
                type: 'select',
                label: 'Connect social networks',
                name: 'ketnoi_mangxahoi',
                options:[{value:0,text:'No'},{value:1,text:'Yes'}],
            }
        ],
            title: formTitle
        };

        if (action === 'edit') {
            var id = $stateParams.id;
            $http.get(appHelper.adminURL('pricelists/edit/' + id))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form = appHelper.populateForm($scope.form, result.data);
                    }
                    $scope.form.allLoaded = true;
                }).error(function(result) {
                    dialogs.confirm(result.message, 'Do you want to create a new one?', {
                            size: 'md'
                        })
                        .result.then(function() {
                            $location.path('/app/pricelists/add');
                        }, function() {
                            $location.path('/app/pricelists/list');
                        });
                });
        } else {
            $scope.form.allLoaded = true;
        }

        $scope.save = function() {
            // if (categoryForm.$valid) {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
            }
            $scope.upload(file, data);
            // }
        };

        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('pricelists/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/pricelists/edit/' + result.data.id);
                }
            });
        };
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 60,
            columns: [ {
                    name: 'Title',
                    field: 'title'
                }, {
                    name: 'Description',
                    field: 'description'
                }],
            list: appHelper.adminURL('pricelists/list'),
            edit: '#/app/pricelists/edit',
            delete: appHelper.adminURL('pricelists/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });
    }
}]);
