'use strict';

app.controller('CommentsController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'Price list';
    $scope.template = appHelper.baseURL + '/tpl/comments/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'Comments';
    $scope.action = action;
    $scope.button = {
        title: 'List',
        link: '#/app/comments/list'
    };
    $scope.button_add = {
            title: 'Add',
            link: '#/app/comments/add'
    };
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }
    
    if (['add', 'edit'].indexOf(action) != -1) {
        $scope.form = {
            fields: [{
                type: 'hidden',
                name: 'id'
            },{
                type: 'select',
                label: 'Customer',
                name: 'customer_id',
                options:[]
            }, {
                type: 'select',
                label: 'Màu nền',
                name: 'mau_nen',
                options:[{value:'white',text:'Trắng'},{value:'brow',text:'Đen'},{value:'red',text:'Đỏ'},{value:'black',text:'Đen'},{value:'green',text:'Xanh lá'},{value:'blue',text:'Xanh biển'}],
                value:'white'
            }, {
                type: 'select',
                label: 'Màu chữ',
                name: 'mau_chu',
                options:[{value:'white',text:'Trắng'},{value:'brow',text:'Đen'},{value:'red',text:'Đỏ'},{value:'black',text:'Đen'},{value:'green',text:'Xanh lá'},{value:'blue',text:'Xanh biển'}],
                value:'brow'
            },{
                type: 'text-editor',
                label: 'Comments',
                name: 'comments'
            }
            , {
                type: 'select',
                label: 'Active/Hide',
                name: 'active',
                options:[{value:0,text:'Hide'},{value:1,text:'Show'}],
                value:1
            }
        ],
            title: formTitle
        };

        if (action === 'edit') {
            var id = $stateParams.id;
            $http.get(appHelper.adminURL('comments/edit/' + id))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form = appHelper.populateForm($scope.form, result.data);
                    }
                    $scope.form.allLoaded = true;
                }).error(function(result) {
                    dialogs.confirm(result.message, 'Do you want to create a new one?', {
                            size: 'md'
                        })
                        .result.then(function() {
                            $location.path('/app/comments/add');
                        }, function() {
                            $location.path('/app/comments/list');
                        });
                });
        } else {
            $scope.form.allLoaded = true;
        }
        $http.get(appHelper.adminURL('comments/get-List-Customers/'))
            .success(function(result) {
            if (result.error === 0) {
                $scope.form.fields[1].options = result.data;
            }
            $scope.form.allLoaded = true;
        });
        $scope.save = function() {
            // if (categoryForm.$valid) {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
            }
            $scope.upload(file, data);
            // }
        };

        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('comments/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/comments/edit/' + result.data.id);
                }
            });
        };
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 60,
            columns: [ {
                    name: 'Customer name',
                    field: 'customer_name'
                }, {
                    name: 'Comment',
                    field: 'comments'
                }],
            list: appHelper.adminURL('comments/list'),
            edit: '#/app/comments/edit',
            delete: appHelper.adminURL('comments/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });
    }
}]);
