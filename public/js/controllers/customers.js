'use strict';

app.controller('CustomerController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'Customers';
    $scope.template = appHelper.baseURL + '/tpl/customers/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'Customers';
    $scope.action = action;
    $scope.button = {
        title: 'List',
        link: '#/app/customers/list'
    };
    $scope.button_add = {
            title: 'Add customers',
            link: '#/app/customers/add'
    };
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add customers';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit customers';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }

    if (['add', 'edit'].indexOf(action) != -1) {
        $scope.form = {
            fields: [{
                type: 'hidden',
                name: 'id'
            },{
                type: 'text',
                label: 'Name',
                name: 'name'
            },{
                type: 'text-editor',
                label: 'Description',
                name: 'description'
            }, {
                type: 'text',
                label: 'URL',
                name: 'link'
            }, {
                type: 'image-upload',
                label: 'Image',
                name: 'image',
                value: 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image',
                inputAttr: 'ngf-validate="{size: {max: \'2MB\', min: \'10B\'} }" name="file" required',
                inputStyle: 'max-width: 200px;',
                validate: {
                    requiredMessage: 'Image must not be empty!',
                    patternMessage: 'Image must be valid!',
                }
            }
            , {
                type: 'select',
                label: 'Active/Hide',
                name: 'active',
                options:[{value:0,text:'Hide'},{value:1,text:'Show'}],
                value:1
            }            
        ],
            title: formTitle
        };

        if (action === 'edit') {
            var id = $stateParams.id;
            $http.get(appHelper.adminURL('customers/edit/' + id))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form = appHelper.populateForm($scope.form, result.data);
                    }
                    $scope.form.allLoaded = true;
                }).error(function(result) {
                    dialogs.confirm(result.message, 'Do you want to create a new one?', {
                            size: 'md'
                        })
                        .result.then(function() {
                            $location.path('/app/customers/add');
                        }, function() {
                            $location.path('/app/customers/list');
                        });
                });
        } else {
            $scope.form.allLoaded = true;
        }

        $scope.save = function() {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                if ($scope.form.fields[i].name == 'image') {
                    if (action == 'add') {
                        if (!$scope.form.fields[i].file ) {
                            toaster.pop('error', 'Error', 'Image must not be empty!');
                            return false;
                        }
                    }
                    file = $scope.form.fields[i].file;
                } else {
                    data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
                }
            }
            $scope.upload(file, data);
        };

        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('customers/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/customers/edit/' + result.data.id);
                }
            });
        };
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 60,
            columns: [ {
                    name: 'Title',
                    field: 'name'
                }, {
                    name: 'Description',
                    field: 'description'
                },{
                    name: 'Image',
                    field: 'image',
                    enableSorting: false,
                    enableFiltering: false,
                    cellClass: 'text-center',
                    cellTemplate: '<img ng-if="row.entity.image" style="height: 100px;" src="{{ row.entity.image }}" class="image-thumbs" />'
                }],
            list: appHelper.adminURL('customers/list'),
            edit: '#/app/customers/edit',
            delete: appHelper.adminURL('customers/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });
    }
}]);
