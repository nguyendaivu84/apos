'use strict';

app.controller('JobsController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'Job';
    $scope.template = appHelper.baseURL + '/tpl/jobs/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'jobs';
    $scope.action = action;
    $scope.button = {
        title: 'List Job',
        link: '#/app/jobs/list'
    };
    $scope.button_add = {
        title: 'Add Job',
        link: '#/app/jobs/add'
    };
    
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add jobs';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit jobs';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }    
    if (['add', 'edit'].indexOf(action) != -1) {        
        $scope.form = {            
            fields: [{
                type: 'hidden',
                name: 'id'
            }, {
                type: 'text',
                label: 'Title',
                name: 'title'
            }, {
                type: 'select',
                label: 'Customer',
                name: 'customer_id',
                options: [],
                onchange_function:true
            }, {
                type: 'textarea',
                label: 'Short Description',
                name: 'short_description'
            }, {
                type: 'number',
                label: 'Order',
                name: 'order_no',
                value:1
            },  {
                type: 'select',
                label: 'Active',
                name: 'active',
                options: [{value:1,text:'Active'},{value:0,text:'Inactive'}],
                value : 1
            }
            , {
                type: 'select',
                label: 'Type',
                name: 'type',
                options: [
                    {value:'freelance',text:'Freelance'}
                    ,{value:'full-time',text:'Full Time'}
                    ,{value:'internship',text:'Internship'}
                    ,{value:'part-time',text:'Part Time'}
                    ,{value:'temporary',text:'Temporary'}
                ],
                value:"temporary"
            }
            , {
                type: 'select',
                label: 'Country',
                name: 'country',
                options: [
                    {value:'VietNam',text:'Việt Nam'}
                    ,{value:'Singapore',text:'Singapore'}
                ],
                value:"VietNam"
            }
            , {
                type: 'select',
                label: 'City',
                name: 'city',
                options: [
                    {value:'HCMC',text:'HCM'}
                    ,{value:'HNC',text:'HaNoi'}
                ],
                value:"HCMC"
            }, {
                type: 'date-picker',
                label: 'Date',
                name: 'time_expires'

            }, {
                type: 'text-editor',
                label: 'Description',
                name: 'description'
            }
            ],
            title: formTitle
        };
       
        try{
            $http.get(appHelper.adminURL('jobs/get-options-customers'))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form.fields[2].options = result.data;                    
                    }            
                    $scope.form.allLoaded = true;
                    // $scope.loadDatepicker();
                }            
            );
            if (action === 'edit') {
                var id = $stateParams.id;
                $http.get(appHelper.adminURL('jobs/edit/' + id))
                    .success(function(result) {
                        if (result.error === 0) {                            
                            $scope.form = appHelper.populateForm($scope.form, result.data);
                            $scope.form.fields[2].value = result.data.customer_id;
                            $scope.form.fields[5].value = result.data.active;
                            $scope.form.fields[6].value = result.data.type;
                            $scope.form.fields[7].value = result.data.country;
                            $scope.form.fields[8].value = result.data.city;
                        }
                        $scope.form.allLoaded = true;
                    }).error(function(result) {
                        
                    });
            }
        }catch(e){

        }
        $scope.save = function() {
            // if (categoryForm.$valid) {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                if ($scope.form.fields[i].name == 'image') {                    
                    file = $scope.form.fields[i].file;
                } else {
                    data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
                }
            }
            $scope.upload(file, data);
            // }
        };
        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('jobs/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/jobs/edit/' + result.data.id);
                }
            });
        };
        
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 50,
            columns: [
                {
                    name: 'Title',
                    field: 'title',
                    enableCellEdit: false,
                    ngcellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">{{ row.entity.title}}</div>'
                },{
                    name: 'Short Description',
                    field: 'short_description'
                },{
                    name: 'Customer',
                    field: 'customer_logo',
                    enableSorting: false,
                    enableFiltering: false,
                    cellClass: 'text-center',
                    cellTemplate: '<img ng-if="row.entity.customer_logo" style="height: 100px;" src="{{ row.entity.customer_logo }}" class="image-thumbs" />'
                }
            ],
            list: appHelper.adminURL('jobs/list'),
            edit: '#/app/jobs/edit',
            delete: appHelper.adminURL('jobs/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });        
    }   
}]);