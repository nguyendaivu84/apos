'use strict';

app.controller('MenuController', ['$rootScope', '$scope', '$http', 'toaster', 'dialogs', '$compile', function($rootScope, $scope, $http, toaster, dialogs, $compile) {
    $rootScope.title = 'Menu';
    $scope.baseURL = appHelper.baseURL;
    $scope.button_add = {
        title: 'Add Menus'
    };

    $scope.menu = {
        html: '',
        group: 'header',
        parent: {},
        serialize:{},
        parentOptions: [],
        categories: [],
        pricelist:[],
        form: {
            id: 0,
            name: '',
            pricelist_id:0,
            group_name: 'header',
            link: '',
            parent_id: 0,
            order_no: 1,
            image:'',
            contents:'',
            displayhtml:0,
            category_id:0
        }
    };

    $http.get(appHelper.adminURL('menus'))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.menu.parent = result.data.parent;
                        $scope.categories = result.data.category;
                        $scope.pricelist = result.data.pricelists;
                        if ($scope.menu.parent.header && $scope.menu.form.group_name=='header') {
                            $scope.menu.parentOptions = $scope.menu.parent.header;
                             for(var index  in $scope.menu.parentOptions){
                                parseInt($scope.menu.parentOptions[index]['id'],10);
                             }
                        }else{
                            $scope.menu.parentOptions = $scope.menu.parent.footer;
                             for(var index  in $scope.menu.parentOptions){
                                parseInt($scope.menu.parentOptions[index]['id'],10);
                             }
                        }
                    }
                    $scope.menu.allLoaded = true;
                    var parent = result.data.parent;
                    var tree = result.data.tree;
                    var html_header='';
                    for(var i=1;i<parent['header'].length;i++){
                          var menu = parent['header'][i];
                          if(menu['image']=="" || !menu['image'] || menu['image']==null) menu['image'] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                          html_header+='<li style="cursor:pointer" class="dd-item dd3-item" data-id="'+menu['id']+'">';
                          html_header+=              '<div class="dd-handle dd3-handle">Drag</div>';
                          html_header+=              '<div class="dd3-content" ng-click="edit_menu($event)" data-category_id="'+menu['category_id']+'" data-pricelist_id="'+menu['pricelist_id']+'"  data-image="'+menu['image']+'" data-displayhtml="'+menu['displayhtml']+'" data-id="'+menu['id']+'" data-name="'+menu['name']+'" data-group-name="'+menu['group_name']+'" data-link="'+menu['link']+'" data-parent-id="'+menu['parent_id']+'" data-order="'+menu['order_no']+'">'+menu['name']+'</div>';

                          if(tree['header'][menu['id']]){
                                html_header+='<ol class="dd-list">';
                                for(var index in tree['header'][menu['id']]){
                                    var s_menu = tree['header'][menu['id']][index];
                                    if(s_menu['image']=="" || !s_menu['image'] || menu['image']==null ) s_menu['image'] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                        html_header+='<li style="cursor:pointer" class="dd-item dd3-item" data-id="'+s_menu['id']+'">';
                                        html_header+=              '<div class="dd-handle dd3-handle">Drag</div>';
                                        html_header+=              '<div class="dd3-content" ng-click="edit_menu($event)" data-category_id="'+s_menu['category_id']+'"  data-pricelist_id="'+s_menu['pricelist_id']+'"  data-image="'+s_menu['image']+'" data-displayhtml="'+s_menu['displayhtml']+'" data-id="'+s_menu['id']+'" data-name="'+s_menu['name']+'" data-group-name="'+s_menu['group_name']+'" data-link="'+s_menu['link']+'" data-parent-id="'+s_menu['parent_id']+'" data-order="'+s_menu['order_no']+'">'+s_menu['name']+'</div>';
                                        html_header+='</li>';
                                }
                                html_header+='</ol>';
                          }
                          html_header+='</li>'
                    }
                    $scope.menu.header=html_header;
                    var html_footer='';
                    for(var i=1;i<parent['footer'].length;i++){
                          var menu = parent['footer'][i];
                          if(menu['image']=="" || !menu['image'] || menu['image']==null) menu['image'] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                          html_footer+='<li style="cursor:pointer" class="dd-item dd3-item" data-id="'+menu['id']+'">';
                          html_footer+=              '<div class="dd-handle dd3-handle">Drag</div>';
                          html_footer+=              '<div class="dd3-content" ng-click="edit_menu($event)" data-category_id="'+menu['category_id']+'"  data-pricelist_id="'+menu['pricelist_id']+'"  data-image="'+menu['image']+'" data-id="'+menu['id']+'" data-displayhtml="'+menu['displayhtml']+'" data-name="'+menu['name']+'" data-group-name="'+menu['group_name']+'" data-link="'+menu['link']+'" data-parent-id="'+menu['parent_id']+'" data-order="'+menu['order_no']+'">'+menu['name']+'</div>';

                          if(tree['footer'][menu['id']]){
                                html_footer+='<ol class="dd-list">';
                                for(var index in tree['footer'][menu['id']]){
                                    var s_menu = tree['footer'][menu['id']][index];
                                    if(s_menu['image']=="" || !s_menu['image'] || menu['image']==null ) s_menu['image'] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                        html_footer+='<li style="cursor:pointer" class="dd-item dd3-item" data-id="'+s_menu['id']+'">';
                                        html_footer+=              '<div class="dd-handle dd3-handle">Drag</div>';
                                        html_footer+=              '<div class="dd3-content" ng-click="edit_menu($event)" data-category_id="'+s_menu['category_id']+'" data-pricelist_id="'+s_menu['pricelist_id']+'" data-image="'+s_menu['image']+'" data-displayhtml="'+s_menu['displayhtml']+'" data-id="'+s_menu['id']+'" data-name="'+s_menu['name']+'" data-group-name="'+s_menu['group_name']+'" data-link="'+s_menu['link']+'" data-parent-id="'+s_menu['parent_id']+'" data-order="'+s_menu['order_no']+'">'+s_menu['name']+'</div>';
                                        html_footer+='</li>';
                                }
                                html_footer+='</ol>';
                          }
                          html_footer+='</li>'
                    }
                    $scope.menu.footer=html_footer;
                });  
    
    $scope.changeGroup = function(group) {
        $scope.menu.group = group;
        if ($scope.menu.parent[ group ]) {
            $scope.menu.parentOptions = $scope.menu.parent[ group ];
             for(var index  in $scope.menu.parentOptions){
                parseInt($scope.menu.parentOptions[index]['id'],10);
            }
        } else {
            $scope.menu.parentOptions = [{
                id: '0',
                name: '*Root'
            }];
        }
        $scope.menu.allLoaded = true;
        
    };
    $scope.uploadImage = function(obj){
      var formData = new FormData();
      formData.append('file', $('#image')[0].files[0]);      
      $.ajax({
         url : '/admin/menus/uploadImage',
         type : 'POST',
         data : formData,
         processData: false, 
         contentType: false, 
         success : function(data) {
            data = JSON.parse(data);            
            $("#image_display").attr('src',appHelper.baseURL+data.image);
            $scope.menu.form.image = data.image;
         }
      });
    };
    $scope.changeOrder = function() {
        console.log(1212);
    };
   $scope.edit_menu = function(obj){
        var that = obj.currentTarget;

        $http.get(appHelper.adminURL('menus/get-contents/'+that.getAttribute("data-id")))
            .success(function(result) {              
              CKEDITOR.instances.contents.setData(result.data);
              $scope.menu.form = {
                  id: parseInt(that.getAttribute("data-id"),10),
                  name: that.getAttribute("data-name"),
                  group_name: that.getAttribute("data-group-name"),
                  link: that.getAttribute("data-link"),
                  parent_id: parseInt(that.getAttribute("data-parent-id"),10),
                  order_no: parseInt(that.getAttribute("data-order"),10),
                  image: that.getAttribute("data-image"),
                  contents: result.data,
                  displayhtml: (parseInt(that.getAttribute("data-displayhtml"),10) ? true :false),
                  category_id : parseInt(that.getAttribute("data-category_id"),10),
                  pricelist_id : parseInt(that.getAttribute("data-pricelist_id"),10),
              };
              $scope.changeGroup($scope.menu.form.group_name);              
              $scope.menu.form.parent_id = parseInt(that.getAttribute("data-parent-id"),10);                            
        });
   }   

    $scope.reset = function() {
        CKEDITOR.instances.contents.setData("");
        $scope.menu.form = {
            id: 0,
            name: '',
            group_name: 'header',
            link: '',
            parent_id: 0,
            order_no: 1,
            image: "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image",
            contents: "",
            displayhtml: false,
            category_id:0,
            pricelist_id:0
        };
        $scope.changeGroup($scope.menu.form.group_name);
    }

    $scope.reorder = function() {
        serialize_nestable($scope);
        var data = JSON.stringify($scope.menu.serialize);        
        $http.post(appHelper.adminURL('menus/reorder'),data)
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.reload();
                        toaster.pop('success', 'Message', 'Reorder success');
                    }
                });
    };
    $scope.reload = function(){
          $http.get(appHelper.adminURL('menus'))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.menu.parent = result.data.parent;
                        if ($scope.menu.parent.header && $scope.menu.form.group_name=='header') {
                            $scope.menu.parentOptions = $scope.menu.parent.header;
                        }else{
                            $scope.menu.parentOptions = $scope.menu.parent.footer;
                        }
                    }
                    $scope.menu.allLoaded = true;
                    var parent = result.data.parent;
                    var tree = result.data.tree;
                    var html_header='';
                    for(var i=1;i<parent['header'].length;i++){
                          var menu = parent['header'][i];
                          html_header+='<li class="dd-item dd3-item" data-id="'+menu['id']+'">';
                          html_header+=              '<div class="dd-handle dd3-handle">Drag</div>';
                          html_header+=              '<div class="dd3-content" data-id="'+menu['id']+'" ng-click="edit_menu($event)" data-category_id="'+menu['category_id']+'" data-image="'+menu['image']+'" data-name="'+menu['name']+'" data-group-name="'+menu['group_name']+'" data-link="'+menu['link']+'" data-parent-id="'+menu['parent_id']+'" data-order="'+menu['order_no']+'">'+menu['name']+'</div>';

                          if(tree['header'][menu['id']]){
                                html_header+='<ol class="dd-list">';
                                for(var index in tree['header'][menu['id']]){
                                    var s_menu = tree['header'][menu['id']][index];
                                        html_footer+='<li style="cursor:pointer" class="dd-item dd3-item" data-id="'+s_menu['id']+'">';
                                        html_header+='<li  class="dd-item dd3-item" data-id="'+s_menu['id']+'">';
                                        html_header+=              '<div class="dd-handle dd3-handle">Drag</div>';
                                        html_header+=              '<div class="dd3-content" data-id="'+s_menu['id']+'" ng-click="edit_menu($event)" data-category_id="'+s_menu['category_id']+'" data-image="'+s_menu['image']+'" data-id="'+s_menu['id']+'" data-name="'+s_menu['name']+'" data-group-name="'+s_menu['group_name']+'" data-link="'+s_menu['link']+'" data-parent-id="'+s_menu['parent_id']+'" data-order="'+s_menu['order_no']+'">'+s_menu['name']+'</div>';
                                        html_header+='</li>';
                                }
                                html_header+='</ol>';
                          }
                          html_header+='</li>'
                    }
                    $scope.menu.header=html_header;
                    var html_footer='';
                    for(var i=1;i<parent['footer'].length;i++){
                          var menu = parent['footer'][i];
                                        html_footer+='<li style="cursor:pointer" class="dd-item dd3-item" data-id="'+s_menu['id']+'">';
                          html_footer+='<li class="dd-item dd3-item" data-id="'+menu['id']+'" >';
                          html_footer+=              '<div class="dd-handle dd3-handle">Drag</div>';
                          html_footer+=              '<div class="dd3-content" ng-click="edit_menu($event)"  data-pricelist_id="'+menu['pricelist_id']+'"  data-category_id="'+menu['category_id']+'" data-displayhtml = "'+menu['displayhtml']+'" data-image="'+menu['image']+'" data-id="'+menu['id']+'" data-name="'+menu['name']+'" data-group-name="'+menu['group_name']+'" data-link="'+menu['link']+'" data-parent-id="'+menu['parent_id']+'" data-order="'+menu['order_no']+'">'+menu['name']+'</div>';

                          if(tree['footer'][menu['id']]){
                                html_footer+='<ol class="dd-list">';
                                for(var index in tree['footer'][menu['id']]){
                                    var s_menu = tree['footer'][menu['id']][index];
                                        html_footer+='<li class="dd-item dd3-item" data-id="'+s_menu['id']+'">';
                                        html_footer+=              '<div class="dd-handle dd3-handle">Drag</div>';
                                        html_footer+=              '<div class="dd3-content" ng-click="edit_menu($event)" data-category_id="'+s_menu['category_id']+'"  data-pricelist_id="'+s_menu['pricelist_id']+'"  data-displayhtml = "'+s_menu['displayhtml']+'" data-image="'+s_menu['image']+'" data-id="'+s_menu['id']+'" data-name="'+s_menu['name']+'" data-group-name="'+s_menu['group_name']+'" data-link="'+s_menu['link']+'" data-parent-id="'+s_menu['parent_id']+'" data-order="'+s_menu['order_no']+'">'+s_menu['name']+'</div>';
                                        html_footer+='</li>';
                                }
                                html_footer+='</ol>';
                          }
                          html_footer+='</li>'
                    }
                    $scope.menu.footer=html_footer;
                });
    }
    $scope.save = function() {        
        $scope.menu.form.contents = CKEDITOR.instances.contents.getData();      
        $scope.menu.form.category_id = $("#category_id option:selected").val();
        $scope.menu.form.pricelist_id = $("#pricelist_id option:selected").val();
        $http.post(appHelper.adminURL('menus/update'), $scope.menu.form)
              .success(function(result) {
                  if (result.error === 0) {
                      $scope.menu.parent = result.data.parent;
                      if ($scope.menu.parent[ $scope.menu.group ]) {
                          $scope.menu.parentOptions = $scope.menu.parent[ $scope.menu.group ];
                      }
                      $scope.menu.form.id = result.data.id;
                      toaster.pop('success', 'Message', result.message);
                      $scope.reset();
                      $scope.reload();
                  } else if (result.error === 1) {
                      toaster.pop('error', 'Error', result.messages.join('<br />'));
                  }
              });
    };
}]);

angular.module('app')
    .directive('nestable', ['$compile', function($compile){
        return {
            restrict: 'A',
            require: 'ngModel',
            compile: function(element){
                return function(scope, element, attrs, $ngModel) {
                    scope.$watch(
                        function(scope) {
                            return scope.$eval(attrs.nestable);
                        },
                        function(value) {
                            element.html(value);
                            var node = element.parent().parent();
                            $compile(element.contents())(scope);
                            createNestable();
                            createCkeditor();
                            serialize_nestable(scope);
                            node.bind('change', function() {
                                serialize_nestable(scope);
                            });
                        }
                    );
                };
            }
        }
    }]);    

angular.module('app').
  directive('tabs', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {},
      controller: [ "$scope", function($scope) {
        var panes = $scope.panes = [];
 
        $scope.select = function(pane) {
          angular.forEach(panes, function(pane) {
            pane.selected = false;
          });
          pane.selected = true;
        }
 
        this.addPane = function(pane) {
          if (panes.length == 0) $scope.select(pane);
          panes.push(pane);
        }
      }],
      template:
        '<div class="tabbable col-md-12">' +
          '<ul class="nav nav-tabs">' +
            '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">'+
              '<a href="" ng-click="select(pane)">{{pane.title}}</a>' +
            '</li>' +
          '</ul>' +
          '<div class="tab-content" ng-transclude></div>' +
        '</div>',
      replace: true
    };
  }).
  directive('pane', function() {
    return {
      require: '^tabs',
      restrict: 'E',
      transclude: true,
      scope: { title: '@' },
      link: function(scope, element, attrs, tabsCtrl) {
        tabsCtrl.addPane(scope);
      },
      template:
        '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
        '</div>',
      replace: true
    };
  })
    function createCkeditor(){
       // CKEDITOR.basePath = appHelper.adminURL('../bower_components/ckeditor/');
      try{        
        CKEDITOR.basePath = appHelper.adminURL('../bower_components/ckeditor/');
        CKEDITOR.editorConfig = function( config ) {          
          config.toolbarGroups = [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'forms' },
            { name: 'tools' },
            { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others' },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
            { name: 'styles' },
            { name: 'colors' },
            { name: 'about' }
          ];
          config.removeButtons = 'Underline,Subscript,Superscript';
          
          config.removeDialogTabs = 'image:advanced;link:advanced';
          config.allowedContent = true;
          config.extraAllowedContent = '*(*);*{*};style';
          config.enterMode = CKEDITOR.ENTER_BR;
        };
        CKEDITOR.replace('contents',{});
      }catch(e){
      }
    }
   function createNestable(){
        $("#list_header").nestable({maxDepth:2});
        $("#list_footer").nestable({maxDepth:2});
    };

    function serialize_nestable($scope){
           $(".dd").each(function(){
             var id    = $(this).attr('id');
             var type = id.replace("list_","");
             var array = {};
             var value = $(this).nestable('serialize');
             for(var index in value){
                array[index] = {};
                for(var index2 in value[index]){
                    if(index2 != 'children' && index2 !='$scope'){
                      array[index][index2]=value[index][index2];
                    }
                    if(value[index]['children']){
                      array[index]['children'] = {};
                      for(var index3 in value[index]['children']){
                        array[index]['children'][index3]={};
                        for(var index4 in value[index]['children'][index3]){
                            if(index4 != 'children' && index4 !='$scope'){
                                array[index]['children'][index3][index4]=value[index]['children'][index3][index4];
                            }
                        }
                      }
                    }
                }
             }
             if(type=='header'){
                  $scope.menu.serialize.header = JSON.stringify(array);
             }else{
                  $scope.menu.serialize.footer = JSON.stringify(array);
             }
            

           })
    };
