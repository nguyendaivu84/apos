'use strict';
function deleteoption(id){
    var url = document.URL;
    url = url.split("#");
    url = url[0]+'/packages/deleteOptionList/'+id;    
    $.ajax({                
        data: ''
        ,type:'POST'
        ,url: url
        ,success: function(data){
           data = JSON.parse(data);
           if(data.error==0){
                $("#option_"+id).remove();
           }
        }
    });
}
app.controller('PackagesController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'package';
    $scope.template = appHelper.baseURL + '/tpl/packages/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'packages';
    $scope.action = action;
    $scope.button = {
        title: 'List package',
        link: '#/app/packages/list'
    };
    $scope.button_add = {
        title: 'Add package',
        link: '#/app/packages/add'
    };
    
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add packages';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit packages';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }
    $scope.adddetails = function(){
        if($("#no_option").length){
            $("#no_option").remove();
        }
        var data = {};
        var option_name = $("#option_name").val();
        if(option_name==''){
            $("#option_name").attr('placeholder','option name cannot be empty');
            $("#option_name").css('border','1px solid red');
            return;
        }
        $("#option_name").css('border','');
        var option_price = $("#option_price").val();
        data['name'] = option_name;
        data['price'] = option_price;
        data['id'] = $stateParams.id;
        var config = {
            url: appHelper.adminURL('packages/update-option-list'),
            fields: data
        };
        Upload.upload(config).success(function(result, status, headers, config) {
            $("#option_list").append(result.data);
        });

    };
    if (['add', 'edit'].indexOf(action) != -1) {        
        $scope.form = {            
            fields: [{
                type: 'hidden',
                name: 'id'
            }, {
                type: 'text',
                label: 'Name',
                name: 'name'
            },  {
                type: 'select',
                label: 'Project',
                name: 'pricelist_id',
                options: [],
                value : 1
            }, {
                type: 'textarea',
                label: 'Description',
                name: 'description'
            },  {
                type: 'select',
                label: 'Active',
                name: 'active',
                options: [{value:1,text:'Active'},{value:0,text:'Inactive'}],
                value : 1
            }             
            ],
            title: formTitle
        };
       
        try{
            $http.get(appHelper.adminURL('packages/get-list-option'))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form.fields[2].options = result.data;                    
                    }            
                    $scope.form.allLoaded = true;
                    // $scope.loadDatepicker();
                }            
            );
            if (action === 'edit') {
                var id = $stateParams.id;
                $http.get(appHelper.adminURL('packages/edit/' + id))
                    .success(function(result) {
                        if (result.error === 0) {                            
                            $scope.form = appHelper.populateForm($scope.form, result.data);
                            $scope.form.fields[2].value = result.data.pricelist_id;
                            $scope.form.fields[4].value = result.data.active;                            
                        }
                        $scope.form.allLoaded = true;
                        $http.get(appHelper.adminURL('packages/draw-package-list/'+id))
                            .success(function(result) {
                                if (result.error === 0) {
                                    $("#option_list").append(result.data);
                                    
                                }
                            }            
                        );
                    }).error(function(result) {
                        
                    });
            }
        }catch(e){

        }
        $scope.save = function() {
            // if (categoryForm.$valid) {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                if ($scope.form.fields[i].name == 'image') {                    
                    file = $scope.form.fields[i].file;
                } else {
                    data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
                }
            }
            $scope.upload(file, data);
            // }
        };       
        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('packages/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/packages/edit/' + result.data.id);
                }
            });
        };
        
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 50,
            columns: [
                {
                    name: 'Name',
                    field: 'name',
                    enableCellEdit: false,
                    ngcellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">{{ row.entity.title}}</div>'
                },{
                    name: 'Description',
                    field: 'description'
                }
            ],
            list: appHelper.adminURL('packages/list'),
            edit: '#/app/packages/edit',
            delete: appHelper.adminURL('packages/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });        
    }   
}]);