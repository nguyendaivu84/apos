'use strict';

app.controller('PostController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'Post';
    $scope.template = appHelper.baseURL + '/tpl/posts/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'Posts';
    $scope.action = action;
    $scope.button = {
        title: 'List Post',
        link: '#/app/posts/list'
    };
    $scope.button_add = {
        title: 'Add Post',
        link: '#/app/posts/add'
    };
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add Posts';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit Posts';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }

    if (['add', 'edit'].indexOf(action) != -1) {
        $scope.form = {
            fields: [{
                type: 'hidden',
                name: 'id'
            }, {
                type: 'text',
                label: 'Title',
                name: 'title'
            }, {
                type: 'text',
                label: 'Link',
                name: 'link'
            }, {
                type: 'text',
                label: 'Link name',
                name: 'link_name'
            }, {
                type: 'select',
                label: 'Category',
                name: 'category_id',
                options: []
            }, {
                type: 'textarea',
                label: 'Short Description',
                name: 'short_description'
            }, {
                type: 'text-editor',
                label: 'Description',
                name: 'description'
            }, {
                type: 'text',
                label: 'Meta title',
                name: 'meta_title'
            }, {
                type: 'text',
                label: 'Meta Description',
                name: 'meta_description'
            }, {
                type: 'number',
                label: 'Order',
                name: 'order_no',
                value:1
            }, {
                type: 'image-upload',
                label: 'Image',
                name: 'image',
                value: 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image',
                inputAttr: 'ngf-validate="{size: {max: \'2MB\', min: \'10B\'} }" name="file" required',
                inputStyle: 'max-width: 200px;'
            }, {
                type: 'select',
                label: 'Active',
                name: 'active',
                options: [{value:1,text:'Active'},{value:0,text:'Inactive'}],
                value : 1
            }, {
                type: 'text-editor',
                label: 'Keywords/Tags',
                name: 'keywords'
            }
            , {
                type: 'select',
                label: 'Type',
                name: 'type',
                options: [{value:'home',text:'Hiển thị trang chủ'}],
            }
            ],
            title: formTitle
        };

        try{
            $http.get(appHelper.adminURL('posts/get-options-category'))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form.fields[4].options = result.data;                    
                    }            
                    $scope.form.allLoaded = true;
                }            
            );

            if (action === 'edit') {
                var id = $stateParams.id;
                $http.get(appHelper.adminURL('posts/edit/' + id))
                    .success(function(result) {
                        if (result.error === 0) {                            
                            $scope.form = appHelper.populateForm($scope.form, result.data);
                            $scope.form.fields[4].value = result.data.category_id;
                            $scope.form.fields[11].value = result.data.active;
                        }
                        $scope.form.allLoaded = true;
                    }).error(function(result) {
                        
                    });
            }
        }catch(e){

        }
        $scope.save = function() {
            // if (categoryForm.$valid) {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                if ($scope.form.fields[i].name == 'image') {
                    /*if (!$scope.form.fields[i].file.$valid && $scope.form.fields[i].file.$error) {
                        return false;
                    }*/
                    file = $scope.form.fields[i].file;
                } else {
                    data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
                }
            }
            $scope.upload(file, data);
            // }
        };
        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('posts/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/posts/edit/' + result.data.id);
                }
            });
        };
        
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 50,
            columns: [
                {
                    name: 'Title',
                    field: 'title',
                    enableCellEdit: false,
                    ngcellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">{{ row.entity.title}}</div>'
                },{
                    name: 'Short Description',
                    field: 'short_description'
                },{
                    name: 'Category',
                    field: 'category_name'
                },{
                    name: 'Order no',
                    field: 'order_no',
                    cellClass: "text_center",
                    enableSorting: false,
                    enableFiltering: false,
                }, {
                    name: 'Thumbs',
                    field: 'image',
                    enableSorting: false,
                    enableFiltering: false,
                    cellClass: 'text-center',
                    cellTemplate: '<img ng-if="row.entity.image" style="height: 100px;" src="{{ row.entity.image }}" class="image-thumbs" />',
                    width: '15%'
                }
            ],
            list: appHelper.adminURL('posts/list'),
            edit: '#/app/posts/edit',
            delete: appHelper.adminURL('posts/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });        
    }
}]);
