'use strict';

app.controller('SocialController', ['$rootScope', '$scope', '$stateParams', '$http', '$location', 'Upload', 'toaster', 'dialogs', 'uiGridConstants', 'RW', function($rootScope, $scope, $stateParams, $http, $location, Upload, toaster, dialogs, uiGridConstants, RW) {
    $rootScope.title = 'social';
    $scope.template = appHelper.baseURL + '/tpl/socials/';

    var action = $stateParams.action.toLowerCase(),
        formTitle = $rootScope.title;

    formTitle = 'social';
    $scope.action = action;
    $scope.button = {
        title: 'List social',
        link: '#/app/socials/list'
    };
    $scope.button_add = {
                title: 'Add social',
                link: '#/app/socials/add'
    };
    switch (action) {
        case 'list':
            $scope.template += 'all.html';
            break;
        case 'add':
            $scope.template += 'one.html';
            formTitle = 'Add social';
            break;
        case 'edit':
            $scope.template += 'one.html';
            formTitle = 'Edit social';
            break;
        default:
            $scope.template += 'all.html';
            break;
    }

    if (['add', 'edit'].indexOf(action) != -1) {
        $scope.form = {
            fields: [{
                type: 'hidden',
                name: 'id'
            }, {
                type: 'text',
                label: 'Name',
                name: 'name'
            }, {
                type: 'image-upload',
                label: 'Image',
                name: 'image',
                value: 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image',
                inputAttr: 'ngf-validate="{size: {max: \'2MB\', min: \'10B\'} }" name="file"',
                inputStyle: 'max-width: 200px;'               
            }, {
                type: 'text',
                label: 'Link',
                name: 'link'
            }, {
                type: 'select',
                label: 'Popular Social network',
                name: 'type',
                options: [
                    {value:'youtube',text:'Youtube'}
                    ,{value:'linkedin',text:'Linkedined'}
                    ,{value:'instagram',text:'Instagram'}
                    ,{value:'pinterest',text:'Pinterest'}
                    ,{value:'google-plus',text:'Google Plus'}
                    ,{value:'twitter',text:'Twitter'}
                    ,{value:'facebook',text:'Facebook'}
                ],
                value: 1
            }, {
                type: 'number',
                label: 'Order',
                name: 'order_no',
                value:1
            }, {
                type: 'select',
                label: 'Active',
                name: 'active',
                options: [{value:1,text:'Show'},{value:0,text:'Hide'},{value:'test',text:'<i class="fa fa-twitter"></i>'}],
                value: 1
            }],
            title: formTitle
        };

        if (action === 'edit') {
            var id = $stateParams.id;
            $http.get(appHelper.adminURL('socials/edit/' + id))
                .success(function(result) {
                    if (result.error === 0) {
                        $scope.form = appHelper.populateForm($scope.form, result.data);
                    }
                    $scope.form.allLoaded = true;
                }).error(function(result) {
                    dialogs.confirm(result.message, 'Do you want to create a new one?', {
                            size: 'md'
                        })
                        .result.then(function() {
                            $location.path('/app/socials/add');
                        }, function() {
                            $location.path('/app/socials/list');
                        });
                });
        } else {
            $scope.form.allLoaded = true;
        }

        $scope.save = function() {
            // if (categoryForm.$valid) {
            var file,
                data = {};
            for (var i in $scope.form.fields) {
                if ($scope.form.fields[i].name == 'image') {                    
                    file = $scope.form.fields[i].file;
                } else {
                    data[$scope.form.fields[i].name] = $scope.form.fields[i].value;
                }
            }
            $scope.upload(file, data);
            // }
        };

        $scope.upload = function(file, data) {
            var config = {
                url: appHelper.adminURL('socials/update'),
                fields: data
            };
            if (file && file.blobUrl) {
                config.file = file;
            }
            Upload.upload(config).success(function(result, status, headers, config) {
                if (result.error === 1) {
                    toaster.pop('error', 'Error', result.messages.join('<br />'));
                } else if (result.error === 0) {
                    $scope.form.fields[0].value = result.data.id;
                    toaster.pop('success', 'Message', result.message);
                    $location.path('/app/socials/edit/' + result.data.id);
                }
            });
        };
    } else if (action === 'list') {
        $scope.gridOptions = RW.gridOptions($scope, {
            gridName: 'gridOptions', //*required
            rowHeight: 60,
            columns: [ {
                    name: 'Name',
                    field: 'name'
                }, {
                    name: 'Link',
                    field: 'link'
                },{
                    name: 'social',
                    field: 'image',
                    enableSorting: false,
                    enableFiltering: false,
                    cellClass: 'text-center',
                    cellTemplate: '<img ng-if="row.entity.image" style="height: 100px;" src="{{ row.entity.image }}" class="image-thumbs" />'
                }],
            list: appHelper.adminURL('socials/list'),
            edit: '#/app/socials/edit',
            delete: appHelper.adminURL('socials/delete'),
            deleteConfirm: 'Are you sure you want to delete this category?' //optional
        });
    }
}]);
