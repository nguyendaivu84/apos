-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 27, 2016 at 04:41 AM
-- Server version: 5.5.46-MariaDB
-- PHP Version: 5.6.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_user_9`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `telephone`, `address`, `city`) VALUES
(1, 'Acme', '31566564', 'Address', 'Hello'),
(2, 'Acme Inc', '+44 564612345', 'Guildhall, PO Box 270, London', 'London');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `skey` varchar(255) NOT NULL,
  `svalue` text,
  `description` text,
  `type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `group_customer_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `note` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `group_customer_id`, `name`, `phone`, `email`, `address`, `note`, `deleted`) VALUES
(1, 0, 'Nguyễn Minh Trí 12x3x131213x213x', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị 2x', '', 0),
(2, 0, 'Nguyễn Minh Trí 123', '0939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 0),
(3, 0, 'Nguyễn Minh Trí 44', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(4, 0, 'Nguyễn Minh Trí 123213', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(5, 0, 'Nguyễn Minh 123 21', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(6, 0, '12c321c1', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(7, 0, 'xxxxxxxxxxxxx', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(8, 0, 'cèec12', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(9, 0, 'x123x21', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(10, 0, 'cdqwecwq', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(11, 0, 'x321x3124124', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(12, 0, 'Nguyễn Minh Trí 12312321321123123', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 1),
(13, 0, 'Nguyễn Minh Trí 123x123x2x311', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `distributes`
--

CREATE TABLE `distributes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `tax_code` varchar(255) DEFAULT NULL,
  `note` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `distributes`
--

INSERT INTO `distributes` (`id`, `name`, `phone`, `email`, `address`, `tax_code`, `note`, `deleted`) VALUES
(1, 'Khach 1', '123', '123@gmail.com', 'd wqd qw dq', '', '', 0),
(2, 'Khach 2', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', '', 0),
(3, 'Nguyễn Minh Trí', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', '', 0),
(4, 'Nguyễn Minh Trí 123', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', '', 0),
(5, ' 12c3213c21 ', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', '', 0),
(6, '123 Minh Trí', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', '', 0),
(7, 'fv2qwd1', '939543298', 'nmtri44@gmail.com', '332/44B Phan Văn Trị', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL DEFAULT '0',
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `purchase_quantity` int(11) NOT NULL,
  `current_quantity` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `buy_price` bigint(20) NOT NULL,
  `sell_price` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `purchase_order_id`, `product_id`, `purchase_quantity`, `current_quantity`, `deleted`, `buy_price`, `sell_price`) VALUES
(0, 3, 2, 1, 1, 0, 100000, 120011),
(10, 4, 2, 1, 1, 0, 100000, 120999),
(11, 4, 7, 1, 1, 0, 122000, 142000);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userId` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `status` int(3) DEFAULT '1',
  `type` int(2) NOT NULL DEFAULT '1',
  `totalPrice` float NOT NULL DEFAULT '0',
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customerId` int(11) NOT NULL DEFAULT '0',
  `userName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalTax` float NOT NULL DEFAULT '0',
  `storeId` int(11) NOT NULL,
  `storeName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `created_at`, `updated_at`, `userId`, `description`, `status`, `type`, `totalPrice`, `code`, `customerId`, `userName`, `customerName`, `totalTax`, `storeId`, `storeName`, `deleted`) VALUES
(1, '2016-12-16 02:33:55', '2016-12-16 02:33:55', 1, NULL, 1, 1, 0, '', 0, 'Phalcon Demo', NULL, 0, 1, 'Shop A', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `unitprice` float DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quantity` int(5) NOT NULL,
  `orderId` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `userId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `categoryName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float DEFAULT '0',
  `totalprice` float DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `storeId` int(11) NOT NULL,
  `storeName` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `other_tables`
--

CREATE TABLE `other_tables` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other_tables`
--

INSERT INTO `other_tables` (`id`, `name`, `short_name`, `type`) VALUES
(1, 'Sony', 'sony', 1),
(2, 'Winter Collection', 'winter-collection', 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `product_types_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(5) NOT NULL DEFAULT '1',
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `collection_id` int(11) NOT NULL DEFAULT '0',
  `is_in_store` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_quantity_in_store` int(11) NOT NULL,
  `maximum_quantity_in_store` int(11) NOT NULL,
  `have_attribute` tinyint(1) DEFAULT '0',
  `attributes` text COLLATE utf8_unicode_ci,
  `thumbs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `tax_cost` int(11) DEFAULT '10',
  `hot_item` tinyint(1) NOT NULL DEFAULT '0',
  `new_item` tinyint(1) NOT NULL DEFAULT '0',
  `standout_item` tinyint(1) NOT NULL DEFAULT '0',
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `price` decimal(16,2) NOT NULL,
  `active` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_types_id`, `name`, `short_name`, `sku`, `barcode`, `quantity`, `manufacturer_id`, `collection_id`, `is_in_store`, `minimum_quantity_in_store`, `maximum_quantity_in_store`, `have_attribute`, `attributes`, `thumbs`, `description`, `tax_cost`, `hot_item`, `new_item`, `standout_item`, `seo_title`, `seo_description`, `seo_keywords`, `created_at`, `updated_at`, `price`, `active`, `deleted`) VALUES
(1, 5, 'Artichoke', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '10.50', 'Y', 0),
(2, 6, 'Bell pepper', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '10.40', 'Y', 0),
(3, 5, 'Cauliflower', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '20.10', 'Y', 0),
(4, 5, 'Chinese cabbage', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '15.50', 'Y', 0),
(5, 6, 'Malabar spinach', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '7.50', 'Y', 0),
(6, 5, 'Onion', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '3.50', 'Y', 0),
(7, 5, 'Peanut', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10, 0, 0, 0, NULL, NULL, '', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '4.50', 'Y', 0),
(10, 6, 'test', 'test181016061044', 'test181016061044', 'test181016061044', 0, 1, 2, 1, 0, 0, NULL, NULL, '//public/images/upload/products/18-10-2016-06-10-44-Screenshot (15).png', 'dsadadasda', NULL, 1, 1, 1, 'test', 'dsadadasda', 'dasdasdad', '2016-11-30 17:00:00', '2016-11-30 17:00:00', '0.00', 'Y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `name`) VALUES
(5, 'Vegetables'),
(6, 'Fruits');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `distribute` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `note` text,
  `hinhthuc` bigint(11) DEFAULT NULL,
  `tienhang` bigint(11) NOT NULL,
  `tienthue` bigint(11) NOT NULL,
  `tongcong` bigint(11) NOT NULL,
  `thanhtoan` bigint(11) NOT NULL,
  `conno` bigint(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `distribute`, `date`, `created_by`, `note`, `hinhthuc`, `tienhang`, `tienthue`, `tongcong`, `thanhtoan`, `conno`, `created_at`, `updated_by`, `updated_at`, `deleted`, `status`) VALUES
(1, 7, '2016-11-19 09:11:00', 1, '12321', 0, 2220000, 0, 2220000, 0, 0, '2016-11-19 02:35:08', 1, '2016-12-23 08:42:09', 0, 0),
(2, 4, '2016-11-23 14:11:00', 1, 'TEst order 2', 0, 820000, 0, 820000, 0, 0, '2016-11-23 07:28:52', 1, '2016-12-23 08:42:05', 0, 0),
(3, 4, '2016-11-24 15:11:00', 1, '123', 0, 262000, 0, 262000, 0, 0, '2016-11-24 08:50:50', 1, '2016-12-23 08:46:42', 0, 0),
(4, 2, '2016-11-28 10:45:00', 1, 'test deleted', 0, 222000, 0, 222000, 0, 0, '2016-11-28 03:46:12', 0, '2016-11-28 03:46:19', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `revenues`
--

CREATE TABLE `revenues` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` bigint(20) NOT NULL,
  `note` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `revenues`
--

INSERT INTO `revenues` (`id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `total`, `note`, `deleted`) VALUES
(1, 1, NULL, '2016-12-16 08:42:05', '2016-12-27 09:40:19', 123456789, '123', 0),
(5, 1, NULL, '2016-12-16 09:23:04', '2016-12-16 09:23:04', 1232, '3x12x312', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `created_at`, `active`) VALUES
(1, 'demo', 'c0bd96dc7ea4ec56741a4e07f6ce98012814d853', 'Phalcon Demo', 'demo@phalconphp.com', '2012-04-10 20:53:03', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributes`
--
ALTER TABLE `distributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_order_id` (`purchase_order_id`,`product_id`),
  ADD UNIQUE KEY `purchase_order_id_2` (`purchase_order_id`,`product_id`),
  ADD UNIQUE KEY `purchase_order_id_3` (`purchase_order_id`,`product_id`),
  ADD KEY `inventory_ibfk_2` (`product_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_tables`
--
ALTER TABLE `other_tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revenues`
--
ALTER TABLE `revenues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `distributes`
--
ALTER TABLE `distributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `other_tables`
--
ALTER TABLE `other_tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `revenues`
--
ALTER TABLE `revenues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `inventory_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
