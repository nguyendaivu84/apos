$(function(){
	$('.dropdown').hover(function(){
		if($(document).width()>720){
			var link = $(this).find('a.dropdown-toggle')[0];
			if($(link).attr("aria-expanded")=="false"){
				$(this).addClass('open');
				$(link).attr("aria-expanded","true");
			}else{
				$(this).removeClass('open');
				$(link).attr("aria-expanded","false");
			}
		}
	});
	$(".dropdown-menu > li > a.trigger").on("click",function(e){
		var current=$(this).next();
		var grandparent=$(this).parent().parent();
		if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
			$(this).toggleClass('right-caret left-caret');
		grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
		grandparent.find(".sub-menu:visible").not(current).hide();
		current.toggle();
		e.stopPropagation();
	});
	$(".dropdown-menu > li > a:not(.trigger)").on("click",function(){
		var root=$(this).closest('.dropdown');
		root.find('.left-caret').toggleClass('right-caret left-caret');
		root.find('.sub-menu:visible').hide();
	});
	$('a[href*="#"]:not([href="#"])').click(function(e) {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);	
				return false;
			}
		}
	});
	$(window).scroll(function(){
		if($(document).width()>800){
			if($(document).scrollTop()>70){
				$(".navbar").addClass("float");
				$("#phone-contact").hide();
				$(".button-top").removeClass('hide');
			}else{
				$(".navbar").removeClass("float");
				$("#phone-contact").show();
				$(".button-top").addClass('hide');
			}
			revealOnScroll();
		}
	})
	$(window).scroll(function(){
		if($(document).width()>767){
			if($(document).scrollTop()>650){
				$(".time-blog").addClass("float");
			}else{
				$(".time-blog").removeClass("float");
			}
			revealOnScroll();
		}
	})
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	$(".row-group").each(function(key,elem){
		var col = $(elem).find(".column-group");
		maxHeight=0;
		col.each(function(key2,elem2){
			if($(elem2).height()>maxHeight){
				maxHeight = $(elem2).height();
			}
		})
		col.height(maxHeight);
	});

	if($("#btn-apply-job")){
		$("#btn-apply-job").on("click",function(){
			$("#form-apply-job").toggle();
		})
	}

	if($(".slider-banner")){
		if($('.item-banner').length>1){
			$(".slider-banner").owlCarousel({
				dots:true,
				responsiveClass:true,
				items:1,
				'autoplay':true,
				'loop':true,
				'autoplayTimeout':5000,
			});
		}else{
			$(".slider-banner").owlCarousel({
				responsiveClass:true,
				items:1,
				'autoplay':false
			});
		}
	}

	if($(".slider-portfolio")){
		$(".slider-portfolio").owlCarousel({
			dots:false,
			responsiveClass:true,
			responsive:{
				0:{
					items:2
				},
				720:{
					items:3
				},
				1000:{
					items:4
				}
			},
			'autoplay':true
		});
	}

	if($(".slider-client-reply")){
		$(".slider-client-reply").owlCarousel({
			dots:true,
			item:3,
			center: true,
			'autoplay':true,
			'loop':true,
			responsive:{
				0:{
					items:1,
					merge:false,
				},
				720:{
					items:3,
					merge:true,
					'margin':70,
				},
				1000:{
					items:3,
					merge:true,
					'margin':70,
				}
			}
		});
	}
	revealOnScroll();
})
function testAnim(x) {
	$('#animationSandbox').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$(this).removeClass();
	});
};

function revealOnScroll() {
	var scrolled      = $(window).scrollTop(),
	win_height_padded = $(window).height() * 1.1;

	// Showed...
	$(".animate:not(.animated)").each(function () {
		var $this         = $(this),
		offsetTop         = $this.offset().top;
		if (scrolled + win_height_padded > offsetTop) {
			if ($this.data('timeout')) {
				window.setTimeout(function(){
					$this.addClass('animated');
				}, parseInt($this.data('timeout'),10));
			} else {
			$this.addClass('animated '+$this.attr("data-animate") );
			testAnim($this.attr("data-animate"));
			}
		}
	});
	// Hidden...
	$(".animate.animated").each(function (index) {
		var $this         = $(this),
		offsetTop         = $this.offset().top;
		if (scrolled + win_height_padded < offsetTop) {
			$(this).removeClass('animated '+$this.attr("data-animate"))
		}
	});
}
