$(document).ready(function() {
	$( ".navbar-toggle.menu-toggler" ).click(function() {
	  $( ".nav.nav-list.dropdown-menu" ).toggle();
	});

	$("#contact-carousel").owlCarousel({
	    autoPlay: false,
	    items: 4, //10 items above 1000px browser width
	    itemsDesktop: [1024, 2], //5 items between 1024px and 901px
	    itemsDesktopSmall: [900, 2], // 3 items betweem 900px and 601px
	    itemsTablet: [600, 2], //2 items between 600 and 0;
	    itemsMobile: [320, 1],
	    navigation: true,
	    navigationText: [
	      "<i class='fa fa-arrow-left'></i>",
	      "<i class='fa fa-arrow-right'></i>"
	    ],
	    slideSpeed: 500,
	    pagination: false
	});

	$('#fadeandscale').popup({
      transition: 'all 0.3s'
    });
    $('#loginpopup').popup({
      transition: 'all 0.3s'
    });
	
});
function dangkyapos(){

	var store_name = $("#ShopName").val();
	var FullName = $("#FullName").val();
	var PhoneNumber = $("#PhoneNumber").val();
	var Email = $("#Email").val();
	var _Password = $("#Password").val();
	var RepeatPassword = $("#RepeatPassword").val();
	if($("#apos_register")[0].checkValidity()) {       
		
    }else {
    	if(!check_form()){
    	 	return check_form();
    	}else{
    		$("#loadingReg").removeClass('hidden');
    		var data = {};
			data = {
				'store_name' 	: store_name,
				'name' 			: FullName,
				'phone' 		: PhoneNumber,
				'email' 		: Email,
				'password' 		: _Password
			}
			$.ajax({
				url 	: '/create-user',
				type	: 'POST',
				data	: data,
				success : function(data){
					$("#loadingReg").addClass('hidden');
					if(data.error){
						alert(data.message);
					}else{
						window.location = data.link_demo;
					}
				}
			});
    	}
    }   
}

function dangnhapapos(){
	$("displaylogin").html('');
	$("div_errorlogin").addClass('hidden');
	$("#loadinglogin").removeClass('hidden');
	data = {
		'email' 		: $("#EmailLogin").val(),
		'password' 		: $("#PasswordLogin").val()
	}
	if(data.email==""||data.password==""){
		$("#displaylogin").html('<li>'+'Email hoặc password không thể để trống'+'</li>');
		$("#div_errorlogin").removeClass('hidden');
		return false;
	}
	$.ajax({
		url 	: '/login',
		type	: 'POST',
		data	: data,
		success : function(data){
			$("#loadinglogin").addClass('hidden');
			if(data.error){
				alert(data.message);
			}else{
				window.location = data.link_demo;
			}
		}
	});
}

function check_form(){
	var str = '';
	$("#display").html('');
	$("#div_error").addClass('hidden');
	var store_name = $("input#ShopName").val();
	var FullName = $("input#FullName").val();
	var PhoneNumber = $("input#PhoneNumber").val();
	var Email = $("input#Email").val();
	var _Password = $("input#Password").val();
	var RepeatPassword = $("input#RepeatPassword").val();
	if(store_name==''){
		str +='<li>'+'Tên cửa hàng không thể để trống'+'</li>';
	}
	if(FullName==''){
		str +='<li>'+'Tên chủ cửa hàng không thể để trống'+'</li>';
	}
	if(Email==''){
		str +='<li>'+'Email cửa hàng không thể để trống'+'</li>';
	}
	if(_Password==''){
		str +='<li>'+'Mật khẩu không thể để trống'+'</li>';
	}
	if(RepeatPassword==''){
		str +='<li>'+'Nhập lại mật khẩu không thể để trống'+'</li>';
	}	
	var email_construct = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
	if (!email_construct.test(Email)){
		str +='<li>'+'Email không hợp lệ'+'</li>';
	}
	if(_Password.length < 6 || RepeatPassword.length < 6) str +='<li>'+'Mật khẩu phải nhiều hơn 5 ký tự'+'</li>';
	if(_Password.length != RepeatPassword.length || _Password != RepeatPassword) str +='<li>'+'Mật khẩu nhập lại không giống mật khẩu'+'</li>';	
	if(str!='') {
		$("#display").html(str);	
		$("#div_error").removeClass('hidden');
		return false;
	}
	return true;
}
