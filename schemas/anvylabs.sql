-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2016 at 05:22 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anvylabs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `active`, `created_at`, `updated_at`) VALUES
(10, 'admin', 'admin', '$2a$08$tEs7SwhmPIcUAOSSkrOX8.BzD1N5sdAmFNKQIMp.dURANdzXRF1ae', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_no` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(3) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `link`, `order_no`, `created_at`, `updated_at`, `position`, `deleted`) VALUES
(4, 'images/banners/1234_25-03-16.jpg', '', 1, '2016-03-24 22:56:56', '2016-03-24 22:56:56', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `position` tinyint(4) DEFAULT '1',
  `order_no` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `short_name`, `description`, `image`, `meta_title`, `meta_description`, `parent_id`, `position`, `order_no`, `created_at`, `updated_at`, `deleted`, `type`) VALUES
(1, 'hùng lâm', 'hung-lam', 'some thing', 'images/category/koala_17-03-16.jpg', 'some thing 2', 'some thing 3', 2, 1, 1, '0000-00-00 00:00:00', '2016-03-25 00:35:35', 1, ''),
(2, 'test 123', 'test-123', '', NULL, '', '', 0, 1, 1, '2016-03-16 22:56:00', '2016-03-25 00:35:32', 1, ''),
(3, 'dadadada', 'dadadada', 'dadad', NULL, '', '', 0, 1, 1, '2016-03-23 20:01:49', '2016-03-23 20:21:25', 1, ''),
(4, 'Những sản phẩm chính', 'nhung-san-pham-chinh', 'trang home nhung san phẩm chính', NULL, 'main-project', '', 0, 1, 1, '2016-03-25 00:36:46', '2016-03-25 00:52:55', 0, 'san-pham-chinh'),
(5, 'Bài viết trang chủ', 'bai-viet-trang-chu', 'bài viết trang chủ', NULL, 'bai-viet-trang-chu', '', 0, 1, 1, '2016-03-25 00:37:42', '2016-03-25 00:52:48', 0, 'bai-viet-trang-chu'),
(6, 'Các dự án đang phát triển', 'cac-du-an-dang-phat-trien', 'Các dự án đang phát triển', NULL, '', '', 0, 1, 1, '2016-03-25 00:38:34', '2016-03-25 00:52:33', 0, 'du-an-dang-phat-trien'),
(7, 'Blogs', 'blogs', 'Blog&#39;s post', NULL, '', '', 0, 1, 1, '2016-04-07 03:07:58', '2016-04-07 03:07:58', 0, '1'),
(8, 'Websites', 'websites', '', NULL, '', '', 0, 1, 1, '2016-04-10 21:02:45', '2016-04-10 21:02:45', 0, '1'),
(9, 'Jobtraq', 'jobtraq', '', NULL, '', '', 0, 1, 1, '2016-04-10 21:03:11', '2016-04-10 21:03:11', 0, '1'),
(10, 'POS', 'pos', '', NULL, '', '', 0, 1, 1, '2016-04-10 21:03:17', '2016-04-10 21:03:17', 0, '1'),
(11, 'DSS2', 'dss2', '', NULL, '', '', 0, 1, 1, '2016-04-10 21:03:24', '2016-12-21 20:01:23', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `mau_chu` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'black',
  `mau_nen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'white'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `customer_id`, `customer_name`, `comments`, `created_at`, `updated_at`, `customer_logo`, `user_id`, `customer_link`, `active`, `deleted`, `mau_chu`, `mau_nen`) VALUES
(1, 1, 'Hanergy', 'hello from the others side', '2016-04-08 21:38:25', '2016-04-08 21:42:09', 'images/customers/logo-client-1.png', 0, NULL, 1, 1, 'brow', 'white'),
(2, 1, 'Hanergy', 'Rainmaker Labs has provided satisfying support and services with regards to the Mobile App Development with own integrated Mobile Marketing Automation platform support. We have also engaged in their Mobility Platform flexibility and custom solutions.', '2016-04-08 21:44:44', '2016-04-08 21:44:44', 'images/customers/logo-client-1.png', 0, NULL, 1, 0, 'brow', 'white'),
(3, 2, 'Vemma', 'Rainmaker Labs has provided satisfying support and services with regards to the Mobile App Development with own integrated Mobile Marketing Automation platform support. We have also engaged in their Mobility Platform flexibility and custom solutions.', '2016-04-08 21:45:07', '2016-04-08 21:45:07', 'images/customers/logo-client-2.png', 0, NULL, 1, 0, 'brow', 'white'),
(4, 3, 'StarHub', 'Rainmaker Labs has provided satisfying support and services with regards to the Mobile App Development with own integrated Mobile Marketing Automation platform support. We have also engaged in their Mobility Platform flexibility and custom solutions.', '2016-04-08 21:45:21', '2016-04-08 21:45:21', 'images/customers/logo-client-3.png', 0, NULL, 1, 0, 'brow', 'white'),
(5, 4, 'SCB\r\n', 'Rainmaker Labs has provided satisfying support and services with regards to the Mobile App Development with own integrated Mobile Marketing Automation platform support. We have also engaged in their Mobility Platform flexibility and custom solutions.', '2016-04-08 21:45:44', '2016-04-08 21:45:44', 'images/customers/logo-client-4.png', 0, NULL, 1, 0, 'brow', 'white');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(10) NOT NULL,
  `cf_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cf_value` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` int(1) NOT NULL DEFAULT '0',
  `order_no` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `cf_key`, `cf_value`, `status`, `created_at`, `updated_at`, `type`, `order_no`) VALUES
(1, 'about_footer', '<p>Products above may not appear exactly as shown. Product, pricing and participation may vary by location. Delivery areas and charges may vary by location and minimum delivery order conditions may apply. All taxes and delivery charges extra. Please note that for the purpose of completing your order, Pizza Hut and/or independent Pizza Hut franchisees must collect customer and order information.This information may be retained by Pizza Hut and/or franchisees to help serve you better in the future. View our Privacy Policy.</p>\r\n        <p>® Reg. TM/MD Pizza Hut International, LLC; Used under license.</p>\r\n        <p>Gluten Free Crust Pizza is made in an environment that contains wheat. Our tomato sauce, meats, cheeses and vegetable toppings are gluten free as well! For more information go to ''Nutrition'' at the bottom of our website.</p>\r\n        <p>Pepsi® and PepsiCo Inc. related companies'' marks are used under license. Brisk® and Lipton® are Unilever BRANDS and related marks are used under license. Dole® and Dole Food Company, Inc.''s related marks are used under license. The HERSHEY’S® and CHIPITS® trademarks are used under license. HERSHEY''S® Chocolate Dunkers include white chocolate and HERSHEY''S® milk chocolate.</p>\r\n        <p>®Reg. TM/MD PH Yum! Franchise I LP; Used under license. © 2015 PH Canada Company. All rights reserved.</p>\r\n        <p>Powered by Tillster®</p>', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `company_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_interest` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `message`, `company_name`, `contact_name`, `contact_email`, `contact_phone`, `primary_interest`, `created_at`, `created_by`, `deleted`) VALUES
(1, 'dadada', 'dad', '1213313', 'adadad', 'dad', 'adada', '0000-00-00 00:00:00', 10, 0),
(2, '121', '21212', 'hung lam', 'hunglmkpc044@gmail.com', '12121', NULL, '2016-03-23 20:58:28', 10, 1),
(3, ' ', '', 'dada', 'dadad', 'adada', 'metting', '2016-03-23 21:45:05', NULL, 0),
(4, ' dadada', 'dad', 'adad', 'dad', 'adada', 'metting', '2016-03-23 21:46:20', NULL, 0),
(5, ' dadadada', 'dad', 'ada', 'dada', 'dada', 'metting', '2016-03-23 21:47:35', NULL, 0),
(6, ' dadada', 'ada', 'dad', 'ada', 'dadad', 'metting', '2016-03-23 21:48:24', NULL, 0),
(7, ' dadada', 'ada31313131', 'dad', 'ada', 'dadad', 'metting', '2016-03-23 21:48:29', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `short_name`, `image`, `active`, `deleted`, `description`, `created_at`, `created_by`, `link`) VALUES
(1, 'Hanergy', 'Hanergy', 'images/customers/logo-client-1.png', 1, 0, 'Hanergy', '2016-03-25 08:40:09', NULL, NULL),
(2, 'Vemma', 'vemma', 'images/customers/logo-client-2.png', 1, 1, NULL, '2016-03-25 08:40:26', NULL, NULL),
(3, 'StarHub', 'starHub', 'images/customers/logo-client-3.png', 1, 0, NULL, '2016-03-25 08:40:41', NULL, NULL),
(4, 'SCB\r\n', 'SCB', 'images/customers/logo-client-4.png', 1, 1, NULL, '2016-03-25 08:40:56', NULL, NULL),
(5, 'Telkmosel', 'telkomsel', 'images/customers/logo-client-5.png', 1, 1, NULL, '2016-03-25 08:41:14', NULL, NULL),
(6, 'KAO', 'kao', 'images/customers/logo-client-6.png', 1, 1, NULL, '2016-03-25 08:41:30', NULL, NULL),
(7, 'abacus', 'abacus', 'images/customers/logo-client-7.png', 1, 1, NULL, '2016-03-25 08:41:45', NULL, NULL),
(8, 'Sistic', 'sistic', 'images/customers/logo-client-8.png', 1, 1, NULL, '2016-03-25 08:42:01', NULL, NULL),
(9, 'Good Year', 'good-year', 'images/customers/logo-client-9.png', 1, 1, NULL, '2016-03-25 08:42:19', NULL, NULL),
(10, 'Kumon', 'kumon', 'images/customers/logo-client-10.png', 1, 1, NULL, '2016-03-25 08:42:34', NULL, NULL),
(11, 'Konica Monolta', 'komica-monolta', 'images/customers/logo-client-11.png', 1, 0, NULL, '2016-03-25 08:42:52', NULL, NULL),
(12, 'CNQC', 'cnqc', 'images/customers/logo-client-12.png', 1, 0, NULL, '2016-03-25 08:43:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_logins`
--

CREATE TABLE `failed_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` char(15) NOT NULL,
  `attempted` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `failed_logins`
--

INSERT INTO `failed_logins` (`id`, `admin_id`, `ip_address`, `attempted`, `created_at`, `updated_at`) VALUES
(1, 10, '127.0.0.1', 65535, '2016-03-22 21:28:36', '2016-03-22 21:28:36'),
(2, 10, '14.186.180.96', 65535, '2016-03-25 09:10:12', '2016-03-25 09:10:12'),
(3, 10, '14.186.180.96', 65535, '2016-03-25 09:10:16', '2016-03-25 09:10:16'),
(4, 10, '127.0.0.1', 65535, '2016-12-15 23:06:37', '2016-12-15 23:06:37'),
(5, 10, '127.0.0.1', 65535, '2016-12-15 23:06:41', '2016-12-15 23:06:41'),
(6, 10, '127.0.0.1', 65535, '2016-12-15 23:06:47', '2016-12-15 23:06:47'),
(7, 10, '127.0.0.1', 65535, '2016-12-15 23:06:51', '2016-12-15 23:06:51'),
(8, 10, '127.0.0.1', 65535, '2016-12-15 23:06:55', '2016-12-15 23:06:55'),
(9, 0, '127.0.0.1', 65535, '2016-12-15 23:06:59', '2016-12-15 23:06:59');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `customer_logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'freelance',
  `time_expires` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `short_description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'Việt Nam',
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'HCMC',
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `customer_id`, `title`, `customer_logo`, `type`, `time_expires`, `created_at`, `updated_at`, `short_description`, `country`, `city`, `short_name`, `order_no`, `active`, `deleted`, `description`) VALUES
(2, 2, 'bài viet có dấu 5', 'images/customers/logo-client-2.png', 'internship', '2016-04-27 17:00:00', '2016-04-19 01:34:17', '2016-04-19 21:59:16', 'dadadadadada', 'VietNam', 'HCMC', 'bai-viet-co-dau-5', 2, 1, 0, NULL),
(3, 3, 'bài viet có dấu 4', 'images/customers/logo-client-3.png', 'freelance', '2016-04-29 17:00:00', '2016-04-19 20:45:14', '2016-04-19 21:59:06', 'dadadad', 'VietNam', 'HCMC', 'bai-viet-co-dau-4', 2, 1, 0, NULL),
(4, 11, 'bài viet có dấu 3', 'images/customers/logo-client-11.png', 'full-time', '2016-05-18 17:00:00', '2016-04-19 20:45:30', '2016-04-19 21:58:55', 'd adsad', 'VietNam', 'HCMC', 'bai-viet-co-dau-3', 1, 1, 0, NULL),
(5, 10, 'Bài viết có dầu 2', 'images/customers/logo-client-10.png', 'part-time', '2016-06-09 17:00:00', '2016-04-19 20:45:49', '2016-04-19 21:58:44', 'adadsada', 'VietNam', 'HCMC', 'bai-viet-co-dau-2', 1, 1, 0, NULL),
(6, 7, 'Lâm minh khánh hùng', 'images/customers/logo-client-7.png', 'part-time', '2016-04-29 17:00:00', '2016-04-19 20:46:12', '2016-04-19 21:54:41', 'dadasdasdadadad', 'Singapore', 'HNC', 'lam-minh-khanh-hung', 1, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'backend',
  `order_no` int(5) UNSIGNED NOT NULL DEFAULT '1',
  `level` int(2) UNSIGNED NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_name` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8_unicode_ci,
  `displayhtml` int(1) NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT '0',
  `pricelist_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `icon_class`, `link`, `parent_id`, `type`, `order_no`, `level`, `active`, `created_by`, `updated_by`, `created_at`, `updated_at`, `image_file`, `group_name`, `image`, `contents`, `displayhtml`, `category_id`, `pricelist_id`) VALUES
(1, 'Các sản phẩm', NULL, 'cac_san_pham', 0, 'backend', 0, 1, 1, 1, 10, '2016-03-23 09:18:54', '2016-04-07 06:45:20', NULL, 'header', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', '', 0, 0, NULL),
(2, 'Websites', NULL, 'category/websites', 1, 'backend', 0, 2, 1, 1, 10, '2016-03-23 09:29:25', '2016-04-10 21:05:47', NULL, 'header', 'null', '', 0, 8, NULL),
(3, 'Jobtraq', NULL, 'category/jobtraq', 1, 'backend', 0, 2, 1, 1, 10, '2016-03-23 09:29:56', '2016-04-10 21:04:58', NULL, 'header', 'null', '', 0, 9, NULL),
(4, 'Liên hệ', NULL, 'contact-us.html', 0, 'backend', 2, 1, 1, 1, 10, '2016-03-23 09:59:58', '2016-04-07 06:44:21', NULL, 'header', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', '', 0, 0, NULL),
(5, 'Dự án đang phát triển', NULL, 'du-an-dang-phat-trien.html', 0, 'backend', 1, 1, 1, 1, 10, '2016-03-23 10:00:20', '2016-04-14 01:59:28', NULL, 'header', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', '', 0, 6, NULL),
(8, 'home', NULL, '/', 0, 'backend', 1, 1, 1, 10, 10, '2016-04-06 01:57:01', '2016-04-13 02:06:18', NULL, 'header', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', '<div class="animate animated bounceInLeft" style="font-size:18px;color:white;margin-left:45%;margin-top:70px;">\n<h3>Thiết kế web + Jobtraq CRM</h3>\nGiải pháp toàn diện cho doanh nghiệp sản xuất<br>\n \n<h3>Thiết kế web + Jobtraq CRM + POS</h3>\nGiải pháp cho hệ thống bán lẻ chuyên nghiệp<br>\n \n<p><a class="btn-primary btn btn-lg" href="#anchor">Nhận báo giá sản phẩm</a></p>\n</div>\n', 1, 0, NULL),
(9, 'Blogs', NULL, 'blogs.html', 0, 'backend', 1, 1, 1, 10, 10, '2016-04-07 03:08:40', '2016-04-07 03:08:40', NULL, 'header', '', '', 0, 7, NULL),
(10, 'Bảng giá', NULL, 'bang-gia.html', 0, 'backend', 1, 1, 1, 10, 10, '2016-04-07 06:31:41', '2016-04-07 06:31:41', NULL, 'header', '', '', 0, 0, NULL),
(14, 'Pos', NULL, 'category/pos', 1, 'backend', 2, 1, 1, 10, 10, '2016-04-10 21:06:20', '2016-04-10 21:06:20', NULL, 'header', '', '', 0, 10, NULL),
(15, 'DSS', NULL, 'category/dss', 1, 'backend', 1, 1, 1, 10, 10, '2016-04-10 21:06:34', '2016-04-10 21:06:34', NULL, 'header', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image', '', 0, 11, NULL),
(16, 'Careers', NULL, 'carrers.html', 0, 'backend', 1, 1, 1, 10, 10, '2016-04-20 03:14:01', '2016-04-20 03:14:01', NULL, 'header', '', '', 0, 0, NULL),
(17, 'Jobtraq', NULL, 'chi-tiet/jobtraq.html', 10, 'backend', 1, 1, 1, 10, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'header', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', NULL, 0, 9, 2),
(18, 'Thông tin công ty', NULL, 'thong-tin-cong-ty.html', 0, 'backend', 1, 1, 1, 1, 10, '2016-03-23 10:00:20', '2016-04-14 01:59:28', NULL, 'footer', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', '', 0, 0, NULL),
(19, 'Về chúng tôi', NULL, 've-chung-toi/html', 18, 'backend', 1, 2, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, '<div id="content">\r\n			<h1 class="archive-title">Về chúng tôi </h1>\r\n	  	<div class="col-xs-12 align-left">\r\nLà một đội ngũ các chuyên gia CNTT và Quản lý bán lẻ, chúng tôi có&nbsp;khát vọng làm <strong>tăng hiệu quả và sức cạnh tranh của doanh nghiệp nhỏ bằng cách ứng dụng CNTT .</strong><p></p>\r\n<p>Để phần mềm có thể tăng hiệu quả và mang lại lợi ích cho doanh nghiệp – &nbsp;đặc biệt là các shop, cửa hàng vừa và nhỏ – thì phần mềm ấy cần phải dễ sử dụng, dễ đến mức một người không biết nhiều về máy tính vẫn có học và biết dùng trong 5 – 10 phút.</p>\r\n<h2>…và mục tiêu mang phần mềm quản lý bán hang dễ dùng nhất đến 100.000 shop.</h2>\r\n<p>Với niềm tin đó, chúng tôi tạo ra phần mềm quản lý bán hàng dễ dùng nhất hiện nay, mà chúng tôi gọi là phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN SUNO.vn – dùng cho các cửa hàng, shop bán lẻ, bán buôn qui mô vừa và nhỏ.</p>\r\n<p><a rel="attachment wp-att-773" href="https://suno.vn/phan-mem-quan-ly-ban-hang-sieu-thi-mini/pos-phan-mem-quan-ly-ban-hang-sieu-thi-mini/"><img class="aligncenter wp-image-773 size-full" src="https://suno.vn/wp-content/uploads/2016/10/pos-phan-mem-quan-ly-ban-hang-sieu-thi-mini.png" alt="phần mềm quản lý bán hàng dễ dùng nhất SUNO.vn" width="600" height="347" srcset="https://suno.vn/wp-content/uploads/2016/10/pos-phan-mem-quan-ly-ban-hang-sieu-thi-mini.png 600w, https://suno.vn/wp-content/uploads/2016/10/pos-phan-mem-quan-ly-ban-hang-sieu-thi-mini-300x174.png 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>\r\n<p>Khách hàng thực sự đầu tiên của mỗi sản phẩm hoặc dịch vụ chúng tôi làm ra là “chính chúng tôi”. Chúng tôi sống, đổi mới cùng sản phẩm và dịch vụ của mình cho đến khi chúng đủ tốt và hiệu quả để mang đến cho bạn.</p>\r\n<p>Chúng tôi KHÔNG HỎI – chúng tôi là ai, mà LUÔN TỰ HỎI, chúng tôi đã làm được gì và chúng tôi sẽ làm được gì cho khách hàng? Bạn có thể tìm thấy nhiều hình ảnh của chúng tôi khi dành thời gian để sử dụng và trải nghiệm SUNO.vn.</p>\r\n<p>Mọi thắc mắc, quý khách xin vui lòng email về&nbsp;<a href="mailto:contact@suno.vn">contact@suno.vn</a></p>\r\n<p>Trân trọng!</p>\r\n<p>SUNO.vn</p>\r\n</div>\r\n\r\n		<footer class="entry-meta"><h4>Mục liên quan: </h4><span class="tag-links"><a href="https://suno.vn/tag/phan-mem-quan-ly-ban-hang-de-dung-nhat/" rel="tag">phần mềm quản lý bán hàng dễ dùng nhất</a>; <a href="https://suno.vn/tag/phan-mem-quan-ly-ban-hang-de-su-dung-nhat/" rel="tag">phần mềm quản lý bán hàng dễ sử dụng nhất</a>; <a href="https://suno.vn/tag/phan-mem-quan-ly-ban-le-de-dung-nhat/" rel="tag">phần mềm quản lý bán lẻ dễ dùng nhất</a></span></footer>				</div>', 0, 0, NULL),
(20, 'Phần mềm quản lý', NULL, 'phan-mem-quan-ly.html', 0, 'backend', 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, NULL, 0, 0, NULL),
(21, 'Phần mềm quản lý shop', NULL, 'phan-mem-shop-thoi-trang', 20, 'backend', 1, 2, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, '<div class="full-block main-bg">\r\n<div class="web-inner header">\r\n<div class="img-banner-info">\r\n<div class="col-xs-12">\r\n<h1>Phần mềm quản lý shop thời trang, quần áo</h1>\r\n<p><span style="color: #fff;">Phần mềm quản lý shop thời trang đơn giản và hiệu quả được thiết kế đặc biệt dành riêng cho các cửa hàng thời trang. Đồng bộ website nhanh chóng, kết nối&nbsp;máy in hoá đơn, thiết bị mã vạch&nbsp;dễ dàng</span></p>\r\n<ul style="color: #fff;">\r\n<li>Quản lý mọi loại hàng: quần áo, giầy dép, balo, túi xách, mũ nón, mắt kính, phụ kiện …</li>\r\n<li>Quản lý chủng loại hàng hoá: kiểu, size, màu sắc… thuận tiện</li>\r\n<li>Theo dõi số lượng và giá trị hàng tồn kho <strong>chính xác</strong></li>\r\n<li>SIÊU ĐƠN GIẢN, nhân viên chỉ cần 5 phút làm quen và bán hàng</li>\r\n</ul>\r\n<div>\r\n<div class="form-group no-padding">\r\n<h4 class="white">Bắt đầu sử dụng với 7 ngày MIỄN PHÍ</h4>\r\n</div>\r\n<div class="form-group no-padding"><a class="btn btn-warning btn-xs-100 mt-xs-10" href="https://auth.suno.vn/dang-ky/?ip=14.186.153.123&amp;meta=424to508to398to424to59to424to262to449">Đăng ký MIỄN PHÍ</a></div>\r\n<p style="padding: 5px;">\r\n</p></div>\r\n</div>\r\n</div>\r\n<div class="img-banner-pic"><a rel="attachment wp-att-43" href="../wp-content/uploads/2015/09/pos-l.png"><img class="alignnone wp-image-43 size-full" src="https://suno.vn/wp-content/uploads/2015/09/pos-l.png" alt="Màn hình POS bán hàng - phần mềm quản lý shop thời trang" width="600" height="347" srcset="https://suno.vn/wp-content/uploads/2015/09/pos-l.png 600w, https://suno.vn/wp-content/uploads/2015/09/pos-l-300x174.png 300w" sizes="(max-width: 600px) 100vw, 600px"></a></div>\r\n</div>\r\n</div>', 0, 0, NULL),
(22, 'Chủ đề', NULL, 'chu-de.html', 0, 'backend', 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, NULL, 0, 0, NULL),
(23, 'Khởi nghiệp', NULL, '/chu-de/khoi-nghiep', 22, 'backend', 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, NULL, 0, 0, NULL),
(24, 'Blog', NULL, 'blog.html', 0, 'backend', 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, NULL, 0, 0, NULL),
(25, 'Kinh nghiệm bán hàng online: Xây dựng chân dung khách hàng mục tiêu', NULL, '', 24, 'backend', 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'footer', NULL, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lastsendDate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`id`, `name`, `email`, `userId`, `created_at`, `updated_at`, `lastsendDate`) VALUES
(1, 'hung', 'hunglmkpc044@gmail.com', NULL, '2016-04-06 21:51:35', '2016-04-06 21:51:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `package_details`
--

CREATE TABLE `package_details` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `packaged_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `package_details`
--

INSERT INTO `package_details` (`id`, `name`, `short_name`, `price`, `active`, `deleted`, `created_at`, `updated_at`, `packaged_id`) VALUES
(1, 'dada', 'dada', 0, 1, 1, '2016-04-21 02:00:34', '2016-04-21 02:31:51', 2),
(2, 'dada', 'dada', 0, 1, 1, '2016-04-21 02:00:45', '2016-04-21 02:32:09', 2),
(3, 'dadada', 'dadada', 0, 1, 1, '2016-04-21 02:17:15', '2016-04-21 02:33:48', 2),
(4, 'dadada', 'dadada', 0, 1, 1, '2016-04-21 02:36:40', '2016-04-21 02:36:42', 2),
(5, 'dadadadadadada', 'dadadadadadada', 0, 1, 1, '2016-04-21 02:36:45', '2016-04-21 02:36:49', 2),
(6, 'dadadadadadada', 'dadadadadadada', 0, 1, 1, '2016-04-21 02:36:46', '2016-04-21 02:36:50', 2),
(7, 'dadadadadadada', 'dadadadadadada', 0, 1, 0, '2016-04-21 02:36:47', '2016-04-21 02:36:47', 2),
(8, 'tesdada', 'tesdada', 121331, 1, 0, '2016-04-21 03:30:23', '2016-04-21 03:30:23', 2),
(9, '121212', '121212', 4444, 1, 0, '2016-04-21 03:30:28', '2016-04-21 03:30:28', 2),
(10, 'test ', 'test-', 21212, 1, 0, '2016-04-21 03:31:48', '2016-04-21 03:31:48', 3),
(11, 'test  2', 'test-2', 21212, 1, 0, '2016-04-21 03:31:52', '2016-04-21 03:31:52', 3),
(12, 'test  313131', 'test-313131', 21212, 1, 0, '2016-04-21 03:31:55', '2016-04-21 03:31:55', 3);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `category_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8_unicode_ci,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descoration` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `short_name`, `short_description`, `description`, `category_id`, `category_name`, `category_link`, `meta_title`, `meta_description`, `deleted`, `active`, `created_at`, `updated_at`, `order_no`, `created_by`, `updated_by`, `image`, `title`, `link`, `link_name`, `keywords`, `type`, `descoration`) VALUES
(25, 'phan-mem-quan-ly-ban-hang-sieu-don-gian', ' <ul class="white">\r\n                                 <li>POS bán hàng, tính tiền, in hoá đơn nhanh.</li>\r\n                                 <li>Cực kỳ dễ sử dụng, kể cả người ko biết nhiều về máy tính.</li>\r\n                                 <li>Quản lý cửa hàng bán lẻ, theo dõi doanh số, tồn kho bằng điện thoại.</li>\r\n                                 <li>Đồng bộ với <strong>website</strong> bán hàng trực tuyến và gian hàng <strong>facebook</strong>.</li>\r\n                                 <li>Kết nối thiết bị bán hàng: máy in hoá đơn, máy quét barcode…</li>\r\n                              </ul>\r\n                              <div class="form-inline padding-xs-10">\r\n                                 <div class="form-group no-padding">\r\n                                    <h4 class="white">Bắt đầu sử dụng với 7 ngày MIỄN PHÍ</h4>\r\n                                 </div>\r\n                                 <div class="form-group no-padding"><a class="btn btn-warning btn-xs-100 mt-xs-10" href="https://auth./dang-ky/?ip=14.186.153.123&amp;meta=424">DÙNG THỬ MIỄN PHÍ</a></div>\r\n                              </div>', ' <ul class="white">\r\n                                 <li>POS bán hàng, tính tiền, in hoá đơn nhanh.</li>\r\n                                 <li>Cực kỳ dễ sử dụng, kể cả người ko biết nhiều về máy tính.</li>\r\n                                 <li>Quản lý cửa hàng bán lẻ, theo dõi doanh số, tồn kho bằng điện thoại.</li>\r\n                                 <li>Đồng bộ với <strong>website</strong> bán hàng trực tuyến và gian hàng <strong>facebook</strong>.</li>\r\n                                 <li>Kết nối thiết bị bán hàng: máy in hoá đơn, máy quét barcode…</li>\r\n                              </ul>\r\n                              <div class="form-inline padding-xs-10">\r\n                                 <div class="form-group no-padding">\r\n                                    <h4 class="white">Bắt đầu sử dụng với 7 ngày MIỄN PHÍ</h4>\r\n                                 </div>\r\n                                 <div class="form-group no-padding"><a class="btn btn-warning btn-xs-100 mt-xs-10" href="https://auth./dang-ky/?ip=14.186.153.123&amp;meta=424">DÙNG THỬ MIỄN PHÍ</a></div>\r\n                              </div>', 5, 'Bài viết trang chủ', NULL, 'Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN', 'Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN', 0, 1, '2016-12-22 02:40:58', NULL, 1, 1, 1, 'https://suno.vn/wp-content/uploads/2015/09/pos-l.png', 'Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN', NULL, 'Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN', 'Phần mềm quản lý bán hàng SIÊU ĐƠN GIẢN', NULL, NULL),
(26, 'phan-mem-cua-hang-don-gian', '<p class="sub-title" style="margin-bottom: 15px;">Dù là cửa hàng nhỏ hay chuỗi cửa hàng với hàng ngàn mặt hàng</p>\r\n            <p> hoạt động trên nền web, không cần cài đặt, giúp bạn nắm bắt và điều hành cửa hàng</p>\r\n            <p>từ xa qua&nbsp;điện thoại, máy tính bảng (android, iOS), laptop</p>\r\n            <p>Xem doanh thu, và quản lý thu, chi, dòng tiền ra vào cửa hàng, kế toán bán hàng chính xác.</p>\r\n            <p>Theo dõi tồn kho và mua hàng khi cần, loại bỏ sổ sách</p>\r\n            <p>Phân tích các báo cáo trực quan và chi tiết để ra quyết định nhanh.</p>\r\n            <p>Thêm hàng hóa mới vào kinh doanh ở bất cứ đâu.</p>', '<div class="col-sm-4 center">\r\n               <div class="dc-head">\r\n                  <div class="dc-head-info">\r\n                     <div>\r\n                        <div><i class="fa fa-shopping-cart fa- main-bg icon-120 "></i></div>\r\n                     </div>\r\n                     <h3>Bán hàng, tính tiền</h3>\r\n                     <p>Tính tiền đơn giản, dễ sử dụng, không mất thời gian hướng dẫn, chỉ cần 5 phút làm quen.<br>\r\n                        Bán hàng linh động hơn dùng máy tính bảng, điện thoại.\r\n                     </p>\r\n                  </div>\r\n               </div>\r\n            </div>\r\n            <div class="col-sm-4 center">\r\n               <div class="dc-head">\r\n                  <div class="dc-head-info">\r\n                     <div>\r\n                        <div><i class="fa fa-truck fa- sub-bg icon-120 "></i></div>\r\n                     </div>\r\n                     <h3>Tồn kho</h3>\r\n                     <p>Kiểm soát tồn kho chặt chẽ, giảm thất thoát. Nhập kho và cập nhật giá tiện lợi, bán hàng được ngay.<br>\r\n                        Phương thức quản trị tồn kho linh động theo từng loại hàng hóa, dịch vụ.\r\n                     </p>\r\n                     <p>Nắm nhanh công nợ với nhà cung cấp, phân phối</p>\r\n                  </div>\r\n               </div>\r\n            </div>\r\n            <div class="col-sm-4 center">\r\n               <div class="dc-head">\r\n                  <div class="dc-head-info">\r\n                     <div>\r\n                        <div><i class="fa fa-group fa- com-bg icon-120 "></i></div>\r\n                     </div>\r\n                     <h3>Khách hàng</h3>\r\n                     <p>Hệ thống lưu thông tin chi tiết khách hàng lúc tính tiền nhanh chóng, nhờ đó bạn nắm được thói quen mua hàng của khách, hiểu và chăm sóc họ tốt hơn.<br>\r\n                        Đổi trả hàng tiện lợi theo chính sách cửa hàng.\r\n                     </p>\r\n                  </div>\r\n               </div>\r\n            </div>', 5, 'Bài viết trang chủ', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', 0, 1, '2016-12-22 02:25:19', NULL, 1, 1, 1, '', 'Quản lý cửa hàng đơn giản', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', NULL, 'features'),
(27, 'phan-mem-cua-hang-don-gian', '<p class="mt-50">“Từ 5 đến 10 năm nữa, nếu bạn không kinh doanh qua internet thì đừng kinh doanh nữa”</p>', '', 5, 'Bài viết trang chủ', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', 0, 1, '2016-12-22 02:47:32', NULL, 1, 1, 1, '/public/css/temp/bill-gates.png', 'BILL GATES', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', NULL, 'more-ver light-blue'),
(28, 'phan-mem-cua-hang-don-gian', '<p>&nbsp;</p>\r\n               <div style="font-size: 60px;">149,000đ&nbsp;/1 tháng</div>\r\n               <p class="mt-30" style="color: #cccccc;"><em>* hoàn tiền 100% trong vòng 30 ngày nếu bạn không hài lòng khi sử dụng.</em></p>', '<div class="col-xs-12 mt-10"><label class="try">Bắt đầu sử dụng với 7 ngày MIỄN PHÍ</label></div>\r\n            <div class="col-xs-12 mt-10"><a class="btn btn-warning" href="https://auth./dang-ky/?ip=14.186.153.123&amp;meta=424">Đăng ký MIỄN PHÍ</a>&nbsp; &nbsp; &nbsp;<a class="btn btn-primary btn-xs-100 mt-xs-10" href="https:///tinh-nang/?sid=front">Tìm hiểu thêm các tính năng&nbsp;</a></div>', 5, 'Bài viết trang chủ', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', 0, 1, '2016-12-22 02:25:19', NULL, 1, 1, 1, '', 'Quản lý cửa hàng chuyên nghiệp chỉ với:', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', NULL, 'mt-50 grey'),
(29, 'phan-mem-cua-hang-don-gian', '<p>Tạo ngay website bán hàng online chuyên nghiệp, tinh tế</p>\r\n            <p>Chi phí cực thấp, bao gồm cả hosting</p>\r\n            <p>Không còn phiền toái và rắc rối với nhân viên kĩ thuật web</p>', '<div class="col-xs-12 sm-100"><a href="/public/css/temp/banner-web.png"><img class="aligncenter wp-image-282 size-full" src="/public/css/temp/banner-web.png" alt="web bán hàng cho cửa hàng thời trang" width="855" height="465" sizes="(max-width: 855px) 100vw, 855px"></a></div>\r\n         <div class="contact-wrap more-ver">Hãy bắt đầu bán hàng online với  mà không phải thiết lập thêm gì cả. Các thông tin hàng hóa, số lượng tồn kho, đơn hàng sẽ được đồng bộ trực tiếp với phần mềm bán hàng của bạn. Kho giao diện sẵn có đa dạng, phù hợp với nhiều lĩnh vực kinh doanh. Và chỉ cần tùy chỉnh tí xíu, bạn đã có trang web mang phong cách riêng của mình.</div>', 5, 'Bài viết trang chủ', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', 0, 1, '2016-12-22 02:25:19', NULL, 1, 1, 1, '', 'BẠN MUỐN MỞ RỘNG KINH DOANH', NULL, 'Quản lý cửa hàng đơn giản', 'Quản lý cửa hàng đơn giản', NULL, 'mt-50 grey');

-- --------------------------------------------------------

--
-- Table structure for table `pricelist`
--

CREATE TABLE `pricelist` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `phanloai_danhmuc` int(1) NOT NULL DEFAULT '0',
  `hotro_tag` int(1) NOT NULL DEFAULT '0',
  `danhsach_tin` int(1) NOT NULL DEFAULT '0',
  `nhieudanhsach_tin` int(1) NOT NULL DEFAULT '0',
  `ketnoi_mangxahoi` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `order_no` int(3) NOT NULL DEFAULT '0',
  `gia` float NOT NULL DEFAULT '0',
  `sonam` int(3) NOT NULL DEFAULT '1',
  `maunen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'brow',
  `mauchu` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'white'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pricelist`
--

INSERT INTO `pricelist` (`id`, `title`, `short_name`, `description`, `phanloai_danhmuc`, `hotro_tag`, `danhsach_tin`, `nhieudanhsach_tin`, `ketnoi_mangxahoi`, `created_at`, `updated_at`, `active`, `deleted`, `order_no`, `gia`, `sonam`, `maunen`, `mauchu`) VALUES
(1, 'dadadad31313adad', 'dadadad31313adad', 'adadada', 0, 0, 0, 0, 0, '2016-04-07 22:16:25', '2016-04-07 22:19:04', 0, 1, 1, 0, 1, 'black', 'white'),
(2, 'Anvy Comp', 'anvy-comp', 'Doanh nghiệp, tập đoàn, giới thiệu, thông tin sự kiện, giáo dục, dịch vụ...', 1, 1, 2, 2, 1, '2016-04-07 22:20:35', '2016-04-07 22:57:24', 1, 0, 1, 4500000, 1, 'blue', 'white'),
(3, 'Anvy Shop', 'anvy-shop', 'Shop online với quy mô từ cá nhân bán lẻ đến các trang thương mại điện tử lớn.', 0, 0, 1, 1, 0, '2016-04-07 22:25:15', '2016-04-07 22:57:07', 1, 0, 2, 2500000, 1, 'red', 'white'),
(4, 'Anvy Free', 'anvy-free', 'Là phân hệ COMP hoặc SHOP nhưng giới hạn tính năng', 1, 1, 0, 1, 0, '2016-04-08 04:01:08', '2016-04-07 22:56:44', 1, 0, 3, 0, 1, 'brow', 'white');

-- --------------------------------------------------------

--
-- Table structure for table `pricepackages`
--

CREATE TABLE `pricepackages` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `pricelist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pricepackages`
--

INSERT INTO `pricepackages` (`id`, `name`, `short_name`, `description`, `created_at`, `updated_at`, `active`, `deleted`, `pricelist_id`) VALUES
(1, 'amada da dsad', 'amada-da-dsad', 'dadada', '2016-04-20 22:48:11', '2016-04-20 22:50:10', 1, 1, 3),
(2, 'dadadadaada', 'dadadadaada', 'dadadada', '2016-04-21 01:31:05', '2016-04-21 02:00:28', 1, 0, 2),
(3, 'package 2', 'package-2', 'day la diunh nghia cua package 2', '2016-04-21 03:31:42', '2016-04-21 03:31:42', 1, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_no` int(3) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'youtube'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `name`, `short_name`, `link`, `order_no`, `active`, `deleted`, `image`, `type`) VALUES
(1, 'Facebook', 'facebook', '', 1, 1, 1, 'images/socials/thumbnail_220_14-04-16.jpg', 'youtube'),
(2, 'Youtube', 'youtube', 'https://www.youtube.com/', 1, 1, 0, NULL, 'youtube'),
(3, 'linkedin', 'linkedin', 'https://www.linkedin.com/', 2, 1, 0, NULL, 'linkedin'),
(4, 'Instagram', 'instagram', 'https://www.instagram.com/', 3, 1, 0, NULL, 'instagram'),
(5, 'Pinterest', 'pinterest', 'https://www.pinterest.com/', 4, 1, 0, NULL, 'pinterest'),
(6, 'G+', 'g-', 'https://plus.google.com/', 5, 1, 0, NULL, 'google-plus'),
(7, 'Twitter', 'twitter', 'https://twitter.com/', 6, 1, 0, NULL, 'twitter'),
(8, 'Facebook', 'facebook', 'https://www.facebook.com/', 7, 1, 0, NULL, 'facebook');

-- --------------------------------------------------------

--
-- Table structure for table `success_logins`
--

CREATE TABLE `success_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `ip_address` char(15) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `success_logins`
--

INSERT INTO `success_logins` (`id`, `admin_id`, `ip_address`, `user_agent`, `created_at`, `updated_at`) VALUES
(234, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-25 09:10:27', '2016-03-25 09:10:27'),
(235, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-25 09:47:44', '2016-03-25 09:47:44'),
(236, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-25 09:47:44', '2016-03-25 09:47:44'),
(237, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-26 01:48:07', '2016-03-26 01:48:07'),
(238, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-26 01:48:07', '2016-03-26 01:48:07'),
(239, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-26 01:51:17', '2016-03-26 01:51:17'),
(240, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-28 09:44:34', '2016-03-28 09:44:34'),
(241, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-29 01:50:18', '2016-03-29 01:50:18'),
(242, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-29 03:27:28', '2016-03-29 03:27:28'),
(243, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 01:42:26', '2016-03-31 01:42:26'),
(244, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 01:42:27', '2016-03-31 01:42:27'),
(245, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 02:24:20', '2016-03-31 02:24:20'),
(246, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 06:29:36', '2016-03-31 06:29:36'),
(247, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 06:57:35', '2016-03-31 06:57:35'),
(248, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 08:01:03', '2016-03-31 08:01:03'),
(249, 10, '14.186.180.96', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', '2016-03-31 08:01:04', '2016-03-31 08:01:04'),
(250, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-05 01:43:16', '2016-04-05 01:43:16'),
(251, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-06 01:53:23', '2016-04-06 01:53:23'),
(252, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-06 01:56:43', '2016-04-06 01:56:43'),
(253, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-06 02:01:06', '2016-04-06 02:01:06'),
(254, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-06 02:24:23', '2016-04-06 02:24:23'),
(255, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-06 03:01:53', '2016-04-06 03:01:53'),
(256, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', '2016-04-06 03:54:00', '2016-04-06 03:54:00'),
(257, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-06 06:54:00', '2016-04-06 06:54:00'),
(258, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', '2016-04-06 06:58:44', '2016-04-06 06:58:44'),
(259, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-07 01:31:07', '2016-04-07 01:31:07'),
(260, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-07 01:31:07', '2016-04-07 01:31:07'),
(261, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-07 03:07:10', '2016-04-07 03:07:10'),
(262, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-07 06:31:21', '2016-04-07 06:31:21'),
(263, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-08 02:19:52', '2016-04-08 02:19:52'),
(264, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-08 02:19:53', '2016-04-08 02:19:53'),
(265, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-08 04:33:24', '2016-04-08 04:33:24'),
(266, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-08 07:28:10', '2016-04-08 07:28:10'),
(267, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '2016-04-08 09:45:31', '2016-04-08 09:45:31'),
(268, 10, '14.186.151.22', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-09 01:39:29', '2016-04-09 01:39:29'),
(269, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-10 21:01:40', '2016-04-10 21:01:40'),
(270, 10, '14.186.210.167', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-12 03:02:48', '2016-04-12 03:02:48'),
(271, 10, '14.186.210.167', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-12 03:02:48', '2016-04-12 03:02:48'),
(272, 10, '14.186.210.167', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-13 01:47:38', '2016-04-13 01:47:38'),
(273, 10, '14.186.210.167', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-13 01:47:42', '2016-04-13 01:47:42'),
(274, 10, '14.186.196.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-14 01:52:06', '2016-04-14 01:52:06'),
(275, 10, '14.186.196.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-14 03:31:43', '2016-04-14 03:31:43'),
(276, 10, '14.187.62.231', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-15 02:24:12', '2016-04-15 02:24:12'),
(277, 10, '14.187.62.231', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-15 02:24:13', '2016-04-15 02:24:13'),
(278, 10, '14.187.62.231', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-20 03:13:43', '2016-04-20 03:13:43'),
(279, 10, '14.187.62.231', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '2016-04-21 08:52:13', '2016-04-21 08:52:13'),
(280, 10, '14.187.62.231', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36', '2016-04-21 09:38:08', '2016-04-21 09:38:08'),
(281, 10, '14.186.151.192', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '2016-06-24 07:49:17', '2016-06-24 07:49:17'),
(282, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:09:04', '2016-12-15 23:09:04'),
(283, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:10:55', '2016-12-15 23:10:55'),
(284, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:08', '2016-12-15 23:12:08'),
(285, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:08', '2016-12-15 23:12:08'),
(286, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:08', '2016-12-15 23:12:08'),
(287, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:08', '2016-12-15 23:12:08'),
(288, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:09', '2016-12-15 23:12:09'),
(289, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:09', '2016-12-15 23:12:09'),
(290, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:09', '2016-12-15 23:12:09'),
(291, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:12:09', '2016-12-15 23:12:09'),
(292, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:14:39', '2016-12-15 23:14:39'),
(293, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-15 23:14:40', '2016-12-15 23:14:40'),
(294, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-21 19:58:37', '2016-12-21 19:58:37'),
(295, 10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-21 21:06:09', '2016-12-21 21:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `subscribe` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(2) DEFAULT '1',
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poscode` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `support` int(1) NOT NULL DEFAULT '0',
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `birthday`, `subscribe`, `active`, `created_at`, `updated_at`, `fullname`, `phone`, `gender`, `address1`, `address2`, `address3`, `poscode`, `city`, `state`, `country`, `support`, `thumb`, `skype_id`) VALUES
(1, 'dsd54', 'dsada', 'hung', 'lam', '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'hung lam', '0976723656', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '/public/images/users/test.png', 'hung.lam1990'),
(3, 'dsd2', 'dsada', 'hung', 'lam', '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'hung lam', '0976723656', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '/public/images/users/test.png', 'hung.lam1990'),
(4, 'dsd3', 'dsada', 'hung', 'lam', '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'hung lam', '0976723656', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '/public/images/users/test.png', 'hung.lam1990'),
(5, 'dsd44', 'dsada', 'hung', 'lam', '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'hung lam', '0976723656', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '/public/images/users/test.png', 'hung.lam1990'),
(6, 'dsd54444', 'dsada', 'hung', 'lam', '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'hung lam', '0976723656', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '/public/images/users/test.png', 'hung.lam1990');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `short_name` (`short_name`),
  ADD KEY `short_name_2` (`short_name`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cf_key_2` (`cf_key`),
  ADD KEY `cf_key` (`cf_key`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_logins`
--
ALTER TABLE `failed_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_details`
--
ALTER TABLE `package_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricelist`
--
ALTER TABLE `pricelist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricepackages`
--
ALTER TABLE `pricepackages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `success_logins`
--
ALTER TABLE `success_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `email_2` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `failed_logins`
--
ALTER TABLE `failed_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `package_details`
--
ALTER TABLE `package_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `pricelist`
--
ALTER TABLE `pricelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pricepackages`
--
ALTER TABLE `pricepackages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `success_logins`
--
ALTER TABLE `success_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
